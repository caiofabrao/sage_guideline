[//]: # (******************************************************************************)
[//]: # ( Project           : In-Tech / Interno                                        )
[//]: # ( Author            : caio.fabrao@intech-automacao.com.br                      )
[//]: # ( Date              : 2021/05/13                                               )
[//]: # (                                                                              )
[//]: # ( Revision history                                                             )
[//]: # (------------------------------------------------------------------------------)
[//]: # ( Date          Author  Revision                                               )
[//]: # ( 2021/05/13    caf     First commit.                                          )
[//]: # (------------------------------------------------------------------------------)
[//]: # ( Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       )
[//]: # (******************************************************************************)

- - -

# Sumário
+ [Acesso Remoto](#markdown-header-acesso-remoto)
+ [Acesso Via SSH](#markdown-header-acesso-via-ssh)
+ [Acesso Sem Confirmação de Senha](#markdown-header-acesso-sem-confirmacao-de-senha)
+ [Aplicativos do SAGE via Windows](#markdown-header-aplicativos-do-sage-via-windows)
+ [Acessar SAGE em Ambiente Virtual (VMs)](#markdown-header-acessar-sage-em-ambiente-virtual-vms)
+ [Transferência de Arquivos Entre Servidores](#markdown-header-transferencia-de-arquivos-entre-servidores)

- - -

# Acesso Remoto

- - -

# Acesso Via SSH
1. Abrir o Terminal e executar `ssh sage@<ip_servidor>`
1. Informar a senha do usuário remoto

# Acesso Sem Confirmação de Senha
Para não precisar digitar a senha sempre que for se conectar em um servidor remoto, é preciso disponibilizar a chave pública do host no servidor remoto. 

1. No host, gerar as chaves públicas e privadas
	+ `ssh-keygen -t rsa`
1. Em seguida, enviar uma cópia da chave pública ao servidor remoto
	+ `scp /home/sage/.ssh/id_rsa.pub sage@<ip_servidor>:/tmp`
	+ O diretório _/tmp_ é arbitrário
1. Acessar o servidor remoto via ssh
1. Concatenar o conteúdo da chave pública no arquivos de clientes autorizados do servidor remoto
	+ `cat /tmp/id_rsa.pub >> ~/.ssh/authorized_keys2`

Em alguns casos, ainda é preciso mudar a permissão do arquivo **authorized_keys2**.

1. Ainda acessando o servidor remoto via ssh
1. Entrar no modo **Super Usuário**
1. Alterar a permissão do arquivo
	+ `chmod 600 /export/home/sage/.ssh/authorized_keys2`

**OBS: Neste contexto, todo "sage" representa o nome do usuário do sistema**

# Aplicativos do SAGE via Windows
1. Possuir um programa de acesso SSH (Ex: MobaXtern)
1. Configurar uma conexão SSH com o IP do computador SAGE
1. Logar na sessão com usuário e senha **sage**
1. Abrir o Visor de Acesso via terminal
	+ `VisorAcesso &`
1. Logar com usuário e senha **sage**
1. Se a **base estiver desativada**, será aberta a aba **Base de Tempo Real**
	+ Painel **Ativação da Base de Tempo Real**
	+ Clicar no botão **Ativa**
1. Com a **base ativada**, será aberta a aba **Programas**
	+ Painel **Destino**
	+ Marcar a opção **Avançado "localhost:10.0"**
	+ Clicar no botão **Telas** ou no aplicativo que deseja utilizar.

# Acessar SAGE em Ambiente Virtual (VMs)
Se estiver com dificuldade em acessar a VM do SAGE, tente a seguinte configuração:

1. Configurar a VM com dois adaptadores de rede
	1. O adaptador principal (eth0 no SAGE) deve ser do tipo **Brigded Adaptador**.
		+ Selecione a placa do host que está conectada na rede dos equipamentos.
	1. O adaptador secundário (eth01) deve ser do tipo **Host-only Adapter**
1. Iniciar a VM para configurar os IPs dos adaptadores.
	1. Abrir o configuração de rede (**Administração > Rede**)
	1. O IP do adaptador NAT deve ser o mesmo configurado no arquivo hosts para o servidor SAGE
	1. O IP do adaptador Host-only deve estar na faixa de endereço do Adaptador Ethernet Host-Only Network
	1. Verificar a faixa do IP no computador host
		+ Comando `ipconfig` no Windows
		+ Comando `ip addr` no Linux