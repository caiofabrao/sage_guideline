[//]: # (******************************************************************************)
[//]: # ( Project           : In-Tech / Interno                                        )
[//]: # ( Author            : caio.fabrao@intech-automacao.com.br                      )
[//]: # ( Date              : 2021/05/13                                               )
[//]: # (                                                                              )
[//]: # ( Revision history                                                             )
[//]: # (------------------------------------------------------------------------------)
[//]: # ( Date          Author  Revision                                               )
[//]: # ( 2021/05/13    caf     First commit.                                          )
[//]: # (------------------------------------------------------------------------------)
[//]: # ( Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       )
[//]: # (******************************************************************************)

- - -

# Sumário
+ [Atualizações e Licenças](#markdown-header-atualizacoes-e-licencas)
	+ [Instalação de Atualizações](#markdown-header-instalacao-de-atualizacoes)
	+ [Licenciamento](#markdown-header-licenciamento)
		+ [Ordem na instalação das licenças](#markdown-header-ordem-na-instalacao-das-licencas)
		+ [Ordem de desinstalação das licenças](#markdown-header-ordem-de-desinstalacao-das-licencas)
		+ [Dificuldade na instalação das licenças](#markdown-header-dificuldade-na-instalacao-das-licencas)

- - -

# Instalação CentOS

- - -

# Atualizações e Licenças
## Instalação de Atualizações
+ `instala_update <número_atualização>` - Instala as atualizações do SAGE; siga a numeração.
+ `remove_update <número_atualização>` - Remove uma determinada atualização do SAGE.
+ `cat $SAGE/.upd*` - Lista os Updates instalados no SAGE.

**Exemplo**

	instala_update 23-18

## Licenciamento
+ `ajusta_lic <nome_licença>` - Instala uma licença do SAGE.
	+ No terminal, escreva o nome da licença com o final "original" e dê <ENTER\>
+ `desinstala_lic <nome_licença>` - Desinstala uma licença do SAGE.
+ `verif_lic <nome_licença>` - Verifica o número e a situação de uma determinada licença.
	+ **Ex:** `verif_lic sage61850`
  
### Ordem na instalação das licenças
1. SCD
2. PROTOCOLOS

### Ordem de desinstalação das licenças
1. PROTOCOLOS
2. SCD

### Dificuldade na instalação das licenças
1. Entre no diretório SAGE/BIN.
1. Delete os arquivos **.bin** referente às licenças (SCD e as demais de protocolo).
1. Refaça o processo de instalação de licenças.

- - -

# Configuração IP Estárico
+ Edite o arquivo ifcfg-eth0 como super usuário
	+ `vi etec/sysconfig/network-scripts/ifcfg-eth0`
+ Altere a linha `IPADDR` com o ip desejado 
+ Reinicie o serviço de rede
	+ `service network restart`

- - -

# Adicionar privilégio de Execução à arquivos
+ chmod +x <arquivo>
+ chmod -R <permission> <dir_name>