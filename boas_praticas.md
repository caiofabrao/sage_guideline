[//]: # (******************************************************************************)
[//]: # ( Project           : In-Tech / Interno                                        )
[//]: # ( Author            : caio.fabrao@intech-automacao.com.br                      )
[//]: # ( Date              : 2021/05/13                                               )
[//]: # (                                                                              )
[//]: # ( Revision history                                                             )
[//]: # (------------------------------------------------------------------------------)
[//]: # ( Date          Author  Revision                                               )
[//]: # ( 2021/05/13    caf     First commit.                                          )
[//]: # (------------------------------------------------------------------------------)
[//]: # ( Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       )
[//]: # (******************************************************************************)

- - -

# Sumário
+ [Boas Práticas](#markdown-header-boas-praticas)
+ [Compilação da Base de Dados](#markdown-header-compilacao-da-base-de-dados)
+ [Desabilitar BumpBD antes de alterar a Base de Dados](#markdown-header-desabilitar-bumpbd-antes-de-alterar-a-base-de-dados)
+ [Incluindo Novos Pontos de Aquisição/Distribuição](#markdown-header-incluindo-novos-pontos-de-aquisicaodistribuicao)
+ [Alterações na Base de Dados com Dois Servidores](#markdown-header-alteracoes-na-base-de-dados-com-dois-servidores)
+ [Alterações nos Cálculos](#markdown-header-alteracoes-nos-calculos)
+ [Configurar o Arquivo Hosts](#markdown-header-configurar-o-arquivo-hosts)
+ [Monitorando um Processo](#markdown-header-monitorando-um-processo)
+ [Trocar TAGs de uma Tela](#markdown-header-trocar-tags-de-uma-tela)
+ [Comunicação com Terminal Server](#markdown-header-comunicacao-com-terminal-server)
+ [HotStand-By](#markdown-header-hotstand-by)
+ [Pop-ups com Comando Pré-Selecionado](#markdown-header-pop-ups-com-comando-pre-selecionado)

- - -

# Boas Práticas

- - -

# Compilação da Base de Dados
Antes de iniciar o processo completo de atualização da base de dados, realize a compilação apenas da base quente (utilizar `AtualizaBD` sem parâmetros adicionais). Isso irá evitar que erros na compilação derrubem a base de dados do SAGE.

Após verificar que nenhum erro está presente, utilize `AtualizaBD fria fonte` para atualizar por completo.

# Desabilitar BumpBD antes de alterar a Base de Dados
Antes de dar o comando AtualizaBD fria fonte, desabilite manualmente o processo DumpBd.

Isso é uma medida de segurança, pois o processo DumpBd, a cada 10 minutos, grava dados em um arquivo XDR. Pode acontecer de o processo DumpBd gerar uma gravação de dados ao mesmo tempo em que a atualização da base alterar o arquivo. Isso irá criar um conflito e, ao ativar a base, ela não subirá.

# Incluindo Novos Pontos de Aquisição/Distribuição
Este procedimento é útil pois permite alterar a base de dados do SAGE sem que seja alterado os DATs principais.

1. Gere os DATs dos novos pontos a parte.
1. Navegue até a pasta de dados - `cd $BD/dados`
1. Crie uma nova pasta - `mkdir <novo_diretorio>`
1. Copie os novos DATs para a pasta criada
1. Localize os DATs da base principal correspondente aos novos DATs
1. Adicionar um include no final (ou no inicio, como preferir) de cada DAT existente da base principal - Ex: `#include <novo_diretorio>/pdf.dat`
1. Modifique o nome do arquivo DAT conforme a necessidade

Esta informação vai gerar uma ligação entre os DATs principais e os novos DATs. Para o SAGE, os dois DATs são como se fosse um único arquivo.

**OBS:** Lembre-se, só é preciso colocar o include nos DATs da base principal referentes aos DATs que foram gerados na nova pasta.

# Alterações na Base de Dados com Dois Servidores
Quando tiver dois SAGES operando em HotStand-By, atualize somente em um SAGE a base dos .dat. Após atualizar a base, copie os arquivos .xdr (arquivos DATs compilados) para o SAGE secundário. É preciso que os arquivos .xdr estejam com o mesmo horário para que a base no SAGE funcione corretamente.

# Alterações nos Cálculos
Para evitar ter problemas ao compilar alterações nos cálculos com a base rodando:

1. Desabilitar o processo de calculos
1. Entrar no diretório calculos
1. Executar `instala_calculos`
1. Após a compilação e instalação sem erro, habilitar novamente o processo

# Configurar o Arquivo Hosts
Para fazer alterações rápidas no arquivo hosts, entre como **Super Usuário** e utilize o editor **vi**.

Lembre-se de reiniciar o computador para que as alterações sejam carregadas.

# Monitorando um Processo
1. `kill -USR1 nProcesso` gera um arquivo de monitoramento 
	+ obs: o log só para de ser gerado ao executar o comando novamente ou ao parar o processo
1. `tail -f nome_arquivo.mon` monitora o arquivo gerado

Para saber o número do processo: `ps -aux | grep iec4t`

# Trocar TAGs de uma Tela
Quando for preciso alterar as TAGs de uma tela, dê preferência em utilizar o comando `TrocaIdentTelas`. Além de facilitar o processo, o comando cria um backup da tela, facilitando retroceder caso ocorra algum erro.

	`TrocaIdentTelas <TAGs_pas.csv> <TAGs_pds.csv> <tela_a_ser_alterada>`

É obrigatório gerar os dois arquivos .csv mesmo que um deles não tenha TAGs a serem alteradas (arquivo vazio).

O arquivo csv utilizado deve possuir duas colunas. Na primeira ficam as TAGs atuais da tela (as que serão alteradas) e na segunda as novas TAGs.

# Comunicação com Terminal Server
Além de definir o IP do terminal server no arquivo hosts, deve-se configurar o arquivo tsr.conf

	$SAGE/config/$BASE/sys/

e os DATs CXU, ENU e UTR.

# HotStand-By
A configuração do modo de HotStand-By se dá no arquivo SSC_Amb 

	$SAGE/config/$BASE/sys/

Para isso altere o método de difusão para "dc" (difusão confiável).

# Pop-ups com Comando Pré-Selecionado
O arquivo SigComportamento.dat do diretório $IHM é o responsável por fazer com que as pop-ups de comandos tenham pré-selecionado o comando conforme o estado do equipamento, ex: D7124 Aberto, ao abrir a pop-up de comando já irá deixar em "FECHAR"

# Gerando Backup
Dê prioridade em usar o comando `zipconfig <nome_base> -bz2`. Os comandos `criasagecnf` e `criacnf_com_data` geram arquivos de backup com baixa compressão. Eles são mais rápidos que o `zipconfig`, porém criam arquivos com tamanho consideravelmente maior.

# Pontos de Intertravamento
Ao adicionar pontos de intertravamento, para cada equipamento, sempre colocar o ponto local do sage `PLA-_SSC-_SAGE-_REMLOC` e após ele seus respectivos pontos que irão intertravar o equipamento (entidade RCA).

# Simulação Protocolo SNMP
O programa MIB Browser da ireasoning pode ser usado para testar a comunicação SNMP.

# Backup de Alarmes e Eventos
Ao finalizar o processo de comissionamento, realize o backup dos arquivos de alarme e eventos gerados pelo SAGE. Utilize o comando `zip alarmes_eventos.zip *.alr *.sde *.aud` no diretório `$ARQS` para gerar um zip com todos os arquivos necessários.

Alternativamente, utilize `zip $SAGE/alarmes_eventos.zip $ARQS/*.alr $ARQS/*.sde $ARQS/*.aud` para criar o zip dos alarmes e eventos no diretório `$SAGE` sem precisar navegar até o diretório `$ARQS`. 