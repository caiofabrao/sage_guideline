****************************************************************************************
******************************In-Tech Automação & Sistemas******************************
****************************************************************************************
****BY: Lucas Kloster Wagner************************************************************
****************************************************************************************

Abra o "konsole" (terminal) do SAGE na máquina UNO e digite:

> ssh-keygen -t rsa

Pressione ENTER para executar o comando;
Pressione ENTER novamente para aceitar o caminho do arquivo;
Caso o SAGE pergunte se você deseja substituir o arquivo Digite "y" para aceitar;
Quando o SAGE perguntar "ENTER PASSPHRASE", deixe em branco. 
Apenas pressione ENTER e depois ENTER novamente para confirmar Passphrase em branco;

Agora vamos configurar o acesso a maquina IHM:

> ssh sage@ihm

O SAGE irá lhe perguntar "Are you sure you want to continue connecting (yes/no)?";
Você digitará "yes" e pressionará ENTER;
Quando ele lhe perguntar a senha você irá colocar "sage" e pressionará ENTER;
Após este procedimento você digitará "exit" para encerrar a conexão;

Agora vamos copiar a chave pública para a máquina SAGE IHM: 

> scp /export/home/sage/.ssh/id_rsa.pub sage@ihm:/export/home/sage/sage/tmp/ 

O SAGE irá lhe perguntar a senha novamente e você digitará "sage" e pressionará ENTER;
Em seguida vamos acessar a máquina IHM novamente:

> ssh sage@ihm

Ele irá lhe pedir a senha novamente;
Vamos concatenar o conteúdo da chave para o arquivo de clientes autorizados na IHM:

> cat /export/home/sage/sage/tmp/id_rsa.pub >> /export/home/sage/.ssh/authorized_keys2

Em alguns sistemas ainda é necessário mudar a permissão do authorized_keys2;
Ainda na máquina IHM acesse como super-usuário(su) e digite:

> cd /export/home/sage/.ssh/
> chmod 600 authorized_keys2

Digite "exit" para encerrar o acesso como super-usuário;
“exit” novamente e pressione ENTER para ecerrar o acesso à máquina IHM;
Pronto, agora o acesso via SSH não exigirá senha;
Façamos um teste para verificar se o procedimento obteve sucesso:

> ssh sage@ihm

Agora o acesso deve ter ocorrido de forma direta sem necessidade de senha;
Se não obtiver sucesso, favor entrar em contato com a In-Tech;
Encerre a conexão SSH:

> exit

Copie o arquivo "gcd_on.rc" em anexo no e-mail para a pasta /export/home/sage/sage/tmp/ na máquina UNO e execute o comando abaixo:

> scp /export/home/sage/sage/tmp/gcd_on.rc /export/home/sage/sage/bin/scripts/

Reinicie o servidor X, pressionando "CTRL+ALT+Backspace";
Pronto agora toda vez que o UNO entrar em operação ele irá assumir como Máquina Principal.

****************************************************************************************
****************************************FIM*********************************************
****************************************************************************************