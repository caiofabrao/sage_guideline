#!/bin/bash
# chmod +x ./incluidats.sh
#******************************************************************************#
# Project           : Ingeteam / Joao Camara II
# Author            : caio.fabrao@intech-automacao.com.br
# Date              : 2020/12/18
#
# Revision history
#------------------------------------------------------------------------------#
# Date          Author  Revision
# 2020/12/18    caf     First commit.
#------------------------------------------------------------------------------#
# TODO
#------------------------------------------------------------------------------#
# + Nao inserir o include se a linha ja estiver presente no dat
# + Nao comentar o include se a linha ja estiver comentada no dat
# + Propor sobreescrever dat ao mover
# + Adiconar funcao de remover a linha de include dos dat
# + Verificar se dir atual é filho de $BD/dados e usar como bayName
# + Verificar se o dat está recebendo seu primeiro include (bug onde não é
# adicionado a linha se o arquivo não possuir ao menos um include)
#------------------------------------------------------------------------------#
# Copyright (c) 2020 by In-Tech Automacao & Sistemas. All Rights Reserved.
#******************************************************************************#

function ShowHelp(){
	echo "Uso: incluidats.sh [opção]... bay"
	echo "Adiciona a referencia dos dats do bay nos dats da base ativa."
	echo "Exemplo: incluidats.sh -i intech"
	echo ""
	echo "Opções:"
	echo "-c, --comment             Comenta a referencia em vez de adicionar"
	echo "-u, --uncomment           Descomenta a referencia em vez de adicionar"
	echo "-i, --include             Inclui a referencia dos dats do bay (opcao padrao)"
	echo "-m, --move                Move o diretório BAY da pasta atual para $BD/dados"
	echo "                          e adiciona o include nos dats"
	echo ""
	echo "Miscelânea:"
	echo "-V, --version             mostra informações sobre versão e sai"
	echo "-h, --help                exibe essa ajuda e sai"
	echo ""
	echo "Exit status is 0 if ok, 1 if miscelaneas and 2 if trouble."
	exit 1
}
function ShowVertion(){
	echo "incluidats 1.0.0"
	echo ""
	echo "Copyright 2020 In-Tech - Automacao e Sistemas."
	echo "Todos os diretos reservados."
	exit 1
}
function ShowUsage(){
	echo "Uso: incluidats.sh [opção]... bay"
	echo "Tente 'incluidats.sh -h' para mais informações."
	exit 2
}
function MoveIncludeDat(){
	mv ./$bayName $BD/dados
	IncludeDat
}
function IncludeDat(){
	for datName in "$BD/dados/$bayName"/*.dat
	do
		datName=${datName##*/}
		datFile="$BD/dados/$datName"
		includeLine="#include $bayName/$datName"
		sed -ie "$(grep -n '#include' $datFile |tail -1|cut -f1 -d':')a $includeLine\n" $datFile
	done
}
function CommentInclude(){
	for datName in "$BD/dados/$bayName"/*.dat
	do
		datName=${datName##*/}
		datFile="$BD/dados/$datName"
		includeLine="#include $bayName/$datName"
		sed -i "s+$includeLine+;$includeLine+" $datFile
	done
}
function UncommentInclude(){
	for datName in "$BD/dados/$bayName"/*.dat
	do
		datName=${datName##*/}
		datFile="$BD/dados/$datName"
		includeLine="#include $bayName/$datName"
		sed -i "s+;$includeLine+$includeLine+" $datFile
	done
}

bayName=""
includeFlag=0
moveFlag=0
commentFlag=0
uncommentFlag=0
firstArg=1

while [[ $# -gt 0 ]]
do
	key="$1"
	case $key in
		-c|--comment)
			commentFlag=1
			shift
		;;
		-h|--help)
			ShowHelp
		;;
		-i|--include)
			includeFlag=1
			shift
		;;
		-m|--move)
			moveFlag=1
			shift
		;;
		-u|--uncomment)
			uncommentFlag=1
			shift
		;;
		-V|--version)
			ShowVertion
		;;
		*)
			if [[ firstArg -eq 1 ]]; then
				bayName=$1
				shift
				firstArg=0
			else
				echo "Excesso de argumentos"
				ShowUsage
			fi
		;;
	esac
done

if [[ $((includeFlag + moveFlag + commentFlag + uncommentFlag )) -ge 2 ]]
then
	ShowHelp
elif [[ $((includeFlag + moveFlag + commentFlag + uncommentFlag )) -eq 0 ]]
then
	includeFlag=1
fi

if [[ $includeFlag -eq 1 ]]; then
	IncludeDat
elif [[ $moveFlag -eq 1 ]]; then	
	MoveIncludeDat
elif [[ $commentFlag -eq 1 ]]; then	
	CommentInclude
elif [[ $uncommentFlag -eq 1 ]]; then	
	UncommentInclude
fi

exit 0