[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/12                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/12    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Rede de Difusão Confiável
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para a configuração de uma rede de difusão confiável.

# Entidades DATs Necessárias
![Alt text](imgs/dc_der.png)

+ **[PRO](#markdown-header-pro):** Classe de Processo
+ **[CXP](#markdown-header-cxp):** Relacionamento Classe MCD x Classe de Processo
+ **[INP](#markdown-header-inp):** Instância de Processo
+ **[NOH](#markdown-header-noh):** Nó da Rede
+ **[SXP](#markdown-header-sxp):** Relacionamento Severidade x Classe de Processo
+ **[SEV](#markdown-header-sev):** Severidade

- - -

# PRO
Configura os processos do SAGE.

## Atributos
### ID
Identificador do Processo.

### ATIVA
Tipo de ativação do processo.

### ATVAT
Indicador de ativação automática.

### ESSEN
Indicador de processo essencial.

### MONIT
Tipo de monitoração.

### NOME
Nome do processo usado na descrição de alarmes e eventos.

### NUATV
Número de ativações para falha.

### SCRAT
Script de ativação do processo.

### SCRDE
Script de desativação do processo.

### TINIC
Tempo em segundos.

### TIPPR
Tipo da primeira ativação.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	PRO
		   ID= i104
		ATIVA= CAD
		ATVAT= AUT
		ESSEN= ESC
		MONIT= MON
		 NOME= Protocolo IEC/60870-5-104
		NUATV= 4
		SCRAT= IT104_on.rc
		SCRDE= IT104_off.rc
		TINIC= 6
		TIPPR= INSP

- - -

# CXP
Especifica o relacionamento entre a entidade MCD e PRO.

## Atributos
### CLASSE
Chave estrangeira da Classe MCD. Relacionamento 1 -> n entre as entidades MCD e PRO.

### PRO
Chave estrangeira da Classe de Processo. Relacionamento 1 -> n entre as entidades PRO e MCD.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CXP
		CLASSE= SSC_ClasAlr
		   PRO= alr
	
	CXP
		CLASSE= SSD_ClasIds
		   PRO= alr
	
	CXP
		CLASSE= SSC_ClasTnd
		   PRO= hist

- - -

# INP

Configura as instâncias de todos os processos do SAGE. Um processo pode ser ativado em vários nós e cada uma dessas ativações define uma instância de processo.

## Atributos
### NOH
Chave estrangeira do Nó da Rede. Relacionamento 1 -> n entre as entidades NOH e PRO.

### ORDEM
Ordem do Processo.

### PRO
Chave estrangeira da Classe de Processo. Relacionamento 1 -> n entre as entidades PRO e NOH.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	INP
		  NOH= srv1_dc_see
		ORDEM= 1
		  PRO= i104
	
	INP
		  NOH= srv2_dc_see
		ORDEM= 2
		  PRO= i104

- - -

# SXP

Define para cada processo um limite tolerado de erros e eventos que ele pode gerar, classificados por severidade.

## Atributos
### PRO
Chave estrangeira da Classe de Processo. Relacionamento 1 -> n entre as entidades PRO e SEV.

### SEV
Chave estrangeira da Severidade. Relacionamento 1 -> n entre as entidades SEV e PRO.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	SXP
		PRO= i104
		SEV= ADVER
		
	SXP
		PRO= i104
		NOH= FATAL
	
	SXP
		PRO= i104
		NOH= NORML
	
	SXP
		PRO= i104
		NOH= PANIC
	
	SXP
		PRO= i104
		NOH= SNULA
	
	SXP
		PRO= i104
		NOH= URGEN

- - -

# NOH
Descreve os nós da rede de difusão confiável

## Atributos
### ID
Identificador do Nó da rede. Recomenda-se o uso de uma string concatenando as siglas do nó, do tipo de difusão e da subestação.

### ENDIP
Endereço IP do nó na rede.

### NOME
Nome do nó.

### NTATV
Número de tentativas de ativação do nó.

### TPNOH
Tipo de nó do sistema.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NOH
		ENDIP= 200.15.40.1
		   ID= srv1-dc-see
		 NOME= Servidor SAGE Linux 1
		TPNOH= SERVIDOR
		NTATV= 4
	
	NOH
		ENDIP= 200.15.40.2
		   ID= srv2-dc-see
		 NOME= Servidor SAGE Linux 2
		TPNOH= SERVIDOR
		NTATV= 4
	
	NOH
		ENDIP= 200.15.40.3
		   ID= ihm-dc-see
		 NOME= Console SAGE
		TPNOH= CONSOLE
		NTATV= 4

- - -

# SEV

Lista as severidades existentes no SAGE. Severidade é um grau de importância que se dá a uma falha ou evento, que vai definir a cor do registro de ocorrências nas listas de alarmes e eventos.

## Atributos
### ID
Identificador da Severidade.

### NOME
Nome da severidade.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	SEV
		 ID= ADVER
		NOME= severidade de advertencia
	
	SEV
		 ID= FATAL
		NOME= severidade fatal
	
	SEV
		 ID= NORML
		NOME= severidade de informacao (nao severo)
	
	SEV
		 ID= PANIC
		NOME= sever. de panico (maior sever. p/macro alr)
	
	SEV
		 ID= SNULA
		NOME= nulo
	
	SEV
		 ID= URGEN
		NOME= severidade de urgencia

- - -