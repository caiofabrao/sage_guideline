[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/18                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/18    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Pontos Físicos
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para configuração dos Pontos Físicos.

# Entidades DATs Necessárias
![Alt text](imgs/pontos_fisicos_der.png)

+ **[CNF](#markdown-header-cnf):** Configuração da Ligação Física do SCD
+ **[NV1](#markdown-header-nv1):** Nível 1 da Configuração Física
+ **[NV2](#markdown-header-nv2):** Nível 2 da Configuração Física
+ **[TN1](#markdown-header-tn1):** Tipos de Entidades Físicas do Nível 1
+ **[TN2](#markdown-header-tn2):** Tipos de Entidades Físicas do Nível 2
+ **[CGF](#markdown-header-cgf):** Ponto de Controle Físico
+ **[PAF](#markdown-header-paf):** Pontos Analógicos Físicos
+ **[PDF](#markdown-header-pdf):** Pontos Digitais Físicos
+ **[PTF](#markdown-header-ptf):** Pontos Totalizados Físicos
+ **[RFC](#markdown-header-rfc):** Relação de Filtro Composto
+ **[RFI](#markdown-header-rfi):** Relação de Filtro do SCD

- - -

# CNF
Configura todos as CNFs de aquisição e/ou distribuição associadas a todas as LSCs do sistema.

## Atributos
### ID
Identificador da configuração física. Recomendado utilizar o mesmo id da MUL associada a esta CNF. Sugere-se uma identificação de 2 a 3 caracteres, para que possa ser usada como **prefixo-id** dos pontos dessa CNF.

### CONFIG
Configuração específica da ligação. Depende do protocolo utilizado.

### LSC
Chave estrangeira da ligação (LSC) à qual a CNF pertence. Relacionamento 1 -> 1 entre as entidades LSC e CNF.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CNF
		CONFIG= ApTitle= 1 1 10 / 1 1 10 AeQ= 1 PS= 1 / 1 SS= 1 TS= 1 IDAD= 600 KEEP= 5 NREP= 3 TOUT= 10 MPDU= 0 OPMSK= 0C131 GOOSE= 0
		    ID= 2i1
		   LSC= 2IF1
	
	CNF
		CONFIG= PlPr= 1   LiPr= 5   PlRe= 2   LiRe= 5   DnpLvl= 103
		    ID= ACOT
		   LSC= ACOT

- - -

# NV1
Configura todas as entidades de nível 1 de todas as CNFs.
	
## Atributos
### CONFIG
Configuração específica do nível 1.

### ID
Identificador do NV1. Sugere-se para este identificador o mesmo da entidade CNF, adicionada de um número sequencial, caso o IED possua mais de um Logical Device.

### ORDEM
Este atributo deve ser preenchido com um número de ordem de 1 a "n", onde "n" é a quantidade de Logical Devices do IED utilizados pelo SAGE.

### CNF
Chave estrangeira da configuração à qual a classe pertence. Relacionamento 1 -> n entre as entidades CNF e NV1.

### TN1
Chave estrangeira do tipo da entidade física do nível 1. Relacionamento 1 -> n entre as entidades TN1 e NV1.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NV1
		    ID= 2i11
		   CNF= 2i1
		 ORDEM= 1
		   TN1= NLN1
		CONFIG= AA14F1Q10FP1CFG
	
	NV1
		    ID= 2i12
		   CNF= 2i1
		 ORDEM= 2
		   TN1= NLN1
		CONFIG= AA14F1Q10FP1ANN
	
	NV1
		    ID= 2i13
		    CNF= 2i1
		  ORDEM= 3
		    TN1= NLN1
		CONFIG= AA14F1Q10FP2ANN
	
	NV1
	        ID= 2i21
	       CNF= 2i2
	     ORDEM= 1
	       TN1= NLN1
	    CONFIG= AA14F1Q10FP2CON

- - -

# NV2
Configura todas as entidades de nível 2 de todas as entidades de nível 1 de todas as CNFs.

## Atributos
### CONFIG
Configuração específica do nível 2.

### ID
Identificador do NV2. Sugere-se utilizar o mesmo da entidade CNF relacionada, concatenado por underscore com a sigla do tipo de dado dentre os listados na entidade TN2.

### ORDEM
Este atributo ordena com um número de 1 a "n" a ocorrência do NV2 dentre do conjunto de tipos associados a esta CNF / NV1.

### TPPNT
Tipo dos pontos físicos vinculados a essa NV2.

+ PDF - caso TN2 seja ADAQ (Aquisição de objetos com CDCs classificados no SAGE como "digitais")
+ PAF - caso TN2 seja AAAQ (Aquisição de objetos com CDCs classificados no SAGE como "analógicos")
+ PTF - caso TN2 seja ATTA (Aquisição de objetos com CDCs classificados no SAGE como "discretos" e/ou "totalizadores")
+ CGF - caso TN2 seja CSIM (Objetos de Controle Supervisório)

### NV1
Chave estrangeira da ocorrência da entidade NV1 à qual o conjunto de pontos pertence. Relacionamento 1 -> n entre as entidades NV1 e NV2.

### TN2
Chave estrangeira do tipo de dado sendo configurado dentre os cadastrados na entidade TN2. Relacionamento 1 -> n entre as entidades TN2 e NV2.

+ ADAQ - Aquisição de objetos com CDCs classificados no SAGE como "digitais"
+ AAAQ - Aquisição de objetos com CDCs classificados no SAGE como "analógicos"
+ ATTA - Aquisição de objetos com CDCs classificados no SAGE como "discretos" e/ou "totalizadores"
+ CSIM - Objetos de Controle Supervisório

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NV2
	    CONFIG= Aquisicao CFG para PDF 
	        ID= 2i11_ADAQ
	       NV1= 2i11
	     ORDEM= 1
	       TN2= ADAQ
	     TPPNT= PDF
	
	NV2
	    CONFIG= Aquisicao ANN para PDF
	        ID= 2i12_ADAQ
	       NV1= 2i12
	     ORDEM= 1
	       TN2= ADAQ
	     TPPNT= PDF
		
	NV2
	    CONFIG= Aquisicao ANN para PAF
	        ID= 2i13_AAAQ
	       NV1= 2i13
	     ORDEM= 1
	       TN2= AAAQ
	     TPPNT= PAF
	
	NV2
	    CONFIG= Aquisicao CON para CGF
	        ID= 2i21_CSIM
	       NV1= 2i21
	     ORDEM= 1
	       TN2= CSIM
	     TPPNT= CGF

- - -

# TN1
Descreve os tipos de entidade de nível 1 que podem existir. Não deve ser alterada.

## Atributos
### ID
Identificador do tipo de entidade física do nível 1.

### DESCR
Descrição do tipo de entidade física do nível 1.

### NSEQ
Número sequencial.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TN1
		DESCR= Ausencia de entidade no nivel 1
		   ID= NLN1
		 NSEQ= 0
	
	TN1
		DESCR= Grupo de aquisicao UTR em protocolo DNP V3.00
		   ID= ADNP
		 NSEQ= 35
	
	TN1
		DESCR= Grupo de distribuicao COS em protocolo IEC/60870-5-104
		   ID= D104
		 NSEQ= 55

- - -

# TN2
Descreve os tipos de entidade de nível 2 que podem existir. Não deve ser alterada.

## Atributos
### ID
Identificador do tipo de entidade física do nível 2.

### DESCR
Descrição do tipo de entidade física do nível 2.

### NSEQ
Número sequencial.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TN2
		DESCR= Aquisicao digital UTR simples (non-latch)
		   ID= ASIM
		 NSEQ= 4
	
	TN2
		DESCR= Aquisicao ou distribuicao de medida analogica em ponto flutuante
		   ID= APFL
		 NSEQ= 34

- - -

# PAF
Configura todos os pontos analógicos físicos de aquisição e de distribuição associados a todas as configurações (CNFs) de todas as ligações (LSCs).

## Atributos
### ID
Identificador do ponto analógico físico.

### KCONV1
Coeficiente multiplicativo de conversão da medida (escala).

### KCONV2
Constante de conversão para unidade de engenharia.

### KCONV3
Conversão de formato (e CDCs IEC/61850).

### ORDEM
Ordem do ponto no nível 2 da configuração física.

### TPPNT
Tipo do ponto analógico (distribuição ou aquisição).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PAF.

### PNT
Chave estrangeira do ponto analógico lógico ao qual o ponto analógico físico está associado. Relacionamento 1 -> 1 entre as entidades PAS e PAF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PAF
	       CMT= AA14F1Q10FP2ANN-METMMXU1$MX$TotVAr
	        ID= 2i13-METMMXU1$MX$TotVAr
	    KCONV1= 1
	    KCONV2= 0
	    KCONV3= MV0
	     TPPNT= PAS
	       NV2= 2i13_AAAQ
	       PNT= SEE:02I1:MVAR
	     DESC1= MVAR da LT 02I1-SEE/SCL
	
	PAF
	       CMT= AA14F1Q10FP2ANN-METMMXU1$MX$PhV$phsA
	        ID= 2i13-METMMXU1$MX$PhV$phsA
	    KCONV1= 1.732
	    KCONV2= 0
	    KCONV3= CMV0
	     TPPNT= PAS
	       NV2= 2i13_AAAQ
	       PNT= SEE:02I1:KVAB
	     DESC1= KV da Fase AB da LT 02I1-SEE/SCL

- - -

# PDF
Configura todos os pontos digitais físicos de aquisição e de distribuição associados a todas as configurações (CNFs) de todas as ligações (LSCs).

## Atributos
### ID
Identificador do ponto digitais físico.

### KCONV
Conversão de formato (e CDCs IEC/61850).

### ORDEM
Ordem do ponto no nível 2 da configuração física.

### TPPNT
Tipo do ponto digital (distribuição ou aquisição).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PDF.

### PNT
Chave estrangeira do ponto digital lógico ao qual o ponto digital físico está associado. Relacionamento 1 -> 1 entre as entidades PDS e PDF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PDF
	       CMT= AA14F1Q10FP1CFG-DC1CSWI1$ST$Pos
	        ID= 2i11-DC1CSWI1$ST$Pos
	     KCONV= DPS0
	     TPPNT= PDS
	       NV2= 2i11_ADAQ
	       PNT= SEE:32I1-1:89
	     DESC1= Seccionadora 32I1-1
	     DESC2= 0 - Chave Abriu     1 - Chave Fechou
	
	PDF
	       CMT= AA14F1Q10FP1CFG-M3PPDIS5$ST$Op
	        ID= 2i11-M3PPDIS5$ST$Op
	       NV2= 2i11_ADAQ
	     KCONV= ACT0
	       PNT= SEE:02I1:F1:PDTZ
	     TPPNT= PDS
	     DESC1= Trip Zona 3 (21)
	     DESC2= 0 - Normalizou      1 - Atuou

- - -

# PTF
Configura todos os pontos totalizadores físicos de aquisição e de distribuição associados a todas as configurações (CNFs) de todas as ligações (LSCs).

## Atributos
### ID
Identificador do ponto totalizadores/discretos físico.

### KCONV1
Constante de conversão para unidade de engenharia.

### KCONV2
Constante de conversão para unidade de engenharia.

### KCONV3
Conversão de formato (e CDCs IEC/61850).

### ORDEM
Ordem do ponto no nível 2 da configuração física.

### TPPNT
Tipo do ponto totalizado lógico (distribuição ou aquisição).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PTF.

### PNT
Chave estrangeira do ponto totalizador/discreto lógico ao qual o ponto totalizador/discreto físico está associado. Relacionamento 1 -> 1 entre as entidades PTS e PTF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PTF
		   PNT= SEE:02I1:F1:KM
		KCONV3= INC0
			ID= F13-pdGGIO140$ST$ISCSO7
		   NV2= F13_ATTA
		 TPPNT= PTS

- - -

# CGF
Configura todos os pontos de controle físicos de aquisição e de distribuição associados a todas as configurações (CNFs) de todas as ligações (LSCs).

## Atributos
### ID
Identificador do ponto de controle físico.

### KCONV
Concatenação de duas palavras chaves que indicam o tipo de seleção de controle (SBO ou SBOw) e se tem enhenced security.

Se deixado vazio, indica que o controle é direto, sem enhenced security e sem nenhum dos dois checks (Sincro-Check e Interlock-Check)

+ SBO  - comando do tipo Select-before-operate
+ SBOw - comando do tipo Select-before-operate-with-value

e/ou

+ TERM - deve aguardar por uma mensagem (PDU) com um InformationReport sinalizando o término (CommandTermination) do controle enhenced security.

### ORDEM
Ordem do ponto no nível 2 da configuração física.

### OPCOES
Atributo opcional. Pode ser preenchido com até duas palavras chave, sendo elas:

+ I - Ativa o Interlock-Check
+ S - Ativa o Sincro-Check
+ SI - Ativa Interlock-Check e Sincro-Check

e/ou

+ O1 - origem do controle em bay-control
+ O2 - origem do controle em station-control (default)
+ O3 - origem do controle em remote-control
+ O4 - origem do controle em automatic-bay
+ O5 - origem do controle em automatic-station
+ O6 - origem do controle em automatic-remote
+ O7 - origem do controle em maintenance
+ O8 - origem do controle em process

### CGS
Chave estrangeira do ponto de controle lógico ao qual o ponto de controle físico está associado. Relacionamento 1 -> 1 entre as entidades CGS e CGF.

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto de controle pertence. Relacionamento 1 -> n entre as entidades NV2 e CGF.

### CNF
Chave estrangeira da configuração física associada ao controle distribuído. Relacionamento 1 -> n entre as entidades CNF e CGF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	CGF
	       CMT= AA14F1Q10FP2CON-SCSWI1$CO$Pos
	        ID= 2i21-SCSWI1$CO$Pos
	       NV2= 2i21_CSIM
	       CGS= SEE:12D1:52
	     DESC1= Disjuntor 12P3
		 KCONV= SBO
	    OPCOES= SI O2
	    
	CGF
	       CMT= AA14F1Q10FP2CON-SCSWI1$CO$Pos
	        ID= 2i21-SCSWI1$CO$Pos-d1
	       NV2= 2i21_CSIM
	       CGS= COT:12D1:52
	     DESC1= Disjuntor 12P3
		 KCONV= SBO
	    OPCOES= SI O2

- - -

# RFI
Filtro simples é aquele cujas parcelas (pontos físicos) podem participar de apenas um único filtro gerando ponto lógico.

## Atributos
### ORDEM
Número sequencial de 1 a n que indica a ordem da parcela no filtro.

### TIPOP
Tipo da parcela do filtro. Pode assumir os valores:

+ PAF - Se a parcela do filtro for um ponto analógico físico.
+ PDF - Se a parcela do filtro for um ponto digital físico.
+ PTF - Se a parcela do filtro for um ponto totalizador físico.

### PNT
Chave estrangeira do ponto físico que compõe a parcela. Relacionamento 1 -> 1 entre as entidades de pontos físicos (PAF, PDF ou PTF. Depende do valor de TIPOP).

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	RFI
		ORDEM= 1
		  PNT= S1_A104_1_ASIM_307
		TIPOP= PDF
	
	RFI
		ORDEM= 2
		  PNT= S1_A104_1_ASIM_308
		TIPOP= PDF

- - -

# RFC
Filtro composto é aquele cujas parcelas (pontos físicos) pode participar de vários filtros gerando vários pontos lógicos.

## Atributos
### ORDEM
Número sequencial de 1 a n que indica a ordem da parcela no filtro.

### TPPARC
Tipo da parcela do filtro. Pode assumir os valores:

+ PAF - Se a parcela do filtro for um ponto analógico físico.
+ PDF - Se a parcela do filtro for um ponto digital físico.
+ PTF - Se a parcela do filtro for um ponto totalizador físico.

### TPPNT
Tipo do ponto resultante do filtro. Pode assumir os valores:

+ PAS - Se o ponto resultante do filtro for analógicos lógicos.
+ PDS - Se o ponto resultante do filtro for digitais lógicos.
+ PTS - Se o ponto resultante do filtro for totalizadores lógicos.

### PARC
Chave estrangeira do ponto físico que compõe a parcela do filtro.

### PNT
Chave estrangeira do ponto físico resultante do filtro. Relacionamento 1 -> 1 entre as entidades de pontos físicos (PAF, PDF ou PTF. Depende do valor de TPPNT).

Os tipos de filtros simples ou compostos para pontos digitais implementados para o protocolo são: FIL1/FIL9 - filtro OR de pontos digitais que pode ser utilizado, por exemplo, para agrupamento de pontos de proteção gerando SOE apenas na primeira atuação e na última normalização.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	RFC
	     ORDEM= 1
	      PARC= 2i11-M3PPDIS5$ST$Op
	       PNT= SEE:02I1:00:ONS3
	    TPPARC= PDF
	     TPPNT= PDS
	
	RFC
	     ORDEM= 2
	      PARC= 2i12-OUT2GGIO3$ST$Ind01
	       PNT= SEE:02I1:00:ONS3
	    TPPARC= PDF
	     TPPNT= PDS
	
	RFC
	     ORDEM= 3
	      PARC= 2i12-OUT2GGIO3$ST$Ind06
	       PNT= SEE:02I1:00:ONS3
	    TPPARC= PDF
	     TPPNT= PDS

- - -