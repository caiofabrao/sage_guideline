[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/12                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/12    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Alarmes e Eventos
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para configuração de alarmes e eventos.

# Entidades DATs Necessárias
![Alt text](imgs/alarmes_eventos_der.png)

+ **[MAP](#markdown-header-map):** Macro Alarmes
+ **[E2M](#markdown-header-e2m):** Relacionamento Ponto de Tempo Real ou Ocorrência x Macro Alarmes
+ **[OCR](#markdown-header-ocr):** Ocorrências do Sistema
+ **[TELA](#markdown-header-tela):** Telas associadas a Ocorrências

- - -

# MAP
Define todos os botões de macro alarmes existentes no sistema.

Obrigatório criar uma macro Geral, responsável por captar OCR.

## Atributos
### ID
Identificador do Macro Alarme.

### NARRT
Narrativa.

### ORDEM
Ordem de posicionamento do Macro Alarme na tela.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	MAP
		   ID= 02I1
		NARRT= Alarme Bay 02I1
		ORDEM= 01

- - -

# E2M
Especifica o relacionamento entre a entidade MAP e as entidades PAS, PDS, PTS e OCR.

Utilizada para criar relacionamentos de OCR com áreas macro de Alarme, por exemplo. Assim uma determinada ocorrência irá aparecer na macro mesmo que a ligação do ponto que a ocasionou esteja em outra macro.

## Atributos
### IDPTO
Identificador do ponto de Tempo Real ou Ocorrência.

### MAP
Chave estrangeira do Macro Alarme à qual a classe pertence. Relacionamento 1 -> n entre as entidades MAP e E2M.

### TIPO
Tipo do ponto aquisitado.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	E2M
		IDPTO= OCR_PAS01
		  MAP= GERAL
		 TIPO= OCR
	
	E2M
		IDPTO= GERAL_PDS
		  MAP= GERAL
		 TIPO= PDS

- - -

# OCR

Configura todas as ocorrências existentes no sistema. Existem 2 tipos básicos de ocorrências: as associadas a pontos lógicos do sistema elétrico; e as associadas aos alarmes/eventos do sistema computacional, suporte de comunicação, ações do operador, etc.

## Atributos
### ID
Identificador da ocorrência.

### SEVER
Chave estrangeira da severidade associada a ocorrência. Relacionamento 1 -> n entre as entidades SEV e OCR.

### TEXTO
Texto da ocorrência.

### TELA
Chave estrangeira da tela associada a ocorrência. Relacionamento 1 -> n entre as entidades TELA e OCR.

### TPSON
Sinal sonoro do alarme.

### TPUSR
Tipo do usuário da ocorrência.

### TIPOE
Tipo da ocorrência.

## Exemplo

	OCR
	     CMT =		Inconsistencia ponto duplo 
	      ID =		OCR_DIS01
	    NREP =		3
	   SEVER =		ADVER
	   TEXTO =		Reservado (fechou)
	   TIPOE =		NORML
	   TPSOM =		BREVE
	   TPUSR =		USR
	
	OCR
	     CMT =		Inconsistencia ponto duplo 
	      ID =		OCR_DIS02
	    NREP =		3
	   SEVER =		ADVER
	   TEXTO =		Reservado (abriu)
	   TIPOE =		NORML
	   TPSOM =		BREVE
	   TPUSR =		USR
	
	OCR
	     CMT =		Dupla transicao  0 > 1 e 1 > 0
	      ID =		OCR_DIS03
	    NREP =		3
	   SEVER =		URGEN
	   TEXTO =		Equipamento fechou e abriu
	   TIPOE =		NORML
	   TPSOM =		BREVE
	   TPUSR =		USR
	
	OCR
	     CMT =		Transicao simples 0 > 1
	      ID =		OCR_DIS04
	    NREP =		3
	   SEVER =		URGEN
	   TEXTO =		Equipamento fechou
	   TIPOE =		NORML
	   TPSOM =		BREVE
	   TPUSR =		USR
	
	OCR
	     CMT =		Transicao simples 1 > 0
	      ID =		OCR_DIS05
	    NREP =		3
	   SEVER =		URGEN
	   TEXTO =		Equipamento abriu
	   TIPOE =		NORML
	   TPSOM =		BREVE
	   TPUSR =		USR
	
	OCR
	     CMT =		Dupla transicao  1 > 0 e 0 > 1
	      ID =		OCR_DIS06
	    NREP =		3
	   SEVER =		URGEN
	   TEXTO =		Equipamento abriu e fechou
	   TIPOE =		NORML
	   TPSOM =		BREVE
	   TPUSR =		USR

- - -

# TELA
Configura as elas associadas às ocorrências e instalações.

## Atributos
### ID
Identificador da tela.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TELA
		 ID= SE1_UNI230KV
		CMT= Tela Unifilar 230 kV