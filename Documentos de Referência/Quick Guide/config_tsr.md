[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/23                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/23    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Comunicação via Terminal Server
Neste documento é apresentado um guia rápido dos arquivos utilizados para configurar uma comunicação via terminal server no SAGE.

# Entidades DATs e Arquivos Necessários

+ **[LSC](#markdown-header-lsc):** Ligação SCADA
+ **[TCV](#markdown-header-tcv):** Tipos do Conversor de Protocolo
+ **[TPP](#markdown-header-tpp):** Tipos de Transportadores de Protocolo
+ **[CNF](#markdown-header-cnf):** Configuração da Ligação Física do SCD
+ **[SSC_Amb](#markdown-header-ssc-amb):** 
+ **[TsrDevices](#markdown-header-tsrdevices):**
+ **[Hosts](#markdown-header-hosts):**

- - -

# LSC
Para a comunicação via terminal server, os atributos TCV e TTP devem ser alterados. Os demais atributos seguem configuração usual.

## Atributos

### TCV
Chave estrangeira do tipo de conversor de protocolo utilizado. Relacionamento 1 -> n entre as entidades TCV e LSC.

**Dóminio Válido do TCV para Terminal Server:**

| Protocolo | TTP | Transportador | Descrição |
| ---- | ---- | ---- | ----- |
| LN57 | CXTT3  | iecy  | Transportador em Frames FT3 do IEC/870 sobre TTY |
| I101 | CXTT1  | iec2y  | Transportador Não Balanceado em Frames FT1.2 do IEC/60870 sobre TTY |
| I101 | CXTB1  | iec1y  | Transportador Balanceado em Frames FT1.2 do IEC/60870 sobre TTY |
| DNP3 | CXTTD  | iec3y  | Transportador em Frames FT3-DNP do IEC/870 sobre TTY |
| MODB | YMBUS  | ybus  | Transporte de Frames MODBUS em enlaces TTY |
| ALTUS | ALTUS  | alty  | Transporte de Frames ALTUS AL-1000 em enlaces TTY |
| SPTDS | SPTDS  | sptd  | Transportador de Multiligações em Linhas Seriais Assíncronas |
| MICROLAB | YMLAB  | ylab  | Transportador de Frames do Protocolo Microlab sobre TTY |

### TTP
Chave estrangeira do tipo de transportado de protocolo utilizado. Relacionamento 1 -> n entre as entidades TTP e LSC. O domínio válido desse atributo para comunicação via terminal server está descrito na sessão TTP.

**Domínio Válido do TTP para Terminal Server:**

| Protocolo	| TCV | Descrição |
| ---- | ---- | ---- | ----- |
| CNO | CNVA | Conversor SINSC Modo Mestre |
| LN57 | CNVF | Conversor L&N IEC/60870-5 |
| I101 | CNVG | Conversor IEC/60870-5-101 |
| DNP V3.0 | CNVH | Conversor do Protocolo DNP V3.00 |
| MODB | CNVJ | Conversor do Protocolo ModBus (MODB) |
| ALTUS | CNVK | Conversor do Protocolo ALTUS AL-1000 |
| MICROLAB | CNVL | Conversor do Protocolo MicroLab |

## Exemplo

	LSC
	       GSD= SEE
	        ID= 2T1
	       MAP= 02K1
	      NOME= Ligacao DNP3 TSR - 02T1-F1
	     NSRV1= localhost
	     NSRV2= localhost
	       TCV= CXTTD
	      TIPO= AD
	       TTP= CNVH
	     VERBD= JUL21

- - -

### TCV
Entidade que descreve os tipos dos controladores de protocolo cadastrado no SAGE. Não é preciso alterar essa entidade.

- - -

### TTP
Entidade que descreve os tipos de transportadores de protocolo cadastrado no SAGE. Não é preciso alterar essa entidade.

- - -

# CNF
Para a comunicação via terminal server, o atributo CONFIG deve ser alterado. Os demais atributos seguem configuração usual.

## Atributos

## CONFIG
Configurar o atributo CONFIG com as placas (PlPr) e linhas (LiPr) que serão utilizadas para a comunicação via Terminal Server. Sugere-se que esta configuração seja de forma continua a partir da placa 1 linha 1.

| Terminal Server | Placa | Linha  | Porta   | Dispositivo Associado |
|---- | ---- | ----| ---- | ---- |
| 01 porta        | 1     | 1      | 1       | /dev/ctty1a |
| 04 portas       | 1     | 1 a 4  | 1 a 4   | /dev/ctty1a  até  /dev/ctty1d |
| 08 portas       | 1     | 1 a 8  | 1 a 8   | /dev/ctty1a  até  /dev/ctty1h |
| 16 portas       | 1     | 1 a 16 | 1 a 16  | /dev/ctty1a  até  /dev/ctty1p |
| 32 portas       | 1     | 1 a 16 | 1 a 16  | /dev/ctty1a  até  /dev/ctty1p |
|                 | 2     | 1 a 16 | 17 a 32 | /dev/ctty2a  até  /dev/ctty2p |
| 64 portas       | 1     | 1 a 16 | 1 a 16  | /dev/ctty1a  até  /dev/ctty1p |
|                 | 2     | 1 a 16 | 17 a 32 | /dev/ctty2a  até  /dev/ctty2p |
|                 | 3     | 1 a 16 | 33 a 48 | /dev/ctty3a  até  /dev/ctty3p |
|                 | 4     | 1 a 16 | 49 a 64 | /dev/ctty4a  até  /dev/ctty4p |

## Exemplo

	CNF
		CONFIG= PlPr= 4 LiPr= 1 PlRe= 0 LiRe= 0  Proto= BIN
		    ID= 2T1
		   LSC= 2T1

- - -

# SSC_Amb
Alterar o conteúdo da variável `TTY00` para `ctty1a`, se necessário.

	$SAGE/config/$BASE/sys

## Exemplo

	setenv TTY00 "ctty1a"	# device tty para comunic. c/utr via Terminal server (TS)

- - -

# TsrDevices
Configurar as portas do Terminal Server, utilizando, como referencia, a configuração feita na entidade CNF. Caso a base não tenha esse arquivo, utilize o modelo em `$SAGE/config/demo_ems/sys`.

	$SAGE/config/$BASE/sys

## Exemplo

	/dev/ctty1a:prts:tsr:1:socket:	# Placa 1 Linha 1 pela porta 1)
	/dev/ctty1b:prts:tsr:2:socket:	# Placa 1 Linha 2 pela porta 2)
	/dev/ctty1c:prts:tsr:3:socket:	# Placa 1 Linha 3 pela porta 3)
	/dev/ctty2a:prts:tsr:4:socket:	# Placa 2 Linha 1 pela porta 4)

- - -

# Hosts
Definir o endereço IP e o host-name do Terminal Server.

## Exemplo

	161.79.57.26    tsr

- - -

# Tsr.conf
Configurar o arquivo `tsr.conf` 

	$SAGE/config/$BASE/sys

## Parâmetros
### SECCAO TS
Configuração do hostname, configurado no arquivo hosts, do terminal serve para o(s) transportador(es) definido(s) na coluna TTP.

### TTP
Código dos transportadores disponíveis.

### PLACA
### LINHA
### PORTA SOCKET
### OPMSK
### WDT-IDLE
### TIME_OUT
### NTENT
### END_L
### PAR_OPCS
### COMENTARIO

- - -

# Driver Terminal Server
## Instalação
+ Entrar no modo super-usuário
+ Instalar o driver
	+ `$SAGE/drivers/instala_tsr`

**Obs:** Após a instalação, o lançamento do pseudo driver tty, ocorrerá automaticamente a cada inicialização do sistema operacional.

## Reinstalação
+ Como usuário sage
+ Parar o processo tsrdev
	+ `tsrdev stop`
+ Entrar no modo super-usuário
+ Desinstalar o driver
	+ `$SAGE/drivers/desinstala_tsr`
+ Sair do modo super usuário para executar a manutenção
+ Entrar no modo super-usuário
+ Instalar o driver
	+ `$SAGE/drivers/instala_tsr`

## Partida e Parada do Driver
| Comando                 | Descrição |
| ----                    | ---- |
| `tsrdev start`          | Ativa todas as portas configuradas no arquivo tsrdevices. |
| `tsrdev start <device>` | Ativa somente a porta (device) informada. |
| `tsrdev stop`           | Desativa todas as portas configuradas no arquivo tsrdevices. |
| `tsrdev stop <device>`  | Desativa somente a porta (device) informada. |

**Exemplo:**

	~ tsrdev start /dev/ctty1a
	Starting /dev/ctty1a <<==>> tsr:1 interface

## Monitoramento de Log
| Mensagem | Descrição |
| ---- | ---- |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Using /dev/pts/7 pseudo-tty                                   | Indica a pseudo-tty do sistema que vai utilizar. |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Using 161.79.57.26:31001 socket                               | Indica qual IP e porta vai utilizar para a conexão socket com o Terminal Server. |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Can't initiate connec on a socket:(recv) Connec reset by peer | Falha seguida de reset, na primeira tentativa de conexão socket com o Terminal Server. |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Trying again ...                                              | Tenta conexão com o Terminal Server novamente. |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Opening /dev/pts/7pseudo-tty                                  | Conexão bem sucedia, esperando conexão com transportador do SAGE. |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Closing /dev/pts/7pseudo-tty                                  | Entrada do transporte e protocolo do SAGE. |
| sage_tsr[8668]: tsrsock /dev/ctty1a: Normal shutdown (SIGTERM)                                     | O processo que cuidava da conexão socket com o TS, foi encerrado. |

- - -