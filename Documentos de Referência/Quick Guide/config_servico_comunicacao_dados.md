[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/12                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/12    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Serviço de Comunicação de Dados
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para configuração do serviço de comunicação de dados.

# Entidades DATs Necessárias
![Alt text](imgs/servico_com_dados_der.png)

+ **[TCV](#markdown-header-tcv):** Tipos de Conversor de Protocolo
+ **[TTP](#markdown-header-ttp):** Tipos de Transportadores de Protocolo
+ **[GSD](#markdown-header-gsd):** Gateway SCADA
+ **[CXU](#markdown-header-cxu):** Conexões de Comunicação com UTRs e Canais
+ **[ENU](#markdown-header-enu):** Enlaces de Conexões com UTRs
+ **[UTR](#markdown-header-utr):** Unidade Terminal Remota

- - -

# TCV
Configura os conversores de protocolo existentes no sistema. Dat padronizado.

## Atributos
### ID
Identificador do Conversor.

### DESCR
Descrição do conversor.

### NSEQ
Número sequencial do conversor.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TCV
		   ID= CNVM
		DESCR= Conversor IEC/60870-5-104 (104)
		NSEQ= 13

- - -

# TTP
Configura os transportadores de protocolo existentes no sistema. Dat Padronizado.

## Atributos
### ID
Identificador do transportador.

### DESCR
Descrição do transportador.

### NSEQ
Número sequencial do transportador.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TTP
		   ID= Cx104
		DESCR= Transportador TCP do IEC/60870-5-104
		NSEQ= 20

- - -

# GSD
Configura os gateways do sistema.

## Atributos
### ID
Identificador do gateway.

### NO1
Nó 1 do gateway.

### NO2
Nó 2 do gateway.

### NOME
Nome do gateway.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	GSD
		  ID= SE1
		 NO1= srv1_dc_se1
		 NO2= srv2_dc_se1
		NOME= Subestação Elétrica 1 (SE1)

- - -

# CXU
Configura as conexões de comunicação dos gateways.

## Atributos
### ID
Identificador da conexão de comunicação. Deve-se usar um identificação de 2 a 3 caracteres identificando a subestação supervisionada. Recomenda-se utilizar o mesmo identificador utiliza na CNF referente a conexão.

### AQANL
Tempo em segundos da aquisição analógica. Se omitido, é utilizado o valor padrão 200.

### AQPOL
Tempo em segundos do polling words. Se omitido, é utilizado o valor padrão 100.

### AQTOT
Tempo em segundos da aquisição de totalizadores. Se omitido, é utilizado o valor padrão 6000.

### FAILP
Tempo em segundos do failover para o canal primário. Se omitido, é utilizado o valor padrão 2880000.

### FAILR
Tempo em segundos do failover para o canal reserva. Se omitido, é utilizado o valor padrão 30000.

### GSD
Chave estrangeira do Gateway-SCADA do SAGE (par de servidores) ao qual a conexões de comunicação SCADA está associado. Relacionamento 1 -> n entre as entidades GSD e CXU.

### INTGR
Tempo em segundos da integridade do canal. Se omitido, é utilizado o valor padrão 18000.

### NFAIL
Número de tentativas de failover. Se omitido, é utilizado o valor padrão 5.

### ORDEM
Deve ser preenchido com um número sequencial de 1 a 9999 com a ordem da conexão UTR dentro do GSD.

### SFAIL
Tempo em segundos do sincronismo de failover. Se omitido, é utilizado o valor padrão 200.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CXU
		   ID= BAY
		  GSD= SE1
		AQANL= 200
		AQPOL= 200
		AQTOT= 60000
		FAILP= 0
		FAILR= 0
		INTGR= 180000
		NFAIL= 5
		ORDEM= 1
		SFAIL= 200

- - -

# ENU
Configura os enlaces de todas as conexões de todos os gateways do sistema.

## Atributos
### ID
Identificador do enlace. Deve-se usar a concatenação por undescore do CXU com a inicial da ORDEM.

### CXU
Chave estrangeira da Conexão de Comunicação com UTR e CANAL ao qual o enlace está associado. Relacionamento 1 -> 2 entre as entidades CXU e ENU.

### ORDEM
Ordem de prioridade do enlace. Deve-se utilizar **PRI** para primário e **REV** para reserva.

### VLUTR
Velocidade em bits por segundo do canal. Se omitido, é utilizado o valor padrão 2400.

### TRANS
Tempo em segundos do temporizador de transmissão. Se omitido, é utilizado o valor padrão 50.

### TDESC
Tempo em segundos do descanso para reinício. Se omitido, é utilizado o valor padrão 3.

## Exemplo

	ENU
		   ID= BAY_P
		  CXU= BAY
		ORDEM= PRI
		TRANS= 4
		TDESC= 2
		VLUTR= 9600
	
	ENU
		   ID= BAY_R
		  CXU= BAY
		ORDEM= REV
		TRANS= 4
		TDESC= 2
		VLUTR= 9600
- - -

# UTR
Configura as UTRs de todas as conexões de todos os gateways do sistema, tanto as UTRs físicas de aquisição como as virtuais de distribuição.

### ID
Identificador da unidade terminal. Recomenda-se a concatenação por underscore do CNF com a inicial da ORDEM.

### CNF
Chave estrangeira da configuração física à qual a unidade terminal está associada. Relacionamento 1 -> 2 entre as entidades CNF e UTR.

### CXU
Chave estrangeira da conexão da UTR à qual a unidade terminal está associada. Relacionamento 1 -> n entre as entidades CXU e UTR.

### ENUTR
Endereço da UTR.

### NTENT
Número de tentativas de religação antes de gerar falha de comunicação.

### ORDEM
Ordem de prioridade para utilização. Deve-se utilizar **PRI** para primário e **REV** para reserva.

### RESPT
Tempo limite em segundos para resposta.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	UTR
		   ID= BAY_P
		ORDEM= PRI
		  CNF= BAY
		  CXU= BAY
		ENUTR= 2
		NTENT= 3
		RESPT= 1500
	
	UTR
		   ID= BAY_R
		ORDEM= REV
		  CNF= BAY
		  CXU= BAY
		ENUTR= 0
		NTENT= 3
		RESPT= 1500

- - -
