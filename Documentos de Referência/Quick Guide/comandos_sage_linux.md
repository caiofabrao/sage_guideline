[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/05/13                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/05/13    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

- - -

# Sumario
+ [Comandos do SAGE](#markdown-header-comandos-do-sage)
	+ [Auditoria](#markdown-header-auditoria)
	+ [Instalacao/Backup](#markdown-header-instalacao-backup)
	+ [Operacao](#markdown-header-operacao)
	+ [Programas](#markdown-header-programas)
+ [Comandos do Linux](#markdown-header-comandos-do-linux)
	+ [Basicos](#markdown-header-basicos)
	+ [Arquivos e Diretorios](#markdown-header-arquivos-e-diretorios)
		+ [Gerenciamento](#markdown-header-gerenciamento)
		+ [Visualizacao de Arquivos](#markdown-header-visualizacao-de-arquivos)
		+ [Manipulacao de Arquivos](#markdown-header-manipulacao-de-arquivos)
	+ [Auditoria do Sistema](#markdown-header-auditoria-do-sistema)
	+ [Adiministracao do Sistema](#markdown-header-adiministracao-do-sistema)
	+ [Variaveis de Ambiente](#markdown-header-variaveis-de-ambiente)
+ [Editor vi](#markdown-header-editor-vi)
+ [Variaveis de Ambiente SAGE](#markdown-header-variaveis-de-ambiente-sage)

- - -

# Comandos do SAGE
A descrição completa está nos Documentos de Referência em: **/SAGE/Scripts_SAGE.pdf**

## Auditoria
| Comando | Descrição |
| ---- | ---- |
| `slog` | Abre o arquivo de log do SAGE, através dele você pode descobrir problemas de comunicação, protocolos e outros. Mantenha o log sempre aberto |
| `tail -f /var/log/messages`| Monitora o arquivo de log do SAGE no terminal |
| `var` | Mostra as variáveis de ambiente do SAGE |
| `info-tr` | Mostra comandos do SAGE explicando sua utilização. Pode ser usado para executar vários tipos de comandos via SHELL, bem como coletar dados dos pontos |
| `info-tr Controle ctrl <TAG> 1` | Abre o equipamento com a TAG informada |
| `info-tr Le dig <TAG>` | Le informações do ponto digital que possuí a TAG informada |
| `\mmf <protocolo> rx` | Monitora as mensagens trocadas entre o transportador e o equipamento. O protocolo deve ser no formato i61850, i104, etc. |
| `\Ligador *` | Verifica todos os IDs errados em todas as telas do diretório telas |
| `Tabular -ent lia` | Abre tabular com informações do IED |

## Instalação/Backup
| Comando | Descrição |
| ---- | ---- |
| `instala_sage <nome_base>` | Instala o sage de forma simples e interativa |
| `habilita_base <nome_base>` | Alterar para a base nomeada |
| `habilita_base <nome_base> __SEMDSP__` | Alterar para a base nomeada sem ativar o som |
| `remove_base <nome_base>` | Remove uma base especifica do SAGE |
| `zipconfig <nome_base> -bz2` | Faz um backup de toda a estrutura da base selecionada do SAGE com maior compressão (Salva na pasta $SAGE) |
| `criasagecnf` | Faz um backup de toda a estrutura da base atual do SAGE sem data (Salva na pasta $SAGE) |
| `criacnf_com_data` | Faz um backup de toda a estrutura da base atual do SAGE com data (Salva na pasta $SAGE). Os backups do mesmo dia serão substituídos |
| `sincconfig <arquivo> -h <host>` | Copia o arquivo para o servidor remoto determinado |

## Operação
| Comando | Descrição |
| ---- | ---- |
| `habilita_postgres` | Ativa o Banco de Dados do SAGE |
| `instala_calculos` | Entre na pasta cálculos e execute este comando sempre que instalar uma nova base ou alterar o arquivo calculos.c |
| `AtualizaBD fria fonte` | Recompila o SAGE após alterações |
| `!A_ ou _!Atua` | Comando alternativo ao AtualizaBD, após executa-lo pela primeira vez |
| `cat /tmp/sage/log/STI_cargbf.log` | Visualiza log de erros do sage após compilação |
| `ativa gcd` | Ativa o SAGE (gcd: Gerencia de configuração distribuída) |
| `desativa gcd` | Desativa o SAGE (gcd: Gerencia de configuração distribuída) |
| `ativasage` | Ativa o SAGE na rede |
| `sincconfig <nome_arquivo> -s -x` | Comando para sincronizar todas as maquinas na rede, onde o "-s" copia o arquivo e o "-x" descompacta |

## Programas
| Comando | Descrição |
| ---- | ---- |
| `SigDraw` | Desenvolvedor de tela de supervisão |
| `VisorAcesso` | Abre a Tela de Visor de Acesso do Sage |
| `VisorTelas` | Executa o supervisório |
| `CadUsuario` | Cadastramento de Usuários do SAGE |
| `Ligador -g` | Liga todas as telas com o banco de dados |

- - -

# Comandos do Linux
## Básicos
| Comandos | Descrição |
| ---- | ---- |
| `<CTRL> \+ <L>` | Limpa a tela do shell |
| `su` | Habilita o modo de user usuário |
| `sudo <comando>` | Realiza o comando como super usuário (não é confiável no SAGE) |
| `pwd` | Mostra o diretório atual |
| `cd <diretorio>` | Navega pelos diretórios |
| `ls` | Lista os arquivos e pastas do diretório atual|
| `ls -d */` | Lista apenas diretórios |
| `ls -la` | Lista os arquivos de forma ordenada e com data |
| `alias ls="ls -al —color"` | ls irá imprimir colorido na tela (facilita a visualização) |

## Arquivos e Diretórios
### Gerenciamento
| Comandos | Descrição |
| ---- | ---- |
| `cp <arquivo> <destino>` | Copia um arquivo |
| `mkdir <diretorio>` | Cria um novo diretório |
| `rmdir <diretorio>` | Deleta um diretório |
| `rm <arquivo>` | Deleta um arquivo |
| `rm -rf` | Deleta todos os arquivos e subdiretórios de um determinado diretório |
| `mv <origem>/<arquivo> <destino>/<arquivo>` | Move e/ou renomeia um arquivo |
| `chmod +x <script.sh>` | Adiciona a permissão de execução ao script informado |

### Visualização de Arquivos
| Comandos | Descrição |
| ---- | ---- |
| `cat <arquivo>` | Visualiza conteúdo de um arquivo |
| `less <arquivo>` | Visualiza conteúdo de um arquivo permite utilizar as setas para rolar o texto e outra ferramentas que facilita a leitura |
| `find -name "*.BAK"` | Procura Texto (Pesquisar o comando) |
| `locate <nome_arquivo>` | Procura um arquivo especifico, este comando precisa ser instalado antes de usa-lo |
| `grep <palavra_chave> <arquivo>` | Exibe as linhas em que uma determinada palavra ocorre dentro de um dado arquivo |
| `grep -v <palavra_chave> <arquivo>` | Ignora as linhas em que uma determinada palavra ocorre dentro de um dado arquivo, mostrando o **restante** do arquivo |
| `grep -ra <palavra_chave> *` | Parâmetros para localizar uma palavra recursivamente em todos os arquivos do diretório atual |
| `egrep <expressao_regular> <arquivo>` | Atalho do comando `grep -e` que permite utilizar expressões regulares como padrão de busca |
| `fgrep <metacaracter> <arquivo>` | Atalho do comando `grep -f` que permite utilizar metacaracteres como padrão de busca |

### Manipulação de Arquivos
| Comandos | Descrição |
| ---- | ---- |
| `vi <arquivo>` | Edita um arquivo com o editor de textos "Vi" |
| `sed -i 's,<string_atual>,<string_nova>,g' <arquivo>` | Substitui uma determinada string dentro de um dado arquivo |
| `lpstat -d` | Mostra o nome da impressora ativa |
| `lpr <arquivo> - P <impressora>` | Imprime o arquivo na impressora padrão |

## Auditoria do Sistema
| Comandos | Descrição |
| ---- | ---- |
| `cat /proc/meminfo` | Mostra detalhes sobre a quantidade de dados em buffers, cache e memória virtual (disco). |
| `cat /etc/redhat-release` | Para saber a versão do CentOS |
| `df -h` | Comando utilizado para verificar as partições do Linux bem como o espaço utilizado por cada uma delas |
| `mount -t vfat /dev/sdb1 /media/pen` | Monta pen drive (Este comando pode não ser exatamente assim na sua instalação, carece de mais informações) |
| `mount -t vboxsf bases /mnt/bases` | Montar pasta compartilhada do Virtualbox (Criar as pastas antes) |
| `konqueror` | Abre o konqueror, explorador de arquivos |

## Administração do Sistema
| Comandos | Descrição |
| ---- | ---- |
| `init 0` | Desliga o Linux (Runlevel 0) |
| `shutdown -h now ` | Desliga o sistema  (identico ao `init 0`)|
| `init 6` | Reinicia o Linux (Runlevel 6) |
| `shutdown -r now ` | Reinicia totalmente o sistema (identico ao `init 6`) |
| `reboot` | Alias do comando acima |
| `gcd_shell` | Recurso para habilitar ou desabilitar processos noh/rede via comando Shell (Siga as informações da Tela)|
| `neat` |  Abre as configurações de rede. |
| `psax` | Script em cshell que mostra todos os processos do sage em execução |
| `ps aux` | Mostra todos os processos rodando no momento |
| `top` | Mostra todos os processos ativos, utilização de memória, processador e disco, com este comando é possível descobrir o PID de um determinado processo |
| `kill <PID>` | Termina um processo (Caso queira saber o PID de um processo, use o comando TOP ou HTOP, este ultimo precisa ser instalado) |
| `killall <nome_processo>` | Termina um processo |

## Variáveis de Ambiente
| Comandos | Descrição |
| ---- | ---- |
| `env` | Mostra na tela do usuário todas as variáveis de ambiente e seus conteúdos que estão disponíveis para a seção atual do usuário. |
| `setenv <VARIAVEL> <conteudo>` | Cria uma variável de ambiente com nome e conteúdo. |
| `unsetenv <VARIAVEL>` | Destrói uma variável de ambiente, o seu uso é parecido com o do setenv. |

- - -

# Editor vi
**Uso:** `vi <arquivo>`

**Exemplo:** `vi /etc/hosts`

Para edições rápidas, pode-se utilizar o editor interno do terminal com o comando `vi`. De início, ele exibirá o arquivo em modo de leitura. A tabela a seguir apresenta os principais comandos do editor.


| Tecla | Ação |
| ---- | ---- |
| **INSERT** | Entra no modo de edição de arquivo |
| **ESC** | Sai do modo de edição de arquivo |

| Comando | Ação |
| --- | --- |
| `:w` | Salva as alterações no arquivo |
| `:q` | Sai do editor vi |
| `:wq` | Salva e sai |
| `:q!` | Sai ignorando as alterações feitas |

- - -

# Variáveis de Ambiente SAGE
| Variável | Descrição |
| ---- | ---- |
| `$SAGE`	 | Diretório raiz do SAGE (/export/home/sage/sage) |
| `$BASE`	 | Nome da base SAGE ativa |
| `$BD/dados` | Diretório com os .dat que geram a base de dados (/export/home/sage/sage/config/$BASE/bd/dados) |
| `$TELAS`	| Diretório com as telas (/export/home/sage/sage/config/$BASE/telas) |
| `$IHM`	  | Diretório de configuração da IHM do SAGE (/export/home/sage/sage/config/$BASE/ihm) |
| `$LOG`	  | Diretório com os logs de processos e protocolos |
| `$ARQS`	 | Diretório com o histórico de alarmes, eventos e auditorias |
| `$PWD`	  | Diretório atual |

- - -

# Pipe
**Uso:** `<comando1> | <comando2>`

**Exemplo:** `tail -f /var/log/messages | grep 'IED_1'` 

Muitos comandos Linux podem ser combinados utilizando o pipe (`|`), assim encadeando a saída de um comando como entrado do próximo.

Isso se mostra extremamente útil quando se deseja filtrar o monitoramento de um arquivo de log, por exemplo.

- - - 

# Monitoramento de Log

Utilizando `tail -f <arquivo>` para monitorar um arquivo mais o pipe com o comando `grep <palavra_chave>`, que filtra as linhas que contém a palavra chave, é possível monitorar apenas as linhas que contém a palavra chave no arquivo.

- - -

# Opções do GREP

| Opção | Descrição |
| ---- | ---- |
| `-i` | Ignora case do palavra chave. |
| `-v` | Invert o match. Seleciona as linhas que não contêm a palavra chave. |
| `-e` | Aceita expressões regulares como palavra chave. |
| `-n` | Emite o número da linha na saída. |
| `-h` | Inibi o nome do arquivo na saída. |
| `-H` | Emite o nome do arquivo na saída. |
| `-c` | Exibe apenas a contagem de ocorrências por aquivo como saída. |
| `-l` | Exibe apenas o nome dos arquivos que contêm a palavra chave. |
| `-L` | Exibe apenas o nome dos arquivos que não contêm a palavra chave. |

- - -

# Opções do LS

| Opção | Descrição |
| ---- | ---- |
| `-a` | Não ignora os arquivos ocultos (aqueles iniciados com "."). |
| `-B` | Não lista os arquivos de backup (aqueles terminados com "~"). |
| `-I` | Não lista as entidades que seguem o padrão. |
| `-l` | Formata a lista com a descrição dos arquivos . |
| `-lh` | Exibe os tamanhos em formato compacto (ex., 1K, 234M, 2G). |
| `-S` | Organiza pelo tamanho do arquivo . |
| `-t` | Organiza pela hora de modificação, mais recente primeiro. |
| `-x` | Lista as entradas por linhas em vez de colunas. |
| `-X` | Sort alphabetically by entry extension. |
| `-1` | Lista um arquivo por linha. |