[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/23                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/23    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Comunicação via Porta COM
Neste documento é apresentado um guia rápido dos arquivos utilizados para configurar uma comunicação via porta COM no SAGE.

# Entidades DATs e Arquivos Necessários

+ **[LSC](#markdown-header-lsc):** Ligação SCADA
+ **[TCV](#markdown-header-tcv):** Tipos do Conversor de Protocolo
+ **[TPP](#markdown-header-tpp):** Tipos de Transportadores de Protocolo
+ **[CNF](#markdown-header-cnf):** Configuração da Ligação Física do SCD
+ **[SSC_Amb](#markdown-header-ssc-amb):**

- - -

# LSC
Para a comunicação via porta COM, os atributos TCV e TTP devem ser alterados. Os demais atributos seguem configuração usual.

##Atributos
### TCV
Chave estrangeira do tipo de conversor de protocolo utilizado. Relacionamento 1 -> n entre as entidades TCV e LSC.

**Dóminio Válido do TCV para Terminal Server:**

| Protocolo | TTP | Transportador | Descrição |
| ---- | ---- | ---- | ----- |
| LN57 | CXTT3  | iecy  | Transportador em Frames FT3 do IEC/870 sobre TTY |
| I101 | CXTT1  | iec2y  | Transportador Não Balanceado em Frames FT1.2 do IEC/60870 sobre TTY |
| I101 | CXTB1  | iec1y  | Transportador Balanceado em Frames FT1.2 do IEC/60870 sobre TTY |
| DNP3 | CXTTD  | iec3y  | Transportador em Frames FT3-DNP do IEC/870 sobre TTY |
| MODB | YMBUS  | ybus  | Transporte de Frames MODBUS em enlaces TTY |
| ALTUS | ALTUS  | alty  | Transporte de Frames ALTUS AL-1000 em enlaces TTY |
| SPTDS | SPTDS  | sptd  | Transportador de Multiligações em Linhas Seriais Assíncronas |
| MICROLAB | YMLAB  | ylab  | Transportador de Frames do Protocolo Microlab sobre TTY |

### TTP
Chave estrangeira do tipo de transportado de protocolo utilizado. Relacionamento 1 -> n entre as entidades TTP e LSC. O domínio válido desse atributo para comunicação via terminal server está descrito na sessão TTP.

**Domínio Válido do TTP para Terminal Server:**

| Protocolo	| TCV | Descrição |
| ---- | ---- | ---- | ----- |
| CNO | CNVA | Conversor SINSC Modo Mestre |
| LN57 | CNVF | Conversor L&N IEC/60870-5 |
| I101 | CNVG | Conversor IEC/60870-5-101 |
| DNP V3.0 | CNVH | Conversor do Protocolo DNP V3.00 |
| MODB | CNVJ | Conversor do Protocolo ModBus (MODB) |
| ALTUS | CNVK | Conversor do Protocolo ALTUS AL-1000 |
| MICROLAB | CNVL | Conversor do Protocolo MicroLab |

## Exemplo

	LSC
	       GSD= SEE
	        ID= 2C1
	       MAP= 02C1
	      NOME= Ligacao DNP3 COM - 02C1-F1
	     NSRV1= localhost
	     NSRV2= localhost
	       TCV= XTTD
	      TIPO= AD
	       TTP= CNVH
	     VERBD= JUL21

- - -

### TCV
Entidade que descreve os tipos dos controladores de protocolo cadastrado no SAGE. Não é preciso alterar essa entidade.

- - -

### TTP
Entidade que descreve os tipos de transportadores de protocolo cadastrado no SAGE. Não é preciso alterar essa entidade.

- - -

# CNF
Para a comunicação via porta COM, o atributo CONFIG deve ser alterado. Os demais atributos seguem configuração usual.

##Atributos
## CONFIG
Configurar o atributo CONFIG com as placas (PlPr) e linhas (LiPr) que serão utilizadas para a comunicação via Terminal Server. Sugere-se que esta configuração seja de forma continua a partir da placa 1 linha 1.

| Placa | Linha | Porta Serial| Dispositivo Associado |
| ----  | ----  | ----   | ---- |
| 1     | 1     | COM1   | /dev/ttyS0 |
| 1     | 2     | COM2   | /dev/ttyS1 |

## Exemplo

	CNF
		CONFIG= PlPr= 1 LiPr= 1 PlRe= 0 LiRe= 0  Proto= BIN
		    ID= 2C1
		   LSC= 2C1

- - -

# SSC_Amb
Alterar o conteúdo da variável `TTY00` para `ttyS0`, se necessário.

	$SAGE/config/$BASE/sys

## Exemplo

	setenv TTY00 "ttyS0"	# device tty para comunic. c/utr via Terminal server (TS)

- - -

# Permissão de Execução
É preciso adicionar o privilégio de execução dos dispositivos TTY das portas COM.

+ Entre como super-usuário
+ Adicione a permissão de execução aos dois dispositivos TTY
	+ `chmod 666 /dev/ttyS0`
	+ `chmod 666 /dev/ttyS0`
	
- - -

# Teste da Porta COM
Para testar se a porta está funcionando corretamente, confeccione um conector de loop-back (como no diagrama abaixo) e conecte na Porta COM configurada. Em um terminal, execute o comando `tlop /dev/ttyS0 (COM1)`. O programa deverá imprimir no terminal uma série de ponto (..........),  se isso ocorrer, é sinal que a porta está funcionando corretamente.

	   DB25			   DB9 
	 
	(RTS) 4  +		+ (RTS)	7
		     |		|
	(CTS) 5  +		+ (CTS)	8
	(DTR) 20 +		+ (DTR)	4
		     |   OU	|
	(DSR) 6  +		+ (DSR)	6
		     |		|
	(DCD) 8  +		+ (DCD)	1
	(TX)  2  +		+ (TX) 	2
		     |		|	
	(RX)  3  +		+ (RX)	3

- - -