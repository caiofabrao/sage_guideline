[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/19                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/19    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Pontos Físicos
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para configuração dos Pontos Físicos.

# Entidades DATs Necessárias
![Alt text](imgs/sdd_der.png)

+ **[TDD](#markdown-header-tdd):** Terminal de Distribuição de Dados
+ **[PAD](#markdown-header-pad):** Pontos de Medição Analógica de Distribuição
+ **[PDD](#markdown-header-pdd):** Pontos de Digitais de Distribuição
+ **[PTD](#markdown-header-ptd):** Pontos de Medição de Totalização de Distribuição

- - -

# TDD
Definem grupos de pontos lógicos de distribuição de uma mesma funcionalidade.

## Atributos
### ID
Identificador do terminal de distribuição

### NOME
Nome do terminal.

### LSC
Chave estrangeira da ligação (LSC) a qual o terminal pertence. Relacionamento 1 -> n entre as entidades LSC e TDD.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TDD
		  ID= COD
		NOME= Term. Distribuicao DNP3
		 LSC= DCOD

- - -

### 4.2.15. PAD
# PAD
Configura todos os pontos analógicos de distribuição de todas as TDDs de todas as LSCs de distribuição ou mistas.
	
## Atributos
### ID
Ponto de distribuição analógico

### PAS
Chave estrangeira do ponto de medição analógica a ser distribuído. Relacionamento 1 -> 1 entre as entidades PAS e PAD.

### TDD
Chave estrangeira do terminal de distribuição a qual o ponto de distribuição analógico pertence. Relacionamento 1 -> n entre as entidades TDD e PAD.

## Exemplo

	PAD
		 ID= COD:02T1:MVAR
		PAS= BAY:02T1:MVAR
		TDD= COD

- - -

# PDD
Configura todos os pontos digitais de distribuição de todas as TDDs de todas as LSCs de distribuição ou mistas.

## Atributos
### ID
Ponto de distribuição digital.

### PAS
Chave estrangeira do ponto de medição analógica a ser distribuído. Relacionamento 1 -> 1 entre as entidades PDS e PDD.

### TDD
Chave estrangeira do terminal de distribuição a qual o ponto de distribuição digital pertence. Relacionamento 1 -> n entre as entidades TDD e PDD.

## Exemplo

	PDD
		 ID= COD:12D4-1:52
		PDS= BAY:12D4-1:52
		TDD= COD

- - -

# PTD
Configura todos os pontos totalizados de distribuição de todas as TDDs de todas as LSCs de distribuição ou mistas.

## Atributos
### ID
Ponto de distribuição totalizado.

### PAS
Chave estrangeira do ponto totalizado a ser distribuído. Relacionamento 1 -> 1 entre as entidades PTS e PTD.

### TDD
Chave estrangeira do terminal de distribuição a qual o ponto de distribuição totalizado pertence. Relacionamento 1 -> n entre as entidades TDD e PTD.

## Exemplo

	PTD
		 ID= COD:02I1:F1:KM
		PTS= SEE:02I1:F1:KM
		TDD= COD

- - -