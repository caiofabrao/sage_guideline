[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/10                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/10    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Protocolo IEC/61850
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados pelo protocolo IEC/61850.

- - -

# Estrutura do Endereço IEC/61850

\[PysicalDevice\]\[LogicalDevice\]/\[LogicalNode\]$\[FunctionalConstraint($)\]$\[DataObject($)\]

- - -

# Entidades DATs Necessárias
+ **[CNF](#markdown-header-cnf):** Configuração de Ligação Física do SCD
+ **[NV1](#markdown-header-nv1):** Nível 1 da Configuração Física
+ **[NV2](#markdown-header-nv2):** Nível 2 da Configuração Física
+ **[TN1](#markdown-header-tn1):** Tipo de Entidade Física do Nível 1
+ **[TN2](#markdown-header-tn2):** Tipo de Entidade Física do Nível 2
+ **[PAF](#markdown-header-paf):** Ponto Analógico Físico
+ **[PDF](#markdown-header-psf):** Ponto Digital Físico
+ **[PTF](#markdown-header-ptf):** Ponto Totalizador Físico
+ **[CGF](#markdown-header-cgf):** Ponto de Controle Físico
+ **[RFI](#markdown-header-rfi):** Relacionamento de Filtros Simples
+ **[RFC](#markdown-header-rfc):** Relacionamento de Filtros Compostos
+ **[LSC](#markdown-header-lsc):** Ligação SCADA com o IED
+ **[MUL](#markdown-header-mul):** Multiligação com o IED
+ **[ENM](#markdown-header-enm):** Enlace de Multiligação
+ **[TAC](#markdown-header-tac):** Terminal de Aquisição e Controle

- - -

# CNF
Configura as CNFs associadas às LSCs e MULs do sistema. Cada CNF está associada à aquisição e controle feitos pelo cliente SAGE para/nos os dados de um IED.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 18.

## Atributos
### CONFIG
Esse atributo deve ser preenchido com uma string que especifica o endereçamento do stack OSI, algumas temporizações e características requeridas à parametrização do protocolo SAGE. A sintaxe da string é composta por duas partes opcionais e uma obrigatória.

A primeira parte opcional configura o stack OSI. Em linhas gerais podem ser omitido ou usado os valores padrões

	ApTitle= 1 1 10 / 1 1 10 AeQ= 1 PS= 1 / 1 SS= 1 TS= 1 

A parte obrigatória é composta por

	IDAD= ti KEEP= tk NREP= nr TOUT= to MPDU= o1 OPMSK= o2 GOOSE= o3

onde:

+ ti é o tempo, em segundos, para efetuar a integridade dos dados dos DataSets. Recomendado 1800 segundos
+ tk é o tempo, em segundos, para efetuar o keep-alive na associação com mensagens IdentifyRequest do MMS
+ nr é o valor máximo de Requests sem o recebimento do respectivo Response antes de declarar a associação com o IED desfeita
+ to é o tempo, em segundos, para aguardar a Response a um Request enviado ao IED remoto
+ o1 é o número máximo de itens de DataSet que são incluídos pelo SAGE em um DataSet quando ele está configurado para criar DataSetsdinâmicos ou na construção dos DataSets EXTRA_n. Caso seja especificado como 0 é assumido o default de 64 itens
+ 02 é um número hexadecimal, resultante da máscara dos bits que representam as opções de uso do protocolo UEC/61850 no SAGE (consulte a planilha IEC61850 OPMKS),
+ 03 é o valor esperado no campo APPID das mensagens GOOSE deste IED

A segunda parte opcional configura os endereços MAC do IED principal e do IED reserva, podendo ser omitido. É utilizado quando existe lógica de failover do SAGE entre IEDs redundantes.

	MACp1= mac1 MACp2= mac2 MACr1= mac3 MACr2= mac4

onde:

+ mac1 é o endereço MAC do IED principal na rede principal
+ mac2 é o endereço MAC do IED principal na rede reserva
+ mac3 é o endereço MAC do IED reserva na rede principal
+ mac4 é o endereço MAC do IED reserva na rede reserva

### ID
Identificador da configuração física. Recomendado utilizar o mesmo id da MUL associada a esta CNF. Sugere-se uma identificação de 2 a 3 caracteres, para que possa ser usada como **prefixo-id** dos pontos dessa CNF.

### LSC
Chave estrangeira da ligação (LSC) à qual a CNF pertence. Relacionamento 1 -> 1 entre as entidades LSC e CNF.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CNF
	    CONFIG= IDAD= 600 KEEP= 5 NREP= 3 TOUT= 10 MPDU= 0 OPMSK= 0C131 GOOSE= 0
	        ID= IED1
	       LSC= IED1
	
	CNF
		CONFIG= ApTitle= 1 1 10 / 1 1 10 AeQ= 1 PS= 1 / 1 SS= 1 TS= 1 IDAD= 600 KEEP= 5 NREP= 3 TOUT= 10 MPDU= 0 OPMSK= 0C131 GOOSE= 0
		    ID= 2i1
		   LSC= 01P3

- - -

# NV1
Na implementação do SAGE para o protocolo IEC/61850 as ocorrências da entidade NV1 determinam o Logical Devices deste IED, que possuem dados de interesse do SAGE a serem configurados em PDF, PAF, PTF e CGF.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 23.
	
## Atributos
### CONFIG
Deve ser preenchido com a identificação do Logical Device no IED, que também é o domain-ID MMS para os objetos desse Logical Device.

### ID
Identificador do NV1. Sugere-se para este identificador o mesmo da entidade CNF, adicionada de um número sequencial, caso o IED possua mais de um Logical Device.

### ORDEM
Este atributo deve ser preenchido com um número de ordem de 1 a "n", onde "n" é a quantidade de Logical Devices do IED utilizados pelo SAGE.

### CNF
Chave estrangeira da configuração (CNF) à qual a classe pertence. Relacionamento 1 -> n entre as entidades CNF e NV1.

### TN1
Este atributo deve ser preenchido com o tipo nulo cadastrado para esta entidade, NLN1.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NV1
		    ID= 2i11
		   CNF= 2i1
		 ORDEM= 1
		   TN1= NLN1
		CONFIG= AA14F1Q10FP1CFG
	
	NV1
		    ID= 2i12
		   CNF= 2i1
		 ORDEM= 2
		   TN1= NLN1
		CONFIG= AA14F1Q10FP1ANN
	
	NV1
		    ID= 2i13
		    CNF= 2i1
		  ORDEM= 3
		    TN1= NLN1
		CONFIG= AA14F1Q10FP2ANN
	
	NV1
	        ID= 2i21
	       CNF= 2i2
	     ORDEM= 1
	       TN1= NLN1
	    CONFIG= AA14F1Q10FP2CON

- - -

# NV2
Na implementação do SAGE para o protocolo IEC/61850 esta entidade é utilizada para o agrupamento de objetos do mesmo tipo dentre os listados na descrição da entidade TN2. Para cada ocorrência da entidade NV1 só pode ser configurada uma ocorrência NV2 de cada tipo.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 23.

## Atributos
### CONFIG
Atributo não utilizado. Pode ser usado como comentário sobre o tipo dos objetos de informação configurados sob esse NV2.

### ID
Identificador do NV2. Sugere-se utilizar o mesmo da entidade CNF relacionada, concatenado por underscore com a sigla do tipo de dado dentre os listados na entidade TN2.

### ORDEM
Este atributo ordena com um número de 1 a "n" a ocorrência do NV2 dentre do conjunto de tipos associados a esta CNF / NV1.

### TPPNT
Tipo dos pontos físicos vinculados a essa NV2.

+ PDF - caso TN2 seja ADAQ (Aquisição de objetos com CDCs classificados no SAGE como "digitais")
+ PAF - caso TN2 seja AAAQ (Aquisição de objetos com CDCs classificados no SAGE como "analógicos")
+ PTF - caso TN2 seja ATTA (Aquisição de objetos com CDCs classificados no SAGE como "discretos" e/ou "totalizadores")
+ CGF - caso TN2 seja CSIM (Objetos de Controle Supervisório)

### NV1
Chave estrangeira da ocorrência da entidade NV1 à qual o conjunto de pontos pertence. Relacionamento 1 -> n entre as entidades NV1 e NV2.

### TN2
Chave estrangeira do tipo de dado sendo configurado dentre os cadastrados na entidade TN2. Relacionamento 1 -> n entre as entidades TN2 e NV2.

+ ADAQ - Aquisição de objetos com CDCs classificados no SAGE como "digitais"
+ AAAQ - Aquisição de objetos com CDCs classificados no SAGE como "analógicos"
+ ATTA - Aquisição de objetos com CDCs classificados no SAGE como "discretos" e/ou "totalizadores"
+ CSIM - Objetos de Controle Supervisório

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NV2
	    CONFIG= Aquisicao CFG para PDF 
	        ID= 2i11_ADAQ
	       NV1= 2i11
	     ORDEM= 1
	       TN2= ADAQ
	     TPPNT= PDF
	
	NV2
	    CONFIG= Aquisicao ANN para PDF
	        ID= 2i12_ADAQ
	       NV1= 2i12
	     ORDEM= 1
	       TN2= ADAQ
	     TPPNT= PDF
		
	NV2
	    CONFIG= Aquisicao ANN para PAF
	        ID= 2i13_AAAQ
	       NV1= 2i13
	     ORDEM= 1
	       TN2= AAAQ
	     TPPNT= PAF
	
	NV2
	    CONFIG= Aquisicao CON para CGF
	        ID= 2i21_CSIM
	       NV1= 2i21
	     ORDEM= 1
	       TN2= CSIM
	     TPPNT= CGF

- - -

# TN1
Entidade que descreve os tipos de classes que podem existir. Para o protocolo IEC/61850 é aplicável exclusivamente o tipo de nível 1 nulo - NLN1. Não é preciso alterar essa entidade.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 24.
	
- - -

# TN2
Esta entidade descreve os tipos de dado que podem existir. Não é preciso alterar essa entidade.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 25.

- - -

# PAF
Configura os pontos analógicos físicos associados às configurações (CNFs) das ligações (LSCs).

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 26.

## Atributos
### ID
Identificador do ponto analógico físico. Este identificador corresponde ao Data Object de Common-Data-Class (CDC) simples ou ao Data Attribute primitivo.

Caso seja necessário diferencia-lo de outros pontos com a mesma identificação de Data Object ou Data Attribute em outro IED, ou mesmo em outro Logical Device deste mesmo IED (NV1), deve ser adicionado um **prefixo-id** e/ou **sufixo-id**, conforme necessidade.

### KCONV1
Coeficiente multiplicativo de conversão da medida (escala).

### KCONV2
Define o nesting-level relativo ao do CDC simples nos reportes feitos pelo IED. Quando o objeto é reportado em Data Sets pré-configurados, usando um nesting-level superior ou inferior, o valor de KCONV2 indica esse defasamento, que poderá ser -1 (superior) ou +1 ou +2 (inferiores). Se o objeto incluído no Data Set pré-configurado for relativo ao CDC simples, ou se o Data Set puder ser criado dinamicamente pelo SAGE, o valor do KCONV2 deverá ser zero.

### KCONV3
Informa o Data Object e o Data Attribute do CDC que será lido como ponto analógico. Caso seja um Data Atribute primitivo, deve ser usada a opção IEEE.

| CDC  | KCONV3 | Atributo Escolhido |
| ---- | ----   | ---- |
| ---  | IEEE   | Dado analógico de qualquer tipo primitivo (int, float, ...) |
| APC  | APC0   | Valor do setpoint - setMag |
| SAV  | SAV0   | Valor instantâneo da medida - instMag |
| MV   | MV0    | Valor da medida sob banda morta - mag |
| MV   | MV1    | Valor instantâneo da medida - instMag |
| CMV  | CMV0   | Valor da medida sob banda morta - cVal.mag |
| CMV  | CMV1   | Valor do angulo sob banda morta - cVal.ang |
| CMV  | CMV2   | Valor instantâneo da medida - instCVal.mag |
| CMV  | CMV3   | Valor instantâneo do angulo - instCVal.ang |

### ORDEM
Não utilizado no protocolo IEC/61850, por tanto não deve ser preenchido.

### TPPNT
Indica que o ponto físico é de aquisição (PAS).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PAF.

### PNT
Chave estrangeira do ponto analógico lógico ao qual o ponto analógico físico está associado. Relacionamento 1 -> 1 entre as entidades PAS e PAF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PAF
	       CMT= AA14F1Q10FP2ANN-METMMXU1$MX$TotVAr
	        ID= 2i13-METMMXU1$MX$TotVAr
	    KCONV1= 1
	    KCONV2= 0
	    KCONV3= MV0
	     TPPNT= PAS
	       NV2= 2i13_AAAQ
	       PNT= SEE:02I1:MVAR
	     DESC1= MVAR da LT 02I1-SEE/SCL
	
	PAF
	       CMT= AA14F1Q10FP2ANN-METMMXU1$MX$PhV$phsA
	        ID= 2i13-METMMXU1$MX$PhV$phsA
	    KCONV1= 1.732
	    KCONV2= 0
	    KCONV3= CMV0
	     TPPNT= PAS
	       NV2= 2i13_AAAQ
	       PNT= SEE:02I1:KVAB
	     DESC1= KV da Fase AB da LT 02I1-SEE/SCL

- - -

# PDF
Configura os pontos digitais físicos associados às configurações CNFs das ligações LSCs.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 27.

## Atributos
### ID
Identificador do ponto digitais físico. Este identificador corresponde ao Data Object de Common-Data-Class (CDC) simples ou ao Data Attribute primitivo.

Caso seja necessário diferencia-lo de outros pontos com a mesma identificação de Data Object ou Data Attribute em outro IED, ou mesmo em outro Logical Device deste mesmo IED (NV1), deve ser adicionado um **prefixo-id** e/ou **sufixo-id**, conforme necessidade.

### KCONV
Informa o Data Object e o Data Attribute do CDC que será lido como ponto digital. Caso seja um Data Atribute primitivo, deve ser usada a opção NOR.

| CDC  | KCONV | Atributo Escolhido |
| ---- | ----  | ---- |
| ---  | NOR   | Dado digital de qualquer tipo primitivo (bool, int, bit-string, ...) |
| SPS  | SPS0  | Estado corrente - stVal |
| DPS  | DPS0  | Estado corrente - stVal |
| SPC  | SPC0  | Estado corrente - stVal |
| SPC  | SPC1  | Estado de seleção para controle - stSeld |
| DPC  | DPC0  | Estado corrente - stVal |
| DPC  | DPC1  | Estado de seleção para controle - stSeld |
| ACT  | ACT0  | Indicação geral de atuação - general |
| ACT  | ACT1  | Indicação de atuação da fase A - phsA |
| ACT  | ACT2  | Indicação de atuação da fase B - phsB |
| ACT  | ACT3  | Indicação de atuação da fase C - phsC |
| ACT  | ACT4  | Indicação de atuação do neutro - neut |
| ACD  | ACD0  | Indicação geral de atuação - general |
| ACD  | ACD1  | Indicação de atuação da fase A - phsA |
| ACD  | ACD2  | Indicação de atuação da fase B - phsB |
| ACD  | ACD3  | Indicação de atuação da fase C - phsC |
| ACD  | ACD4  | Indicação de atuação do neutro - neut |
| ACD  | ACD5  | Direção geral da atuação - dirGeneral |
| ACD  | ACD6  | Direção atuação na fase A - dirPhsA |
| ACD  | ACD7  | Direção atuação na fase B - dirPhsB |
| ACD  | ACD8  | Direção atuação na fase C - dirPhsC |
| ACD  | ACD9  | Direção atuação no neutro - dirNeut |

### ORDEM
Não utilizado no protocolo IEC/61850, por tanto não deve ser preenchido.

### TPPNT
Indica que o ponto físico é de aquisição (PDS).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PDF.

### PNT
Chave estrangeira do ponto digital lógico ao qual o ponto digital físico está associado. Relacionamento 1 -> 1 entre as entidades PDS e PDF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PDF
	       CMT= AA14F1Q10FP1CFG-DC1CSWI1$ST$Pos
	        ID= 2i11-DC1CSWI1$ST$Pos
	     KCONV= DPS0
	     TPPNT= PDS
	       NV2= 2i11_ADAQ
	       PNT= SEE:32I1-1:89
	     DESC1= Seccionadora 32I1-1
	     DESC2= 0 - Chave Abriu     1 - Chave Fechou
	
	PDF
	       CMT= AA14F1Q10FP1CFG-M3PPDIS5$ST$Op
	        ID= 2i11-M3PPDIS5$ST$Op
	       NV2= 2i11_ADAQ
	     KCONV= ACT0
	       PNT= SEE:02I1:F1:PDTZ
	     TPPNT= PDS
	     DESC1= Trip Zona 3 (21)
	     DESC2= 0 - Normalizou      1 - Atuou
	
	PDF
	       CMT= AA14F1Q10FP1ANN-OUT2GGIO3$ST$Ind01
	        ID= 2i12-OUT2GGIO3$ST$Ind01
	       NV2= 2i12_ADAQ
	     KCONV= SPS0
	       PNT= SEE:02I1:F1:PDTG
	     TPPNT= PDS
	     DESC1= Trip Geral Relé
	     DESC2= 0 - Normalizou      1 - Atuou
	
	PDF
	       CMT= AA14F1Q10FP1ANN-OUT2GGIO3$ST$Ind06
	        ID= 2i12-OUT2GGIO3$ST$Ind06
	       NV2= 2i12_ADAQ
	     KCONV= SPS0
	       PNT= SEE:02I1:F1:TPDN
	     TPPNT= PDS
	     DESC1= Trip de Neutro
	     DESC2= 0 - Normalizou      1 - Atuou

- - -

# PTF
Configura os pontos totalizadores ou discretos físicos associados às configurações (CNFs) das ligações (LSCs).

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 29.

## Atributos
### ID
Identificador do ponto totalizadores/discretos físico. Este identificador corresponde ao Data Object de Common-Data-Class (CDC) simples ou ao Data Attribute primitivo.

Caso seja necessário diferencia-lo de outros pontos com a mesma identificação de Data Object ou Data Attribute em outro IED, ou mesmo em outro Logical Device deste mesmo IED (NV1), deve ser adicionado um **prefixo-id** e/ou **sufixo-id**, conforme necessidade.

### KCONV1
Não utilizado.

### KCONV2
Não utilizado.

### KCONV3
Informa o Data Object e o Data Attribute do CDC que será lido como ponto totalizador/discreto. Caso seja um Data Atribute primitivo, deve ser usada a opção NULO.

| CDC  | KCONV3 | Atributo Escolhido |
| ---- | ----   | ---- |
| ---  | NULO   | Dado tataliz./discreto de qualquer tipo primitivo (int, float, ...) |
| INS  | INS0   | Estado discreto corrente - stVal |
| SEC  | SEC0   | Contagem corrente de violações - cnt|
| BCR  | BCR0   | Contagem corrente - actVal |
| BCR  | BCR1   | Contagem congelada - frVal |
| INC  | INC0   | Estado discreto corrente - stVal |
| INC  | INC1   | Estado de seleção para controle - stSeld |
| BSC  | BSC0   | Posição binária corrente - valWTr |
| BSC  | BSC1   | Estado de seleção para controle - stSeld |
| ISC  | ISC0   | Posição binária corrente - valWTr |
| ISC  | ISC1   | Estado de seleção para controle - stSeld |

### ORDEM
Não utilizado no protocolo IEC/61850, por tanto não deve ser preenchido.

### TPPNT
Indica que o ponto físico é de aquisição (PTS).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PTF.

### PNT
Chave estrangeira do ponto totalizador/discreto lógico ao qual o ponto totalizador/discreto físico está associado. Relacionamento 1 -> 1 entre as entidades PTS e PTF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PTF
		   PNT= SEE:04P1:F1:KM
		KCONV3= INC0
			ID= F13-pdGGIO140$ST$ISCSO7
		   NV2= F13_ATTA
		 TPPNT= PTS

- - -

# CGF
Configura os pontos de controle físicos associados às configurações (CNFs) das ligações (LSCs).

Na implementação SAGE do protocolo IEC/61850, não é preciso configurar um ponto de controle físico CGF para ser associado a um ponto de controle lógico de gestão da comunicação de dados (pontos CGS com TPCTL = CSCD).

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 30.

## Atributos
### ID
Identificador do ponto de controle físico. Este identificador corresponde ao Data Object de Common-Data-Class (CDC) simples e dos Functional Constraints CO ou SP caso seja, respectivamente, controle digital ou set-point. Também deverá possuir o Data Attribute construído "Oper" conforme especificado na IEC/61850-8.

### KCONV
Concatenação de duas palavras chaves que indicam o tipo de seleção de controle (SBO ou SBOw) e se tem enhenced security.

Se deixado vazio, indica que o controle é direto, sem enhenced security e sem nenhum dos dois checks (Sincro-Check e Interlock-Check)

+ SBO  - comando do tipo Select-before-operate
+ SBOw - comando do tipo Select-before-operate-with-value

e/ou

+ TERM - deve aguardar por uma mensagem (PDU) com um InformationReport sinalizando o término (CommandTermination) do controle enhenced security.

### ORDEM
Não utilizado no protocolo IEC/61850, por tanto não deve ser preenchido.

### OPCOES
Atributo opcional. Pode ser preenchido com até duas palavras chave, sendo elas:

+ I - Ativa o Interlock-Check
+ S - Ativa o Sincro-Check
+ SI - Ativa Interlock-Check e Sincro-Check

e/ou

+ O1 - origem do controle em bay-control
+ O2 - origem do controle em station-control (default)
+ O3 - origem do controle em remote-control
+ O4 - origem do controle em automatic-bay
+ O5 - origem do controle em automatic-station
+ O6 - origem do controle em automatic-remote
+ O7 - origem do controle em maintenance
+ O8 - origem do controle em process

### CGF
Chave estrangeira do ponto de controle lógico ao qual o ponto de controle físico está associado. Relacionamento 1 -> 1 entre as entidades CGS e CGF.

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto de controle físico pertence. Relacionamento 1 -> n entre as entidades NV2 e CGF.

### CNF
Não utilizado no protocolo IEC/61850, por tanto não deve ser preenchido.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	CGF
	       CMT= AA14F1Q10FP2CON-SCSWI1$CO$Pos
	        ID= 2i21-SCSWI1$CO$Pos
	       NV2= 2i21_CSIM
	       CGS= SEE:12I1:52
	     DESC1= Disjuntor 12I3
		 KCONV= SBO
	    OPCOES= SI O2

- - -

# RFI
Filtro simples é aquele cujas parcelas (pontos físicos) podem participar de apenas um único filtro gerando ponto lógico.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 32.

## Atributos
### ORDEM
Número sequencial de 1 a n que indica a ordem da parcela no filtro.

### TIPOP
Tipo da parcela do filtro. Pode assumir os valores:

+ PAF - Se a parcela do filtro for um ponto analógico físico
+ PDF - Se a parcela do filtro for um ponto digital físico
+ PTF - Se a parcela do filtro for um ponto totalizador físico

### PNT
Chave estrangeira do ponto físico que compõe a parcela. Relacionamento 1 -> 1 entre as entidades de pontos físicos (PAF, PDF ou PTF. Depende do valor de TIPOP).

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo
**TODO**

- - -

# RFC
Filtro composto é aquele cujas parcelas (pontos físicos) pode participar de vários filtros gerando vários pontos lógicos.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 32.

## Atributos
### ORDEM
Número sequencial de 1 a n que indica a ordem da parcela no filtro.

### TPPARC
Tipo da parcela do filtro. Pode assumir os valores:

+ PAF - Se a parcela do filtro for um ponto analógico físico
+ PDF - Se a parcela do filtro for um ponto digital físico
+ PTF - Se a parcela do filtro for um ponto totalizador físico

### TPPNT
Tipo do ponto resultante do filtro. Pode assumir os valores:

+ PAS - Se o ponto resultante do filtro for analógicos lógicos
+ PDS - Se o ponto resultante do filtro for digitais lógicos
+ PTS - Se o ponto resultante do filtro for totalizadores lógicos

### PARC
Chave estrangeira do ponto físico que compõe a parcela do filtro.

### PNT
Chave estrangeira do ponto físico resultante do filtro. Relacionamento 1 -> 1 entre as entidades de pontos físicos (PAF, PDF ou PTF. Depende do valor de TPPNT).

Os tipos de filtros simples ou compostos para pontos digitais implementados para o protocolo são: FIL1/FIL9 - filtro OR de pontos digitais que pode ser utilizado, por exemplo, para agrupamento de pontos de proteção gerando SOE apenas na primeira atuação e na última normalização.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	RFC
	     ORDEM= 1
	      PARC= 2i11-M3PPDIS5$ST$Op
	       PNT= SEE:02I1:00:ONS3
	    TPPARC= PDF
	     TPPNT= PDS
	
	RFC
	     ORDEM= 2
	      PARC= 2i12-OUT2GGIO3$ST$Ind01
	       PNT= SEE:02I1:00:ONS3
	    TPPARC= PDF
	     TPPNT= PDS
	
	RFC
	     ORDEM= 3
	      PARC= 2i12-OUT2GGIO3$ST$Ind06
	       PNT= SEE:02I1:00:ONS3
	    TPPARC= PDF
	     TPPNT= PDS

- - -

# LSC
Para a implementação SAGE do protocolo IEC/61850 os seguintes atributos da entidade LSC são especificamente aplicados:

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 33.

## Atributos
### ID
Identificador longo da Ligação SCADA. Este identificador será utilizado nos alarmes e logs gerados pelo SAGE para eventos ocorridos no âmbito deste protocolo que estejam relacionados com o IED. Recomenda-se um nome descritivo com até 8 caracteres.

### NOME
Nome da Ligação SCADA utilizada na descrição dos alarmes e eventos associados.

### GSD
Chave estrangeira do Gateway-SCADA do SAGE ao qual a Ligação SCADA está associado. Relacionamento 1 -> n entre as entidades GSD e LSC.

### MAP
Chave estrangeira do macro alarme ao qual a ligação SCADA está associado. Relacionamento 1 -> n entre as entidades MAP e LSC.

### TCV
Deve ser preenchido com o identificador SAGE para o protocolo IEC/61850, CNVO.

### TTP
Deve ser preenchido com o identificador SAGE para o transporte baseado na Manufacturing Message Specification, MMST, conforme cadastro da entidade TTP.

### VERBD
Versão da base de dados.

### TIPO
Deve ser preenchido com o identificador AD, usado no SAGE para qualificar os protocolos que são implementados através de multiligações.

### NSRV1
Deve ser preenchido com o identificador "localhost".

### NSRV2
Deve ser preenchido com o identificador "localhost".

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	LSC
	       GSD= SE1
	        ID= 2IF1
	       MAP= 02I1
	      NOME= Ligacao IEC61850 - 02I1-F1
	     NSRV1= localhost
	     NSRV2= localhost
	       TCV= CNVO
	      TIPO= AD
	       TTP= MMST
	     VERBD= OUT12
	
	LSC
	       GSD= SEE
	        ID= 2IF2
	       MAP= 02I1
	      NOME= Ligacao IEC61850 - 02I1-F2
	     NSRV1= localhost
	     NSRV2= localhost
	       TCV= CNVO
	      TIPO= AD
	       TTP= MMST
	     VERBD= OUT12

- - -

# MUL
Para a implementação SAGE do protocolo IEC/61850 somente os atributos listados a seguir da entidade MUL deverão ser especificados.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 34.

## Atributos
### ID
Identificador da multiligação. No mapeamento do SAGE para o protocolo IEC/61850, este atributo corresponde ao identificador curto do IED. Recomenda-se ser igual ao identificador da CNF associada.

### CNF
Chave estrangeira da CNF que descreve a configuração de pontos físicos associados à multiligação. Relacionamento 1 -> 1 entre as entidades MUL e CNF.

### GSD
Chave estrangeira do Gateway-SCADA do SAGE (par de servidores) onde residem os servidores IEC/61850 principal e reserva do SAGE. Relacionamento 1 -> n entre as entidades GSD e MUL.

### ORDEM
Deve ser preenchido com um número que determina a ordenação das multiligações na base de dados do SAGE.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	MUL
	        ID= 2i1
	       CNF= 2i1
	     ORDEM= 1
	       GSD= SEE
	       CMT= 192.168.101.42  # Aquisicao IEC61850 - 02I1-F1
	
	MUL
	        ID= 2i2
	       CNF= 2i2
	     ORDEM= 2
	       GSD= SEE
	       CMT= 192.168.101.43  # Aquisicao IEC61850 - 02I1-F2

- - -

# ENM
Para a implementação SAGE do protocolo IEC/61850 é obrigatória a configuração de duas ocorrências desta entidade para cada ocorrência MUL. Essas ocorrências estão associadas ao conceito de IED principal e reserva, para que seja possível sinalizar em tela com qual IED (principal ou reserva) a associação MMS foi estabelecida. Mesmo que não haja realmente um IED reserva, essas duas ocorrências da entidade devem ser configuradas.

Informações completas em /sage/Manual Protocolos/IEC61850.pdf, página 35.

## Atributos
### ID
Deve ser preenchido com o mesmo identificador usado na entidade MUL associada, adicionando-se o número 1 ou 2 para designar os IEDs principal e reserva respectivamente.

### ORDEM
Deve ser preenchido com o número 1 ou 2 conforme descrito acima.

### MUL
Chave estrangeira da ocorrência de MUL à qual o ENM está associado. Relacionamento 1 -> n entre as entidades MUL e ENM (neste caso específico, 1 -> 2).

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	ENM
	        ID= 2i11
	       MUL= 2i1
	     ORDEM= 1
	
	ENM
	        ID= 2i12
	       MUL= 2i1
	     ORDEM= 2
	
	ENM
	        ID= 2i21
	       MUL= 2i2
	     ORDEM= 1
	
	ENM
	        ID= 2i22
	       MUL= 2i2
	     ORDEM= 2

- - -

# TAC
Para a implementação SAGE do protocolo IEC/61850 somente os atributos listados a seguir da entidade TAC deverão ser especificados.

## Atributos
### ID
Identificador do TAC. Sugere-se para este identificador o mesmo da entidade CNF.

### INS
Chave estrangeira da instalação associada ao terminal de aquisição. Relacionamento 1 -> n entre as entidades GSD e TAC.

### NOME
Nome do terminal utilizado na descrição dos alarmes e eventos associados.

### TPAQS
Tipo da aquisição.

### LSC
Chave estrangeira da Ligação SCADA ao qual ao terminal de aquisição está associado. Relacionamento 1 -> n entre as entidades LSC e TAC.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TAC
		   ID= 2i1
		  INS= SEE
		  LSC= 2IF1
		 NOME= Aquisicao IEC61850 - 02I1-F1
		TPAQS= ASAC
	
	TAC
		   ID= 2i2
		  INS= SEE
		  LSC= 2IF2
		 NOME= Aquisicao IEC61850 - 02I1-F2
		TPAQS= ASAC

- - -

# Configurando o Arquivo hosts
O arquivo padrão /etc/hosts, que contém o endereçamento IP cadastrado localmente nos sistemas UNIX, deverá ser editado pelo usuário para que o transportador MMST possa conhecer os dois endereços IP que podem ser associados a cada um dos dois IEDs (principal e reserva) remotos, totalizando assim até 4 endereços IP para endereçar os IEDs remotos de uma MULL/CNF/LSC.

Para estabelecer a relação entre os nomes de servidores (arquivo mmst.cnf) e os endereços IP cadastrados (/etc/hosts), esses nomes devem ser editados com o prefixo "host_mms_", a identificação do ENM e o sufixo "b" para identificar o endereço de backup. 

	<IP_Addres> host_mms_<ID_ENM_P>
	<IP_Addres> host_mms_<ID_ENM_P>b
	<IP_Addres> host_mms_<ID_ENM_R>
	<IP_Addres> host_mms_<ID_ENM_R>b

onde

+ ID_ENM_P é a transportadora principal
+ ID_ENM_R é a transportadora secundária
	

Se existir apenas um canal de comunicação, as transportadoras **devem** ser agrupadas no mesmo endereço IP.

	<IP_Addres> host_mms_<ID_ENM_P>    host_mms_<ID_ENM_P>b    host_mms_<ID_ENM_R>    host_mms_<ID_ENM_R>b

## Guia Rápido
Se seguido o padrão de desenvolvimento desse guia, o `ID_ENM_P` e o `ID_ENM_R` nada mais são do que o id da CNF comcatenado com 1 e 2, respectivamente.

Assim, a definição dos endereços pode seguir o padrão:

	<IP_Addres> host_mms_<ID_CNF>1
	<IP_Addres> host_mms_<ID_CNF>1b
	<IP_Addres> host_mms_<ID_CNF>2
	<IP_Addres> host_mms_<ID_CNF>2b
	
Ou

	<IP_Addres> host_mms_<ID_CNF>1    host_mms_<ID_CNF>1b    host_mms_<ID_CNF>2    host_mms_<ID_CNF>2b
	
## Exemplo

	192.168.101.42    host_mms_2i11    host_mms_2ej1b    host_mms_2i12    host_mms_2i12b    # IED 1 Endereços IPs Agrupados
	192.168.101.43    host_mms_2i21    host_mms_2i21b    # IED 2 Endereço IP Principal
	192.168.101.44    host_mms_2i22    host_mms_2i22b    # IED 2 Endereço IP Secundario

- - -
