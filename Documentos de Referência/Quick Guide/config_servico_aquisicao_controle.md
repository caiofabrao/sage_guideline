[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/12                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/12    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Serviço de Aquisição e Controle
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para configuração do serviço de aquisição e controle (SAC).

# Entidades DATs Necessárias
![Alt text](imgs/sac_der.png)

+ **[LSC](#markdown-header-lsc):** Ligação SCADA
+ **[INS](#markdown-header-ins):** Instalação
+ **[TAC](#markdown-header-tac):** Terminal de Aquisição e Controle
+ **[TCL](#markdown-header-tcl):** Tipo de Cálculos para Pontos de Medição
+ **[TCTL](#markdown-header-tctl):** Tipo de Ponto de Controle
+ **[CGS](#markdown-header-cgs):** Ponto de Controle Genérico do SAC
+ **[PAS](#markdown-header-pas):** Ponto Analógico Lógico
+ **[PDS](#markdown-header-pds):** Ponto Digital Lógico
+ **[PTS](#markdown-header-pts):** Ponto Totalizado Lógico
+ **[RCA](#markdown-header-rca):** Relacionamento de Pontos Lógicos x Pontos Calculados
+ **[REGRA](#markdown-header-regra):** Regra para o Registrador Condicional
+ **[GRUPO](#markdown-header-grupo):** Grupo de Componentes para Diálogo Automático
+ **[GRCMP](#markdown-header-grcmp):** Componentes para Diálogo Automático

- - -

# LSC
Configura as ligações virtuais lógicas de aquisição e controle, de distribuição e mistas existentes no sistema.

## Atributos
### ID
Identificador longo da Ligação SCADA. Recomenda-se um nome descritivo com até 8 caracteres.

### NOME
Nome da Ligação SCADA utilizada na descrição dos alarmes e eventos associados.

### GSD
Chave estrangeira do Gateway-SCADA do SAGE ao qual a Ligação SCADA está associado. Relacionamento 1 -> n entre as entidades GSD e LSC.

### MAP
Chave estrangeira do macro alarme ao qual a ligação SCADA está associado. Relacionamento 1 -> n entre as entidades MAP e LSC.

### TCV
Chave estrangeira do tipo de conversor de protocolo ao qual a ligação SCADA está associado. Relacionamento 1 -> n entre as entidades TCV e LSC.

### TTP
Chave estrangeira do tipo de transportador de protocolo ao qual a ligação SCADA está associado. Relacionamento 1 -> n entre as entidades TTP e LSC.

### VERBD
Versão da base de dados.

### TIPO
Tipo de ligação.

### NSRV1
Nó com o servidor 1 do transportador.

### NSRV2
Nó com o servidor 2 do transportador.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	LSC
	       GSD= SEE
	        ID= 2JF1
	       MAP= 02J1
	      NOME= Ligacao IEC61850 - 02J1-F1
	     NSRV1= localhost
	     NSRV2= localhost
	       TCV= CNVO
	      TIPO= AD
	       TTP= MMST
	     VERBD= OUT12

- - -

# INS
Configura as diversas instalação do SAGE.

## Atributos
### ID
Identificador da instalação.

### NOME
Nome da instalação.

### TELA
Chave estrangeira da tela associada à instalação. Relacionamento 1 -> n entre as entidades TELA e INS.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	INS
	      ID= SE1
		NOME= Subestacao SE1
	    TELA= SE1_UNI230KV

- - -

# TAC
As TACs definem grupos lógicos de aquisição de uma mesma funcionalidade. A TAC serve também para estabelecer bloqueio de controle, ou seja, apenas um ponto da TAC pode estar sendo controlado em um determinado instante.

## Atributos
### ID
Identificador do TAC. Sugere-se para este identificador o mesmo da entidade CNF.

### INS
Chave estrangeira da instalação associada ao terminal de aquisição. Relacionamento 1 -> n entre as entidades GSD e TAC.

### NOME
Nome do terminal utilizado na descrição dos alarmes e eventos associados.

### TPAQS
Tipo da aquisição.

ASAC - Aquisição e controle.

### LSC
Chave estrangeira da Ligação SCADA ao qual ao terminal de aquisição está associado. Relacionamento 1 -> n entre as entidades LSC e TAC.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TAC
		   ID= 2j1
		  INS= SE1
		  LSC= 2JF1
		 NOME= Aquisicao IEC61850 - 02J1-F1
		TPAQS= ASAC

- - -

# TCL
Configura os cálculos implementados no sistema. Existem 3 tipos de cálculos no SAGE: cálculos dinâmicos; cálculos estáticos interpretado; e cálculo estático compilado.

## Atributos
### ID
Identificador do tipo de cálculo.

### DESCR
Descrição do tipo de cálculo.

### NSEQ
Número sequencial do tipo de cálculo.

### FORMULA
Fórmula do cálculo.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TCL
		  DESCR= Cálculo Dinâmico
		     ID= DINAMICO
		   NSEQ= 255
	
	TCL
		  DESCR= Soma Interpretada de Duas Parcelas
		     ID= SOMA2
		   NSEQ= 0
		FORMULA= P1+P2
	
	TCL
		  DESCR= Cálculo da Corrente via Tensão/Pot.Ativa/Pot.Reativa
		     ID= CORRENTE
		   NSEQ= 1

- - -

# TCTL
Especifica os tipos que o ponto de controle pode assumir na configuração do diálogo de controle.

## Atributos
### ID
Identificador do tipo de cálculo.

### ALR_CLOSE
Texto para alarme de controle close.

### ALR_TRIP
Texto para alarme de controle trip.

### DLG_CLOSE
Texto para diálogo de controle close.

### DLG_TRIP
Texto para diálogo de controle trip.

### NSEQ
Número sequencial do tipo de cálculo.

### TIP
Comunicação (CMD ou Controle Tempo Real (CTL).

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TCTL
		        ID= CTCL
		 ALR_CLOSE= Fechar
		  ALR_TRIP= Abrir
		 DLG_CLOSE= Fechar Equipamento
		  DLG_TRIP= Abrir Equipamento
		      NSEQ= 1
		       TIP= CTL
- - -

# CGS
Configura os pontos de controle lógicos de todas as TACs de todas as LSCs do sistema.

## Atributos
### ID
Identificador do ponto de controle lógico.

### LMI1C
Limite inferior do setpoint 1.

### LMI2C
Limite inferior do setpoint 2.

### LMS1C
Limite superior do setpoint 1.

### LMS2C
Limite superior do setpoint 2.

### NOME
Nome do ponto de controle.

### PAC
Chave estrangeira do ponto aquisitado. Relacionamento 1 -> n entre as entidades PDS e CGS.

### PINT
Ponto digital lógico de intertravamento. Relacionamento 1 -> n entre as entidades PDS e CGS.

### TAC
Chave estrangeira do TAC a qual o Ponto de Controle Genérico pertence. Relacionamento 1 -> n entre as entidades TAC e CGS.

### TIPO
Tipo do ponto aquisitado.

### TIPOE
Chave estrangeira do tipo de ponto de controle associado ao ponto lógico. Relacionamento 1 -> n entre as entidades TCTL e CGS.

### TPCTL
Tipo do controle.

### TRRAC
Tempo em segundos para o resultado.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CGS
		   ID= SEE:14J1:52
		 NOME= Disjuntor 14J1
		  TAC= 2j1
		  PAC= SEE:14J1:52
		 PINT= SEE:14J1:52:IATV
		 TIPO= PDS
		TIPOE= CTCL
		LMI1C= 0
		LMI2C= 0
		LMS1C= 0
		LMS2C= 0
		TPCTL= CSAC
		TRRAC= 35
		
	CGS
		   ID= COT:14J1:52
		 NOME= Disjuntor 14J1
		  TAC= 2j1
		  PAC= SEE:14J1:52
		 PINT= SEE:14J1:52:IATV
		 TIPO= PDS
		TIPOE= CTCL
		LMI1C= 0
		LMI2C= 0
		LMS1C= 0
		LMS2C= 0
		TPCTL= CSAC
		TRRAC= 35

- - -

# PAS
Configura os pontos analógicos calculados e lógicos de todas as TACs de todas as LSCs do sistema.

## Atributos
### ID
Identificador do ponto analógico.

### NOME
Descrição do ponto analógico.

### TAC
Chave estrangeira do TAC a qual o ponto analógico pertence. Relacionamento 1 -> n entre as entidades TAC e PDS.

### LIA
Limite inferior de advertência.

### LIE
Limite inferior de escala.

### LIU
Limite inferior de urgência.

### LSA
Limite superior de advertência.

### LSE
Limite superior de escala.

### LSU
Limite superior de urgência.

### ALINT
Indicador para Alarme na Inicialização e na Integridade.

### ALRIN
Alarme inibido.

### TCL
Chave estrangeira do tipo de cálculo do ponto analógico. Relacionamento 1 -> n entre as entidades TCL e PDS.

### OCR
Chave estrangeira da ocorrência do ponto analógico. Relacionamento 1 -> n entre as entidades OCR e PDS.

### TIPO
Tipo da grandeza medida.

MW | Potência Ativa; MVAR | Potência Reativa; KV_AB | Tensão Fase AB; KV_BC | Tensão Fase BC; KV_CA | Tensão Fase CA; AMP_A | Corrente Fase A; AMP_B | Corrente Fase B; AMP_C | Corrente Fase C; DIST | Distância de Falta; TMP | Temperatura; TAP | Posição Tap; FREQ | Frequência;

### BNDMO
Banda morta.

### CDINIC
Condição inicial do ponto analógico.

### VLINIC
Valor inicial do ponto analógico.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	PAS
			ID= BAY:02T1:MVAR
		  NOME= Potência Reativa 02T1
		   TAC= BAY
		   LIA= -310
		   LIE= -340
		   LIU= -320
		   LSA= 360
		   LSE= 380
		   LSU= 370
		 ALINT= SIM
		 ALRIN= NAO
		   TCL= NLCL
		   OCR= OCR_PAS01
		  TIPO= MVAR
		 BNDMO= 0
		CDINIC= NORMAL
		VLINIC= 0
	
	PAS
			ID= BAY:02T1:IA
		  NOME= Corrente de Fase A 02T1
		   TAC= BAY
		   LIA= -99999
		   LIE= -99999
		   LIU= -99999
		   LSA= 99999
		   LSE= 99999
		   LSU= 99999
		 ALINT= NAO
		 ALRIN= SIM
		   TCL= NLCL
		  TIPO= AMP_A
		VLINIC= 0

- - -

# PDS
Configura os pontos digitais calculados e lógicos de todas as TACs de todas as LSCs do sistema.

## Atributos
### ID
Identificador do ponto digital.

### NOME
Nome do ponto digital.

### OCR
Chave estrangeira da ocorrência do ponto digital. Relacionamento 1 -> n entre as entidades OCR e PDS.

### ALINT
Indicador para Alarme na Inicialização e na Integridade.

### ALRIN
Alarme inibido.

### STINI
Estado de inicialização do ponto digital.

### STNOR
Estado de referência do ponto digital - IHM.

### TAC
Chave estrangeira do TAC a qual o ponto digital pertence. Relacionamento 1 -> n entre as entidades TAC e PDS.

### CDINIC
Condição inicial do ponto digital.

### TCL
Chave estrangeira do tipo de cálculo do ponto digital. Relacionamento 1 -> n entre as entidades TCL e PDS.

### TIPO
Tipo de grandeza.

### TPFIL
Tipo do filtro.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	PDS
		  NOME= Disjuntor 12D4-1
		    ID= BAY:12D4-1:52
		   TAC= BAY
		   OCR= OCR_DIS01
		 ALINT= SIM
		 ALRIN= NAO
		CDINIC= NORMAL
		   TCL= NLCL
		 TPFIL= NLFL

- - -

# PTS
Configura os pontos totalizadores calculados e lógicos de todas as TACs de todas as LSCs do sistema.

## Atributos
### ID
Identificador do ponto totalizado.

### NOME
Nome do ponto totalizado.

### LSA
Limite superior de advertência.

### LSE
Limite superior de escala.

### LSU
Limite superior de urgência.

### OCR
Chave estrangeira da ocorrência do ponto totalizado. Relacionamento 1 -> n entre as entidades OCR e PTS.

### TAC
Chave estrangeira do TAC a qual o ponto totalizado pertence. Relacionamento 1 -> n entre as entidades TAC e PTS.

### ALINT
Indicador para Alarme na Inicialização e na Integridade.

### ALRIN
Alarme inibido.

### CDINIC
Condição inicial do ponto totalizado.

### TCL
Chave estrangeira do tipo de cálculo do ponto analógico. Relacionamento 1 -> n entre as entidades TCL e PTS.

### TPFIL
Tipo do filtro.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	PTS
		    ID= SEE:04J1:F1:KM
		  NOME= Distância de Falta LT 04J1-SEE/UVT
		   LSA= 32767
		   LSE= 32767
		   LSU= 32767
		   OCR= OCR_PAS01
		   TAC= BAY
		 ALRIN= NAO
		CDINIC= NORMAL
		   TCL= NLCL
		 TPFIL= NLFL

- - -

# RCA
Especifica os relacionamentos dos pontos lógicos com os pontos calculados das entidades PAS, PTS e PDS. Enumera todas as parcelas (pontos lógicos simples) de todos os pontos calculados.

## Atributos
### PNT
Identificador do ponto calculado.

### TPPNT
Tipo do ponto calculado.

### ORDEM
Ordem da parcela no cálculo.

### PARC
Identificador da parcela do cálculo.

### TPPARC
Tipo de ponto da parcela.

### TIPOP
Tipo da parcela.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	RCA
		   PNT= JCD:12J1:52:IATV
		 TPPNT= PDS
		 ORDEM= 1
		  PARC= JCD:12J1:52
		TPPARC= PDS
		 TIPOP= EDC
	
	RCA
		   PNT= JCD:12J1:52:IATV
		 TPPNT= PDS
		 ORDEM= 2
		  PARC= JCD:12J1:52:IAAB
		TPPARC= PDS
		 TIPOP= EDC
	
	RCA
		   PNT= JCD:12J1:52:IATV
		 TPPNT= PDS
		 ORDEM= 3
		  PARC= JCD:12J1:52:IAFE
		TPPARC= PDS
		 TIPOP= EDC
	
	RCA
		   PNT= JCD:12J1:52:IATV
		 TPPNT= PDS
		 ORDEM= 4
		  PARC= JCD:02J1:UC1:ULOC
		TPPARC= PDS
		 TIPOP= EDC

- - -

# REGRA
[//]: # "TODO"

## Atributos
### ID
Identificador da regra.

### NOME
Nome da regra para o registrador condicional.

### GRUPO
Chave estrangeira do grupo associado ao componente para diálogo automático. Relacionamento 1 -> n entre as entidades GRUPO e REGRA.

### HABILIT
Indicador de habilitação da regra.

### PERMAX
Período máximo em segundos de armazenamento considerando redisparos.

### PERPRE
Período em segundos de armazenamento antes o disparo da regra.

### PERPOS
Período em segundos de armazenamento após o disparo da regra.

### PERVAR
Período de varredura em segundos para armazenamento do buffer circular.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo
[//]: # "TODO"

- - -

# GRUPO
Configura os grupos de componentes para diálogo automático (popup).

## Atributos
### ID
Identificador do grupo de componentes para diálogo automático.

### NOME
Nome do grupo de componentes para diálogo automático.

### APLIC
Processo associado ao grupo de componentes para diálogo automático.

### TIPO
Tipo do grupo.

### COR_BG
Cor de fundo associada ao grupo.

### COR_FG
Cor de frente associada ao grupo.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	GRUPO
		    ID= TRAFOS-US01
		  NOME= Trafos da Usina 01
		 APLIC= VTelas
		  TIPO= OUTROS
		COR_BG= OCEANO
		COR_FG= AZUL_MEDIO

- - -

# GRCMP
[//]: # "TODO"

## Atributos
### GRUPO
Chave estrangeira do grupo associado ao componente para diálogo automático. Relacionamento 1 -> n entre as entidades GRUPO e GRCMP.

### PNT
Chave estrangeira do recurso associado ao componente para diálogo automático. Relacionamento 1 -> n entre as entidades (PAS/PDS/PTS) e GRCMP.

### TPPNT
Tipo do recurso associado ao componente para diálogo automático.

### ORDEM1
Primeiro nível de ordem do componente.

### ORDEM2
Segundo nível de ordem do componente.

### CORTXT
Cor do texto associado ao componente.

### TPTXT
Tipo de texto associado ao componente.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	GRCMP
		 GRUPO= TRAFOS-US01
		   PNT= TRAFOS-US01-TR21
		 TPPNT= GRUPO
		ORDEM1= 1
		ORDEM2= 1
		CORTXT= PRETO
		 TPTXT= ID
	
	GRCMP
		 GRUPO= TRAFOS-US01
		   PNT= TRAFOS-US01-TR22
		 TPPNT= GRUPO
		ORDEM1= 2
		ORDEM2= 1
		CORTXT= PRETO
		 TPTXT= ID

- - -