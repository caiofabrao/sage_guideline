[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/10                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/10    caf     First commit.                                          "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

# Aquisição e Distribuição em Protocolo DNP 3.0
Neste documento é apresentado um guia rápido dos arquivos DATs utilizados para aquisição e distribuição em protocolo DNP 3.0 .

- - -

# Entidades DATs Necessárias
+ **[CNF](#markdown-header-cnf):** Configuração de Ligação Física do SCD
+ **[NV1](#markdown-header-nv1):** Nível 1 da Configuração Física
+ **[NV2](#markdown-header-nv2):** Nível 2 da Configuração Física
+ **[TN1](#markdown-header-tn1):** Tipo de Entidade Física do Nível 1
+ **[TN2](#markdown-header-tn2):** Tipo de Entidade Física do Nível 2
+ **[PAF](#markdown-header-paf):** Ponto Analógico Físico
+ **[PDF](#markdown-header-psf):** Ponto Digital Físico
+ **[PTF](#markdown-header-ptf):** Ponto Totalizador Físico
+ **[CGF](#markdown-header-cgf):** Ponto de Controle Físico
+ **[RFI](#markdown-header-rfi):** Relacionamento de Filtros Simples
+ **[RFC](#markdown-header-rfc):** Relacionamento de Filtros Compostos
+ **[LSC](#markdown-header-lsc):** Ligação SCADA com o IED
+ **[CXU](#markdown-header-cxu):** Conexões de Comunicação com UTR e Canal
+ **[UTR](#markdown-header-utr):** Unidade Terminal Remota
+ **[ENU](#markdown-header-enu):** Enlace de Conexão com UTR
+ **[TAC](#markdown-header-tac):** Terminal de Aquisição e Controle
+ **[TDD](#markdown-header-tdd):** Terminal de Distribuição de Dados

- - -

# CNF
Configura as CNFs de aquisição e distribuição associadas as LSCs do sistema. Cada CNF está associada à aquisição de uma UTR ou de um centro de controle (LSC do tipo "aa"), ou ainda, à distribuição para um centro de controle (LSC do tipo "dd").

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 1.

## Atributos
### CONFIG
Especifica o meio pelo qual serão transmitidas e recebidas as mensagens desta CNF. O preenchimento desse atributo é feito com uma string que especifica o canal físico que será utilizado.

	PlPr= p1 LiPr= p2 PlRe= r1 LiRe= r2 SINCR= sn TZBR= tk ou TZGM= tk DnpLvl= Mmcdu

onde:

+ **p1** é o número que identifica a placa de comunicação ou a placa virtual do enlace principal;
+ **p2** é o número que identifica a linha ou linha virtual dessa placa usada para o enlace principal;
+ **r1/r2** são os números equivalentes que identificam o enlace reserva;
+ **sn** é uma combinação de dois números (s e n). Com **s** igual a 0 (default), o SAGE suprime do arquivo SDE eventos com time-tag inválidos, caso contrário, os eventos são sempre registrados. Para **n** igual a 1 (default), o SAGE envia o acerto de hora sempre que solicitado pela UTR. Caso **n** seja 2, o SAGE envia o acerto de hora de forma incondicional no estabelecimento da comunicação e, posteriormente, a cada meia hora. Para outros valores de **n**, o SAGE ignora o pedido de envio de acerto de hora transmitido pela UTR;
+ **tk** é o offset relativo à hora de Brasília (se usado a string TZBR) ou à hora de Greenwich (string TZGM) e a localização do Centro de Controle ou a remota. O valor padrão é TZBR= 0;
+ **Mmcdu** é um conjunto de 5 números (M, m, c, d, u) onde:
	+ **u** identifica o nível de implementação do protocolo DNP 3.0 devendo ser 0, 1, 2 ou 3, conforme device profile document do fabricante da UTR ou Centro de Controle associado a esta configuração;
	+ **d** identifica com 1 ou 0 a capacidade de ignorar ou sinalizar erros para endereços de objetos inválidos recebidos na aquisição ou ignora transição do bit de inválido na distribuição;
	+ **c** identifica, com 1, a mandatoriedade em considerar dupla variação de um ponto digital apenas quando dois reports do mesmo são recebidos em estados diferentes;
	+ **m** permite que o indicador de pedido de confirmação não seja ligado nos requests feitos pelo cliente SAGE independentemente do valor de AQPOL;
	+ **M** faz, em 1 e com **u** em 3, com que o cliente SAGE faça, na inicialização do enlace, a habilitação do envio de eventos usando a mensagem com FC20 e, ao verificar algum bit de classe assinalado na internal-indication, faça a leitura das classes com FC01 e repita a habilitação do envio de eventos usando a mensagem com FC20.

**Obs:** Esta string deve seguir a sintaxe: **string= valor**, sem espaço entre a string e o sinal de igual.

### ID
Identificador da configuração física. Deve-se usar um identificação de 2 a 3 caracteres identificando a subestação supervisionada.

### LSC
Chave estrangeira da ligação (LSC) à qual a CNF pertence. Relacionamento 1 -> 1 entre as entidades LSC e CNF.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CNF
		CONFIG= PlPr= 1   LiPr= 1   PlRe= 2   LiRe= 1   SINCR= 1 TZBR= 0 DnpLvl= 3
		    ID= BAY
		   LSC= BAY
	
	CNF
		CONFIG= PlPr= 1   LiPr= 5   PlRe= 2   LiRe= 5   DnpLvl= 103
		    ID= DCOD
		   LSC= DCOD

- - -

# NV1
Na implementação do SAGE para o protocolo DNP 3.0 as ocorrências da entidade NV1 determinam as classes de dados no âmbito da aquisição ou da distribuição.

Nas CNF de distribuição pode-se configurar uma ocorrência de NV1 para cada classe de dados que contém objetos a serem distribuídos pela CNF, e uma ocorrência para os pontos de controle supervisório ligados ao sistema elétrico. Para as CNF de aquisição, deve-se configurar uma única ocorrência relacionada aos pontos aquisitados, outra relacionada aos pontos de controle supervisório ligados ao sistema elétrico e uma terceira relacionada aos pontos de controle de gestão da comunicação de dados.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 4.
	
## Atributos
### CONFIG
Esse atributo só é utilizado para ocorrências de NV1 ligadas a CNFs de distribuição. O atributo defini a que classe do protocolo DNP 3.0 pertencem os objetos vinculados a esta ocorrência de NV1 de distribuição.

	CONFIG= Classe= n

onde n é 0, 1, 2 ou 3, de acordo com a classe desejada.

Os objetos pertencentes a classe zero são considerados objetos unicamente estáticos, ou seja, não são reportados em mensagens de eventos (variação de estado de pontos digitais e valor de pontos analógicos ou totalizadores), sendo reportados apenas em pedidos de scan de valores estáticos correntes. As classes 1 à 3 contêm os objetos que são reportados em eventos, ou seja, sempre que é detectada uma alteração de valor ou estado dos objetos pertencentes à classe, além de também serem reportado num pedido de scan de valores estáticos correntes.

### ID
Identificador do grupo de aquisição, distribuição ou controle. NV1. Sugere-se para este identificador a concatenação, com underscore, dos atributos ID da CNF a que pertence o NV1 e do TN1 e da ORDEM especificados nessa entidade.

### ORDEM
Este atributo deve ser preenchido com um número de 0 a "n" que ordena os grupos de uma CNF e também identifica as diferentes classes escolhidas para os grupos de distribuição.

### CNF
Chave estrangeira da configuração (CNF) à qual o grupo pertence. Relacionamento 1 -> n entre as entidades CNF e NV1.

### TN1
Chave estrangeira do tipo (TN1) à qual o grupo pertence. Relacionamento 1 -> n entre as entidades TN1 e NV1. O domínio válido desse atributo para o conversor de protocolo DNP 3.0 está descrito na entidade TN1.

**Domínio Válido do TN1 para DNP 3.0:**

| ID   | Descrição |
| ---- | ---- |
| NLN1 | Ausência de entidade de nível 1 |
| ADNP | Grupo de Aquisição de dados de UTR em protocolo DNP 3.0 |
| CDNP | Grupo de Controle Supervisório (Nível 2) em protocolo DNP 3.0 |
| GDNP | Grupo de Controle de Gestão de Comunicação de Dados |
| DDNP | Grupo de Distribuição de dados em protocolo DNP 3.0 |
| ODNP | Grupo de Controle Supervisório COS (Nível 3) em protocolo DNP 3.0 |

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NV1
		   CMT= Aquisicao de dados DNP3 
		    ID= BAY_ADNP_1
		   CNF= BAY
		   TN1= ADNP
		 ORDEM= 1
	
	NV1
		   CMT= Comandos Nivel 2 DNP3 
		    ID= BAY_CDNP_2
		   CNF= BAY
		   TN1= CDNP
		 ORDEM= 2
	
	NV1
		   CMT= Distribuicao de dados DNP3 
		CONFIG= Classe= 1
		    ID= DCOD_DDNP_1
		   CNF= DCOD
		   TN1= DDNP
		 ORDEM= 1
	
	NV1
		   CMT= Comandos Nivel 3 DNP3 
		    ID= DCOD_ODNP_2
		   CNF= DCOD
		   TN1= ODNP
		 ORDEM= 2

- - -

# NV2
Na implementação do SAGE para o protocolo DNP 3.0, esta entidade permite ao SAGE classificar os objetos de informação (pontos digitais, analógicos, totalizadores e de controle) em tipos que são reportados em mensagens que agrupam vários pontos de um mesmo tipo. 

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 5.

## Atributos
### CONFIG
Atributo não utilizado. Pode ser usado como comentário sobre o tipo dos objetos de informação configurados sob esse NV2.

### ID
Identificador do NV2. Sugere-se utilizar o ID do NV1 a que pertence o NV2 concatenado por underscore com o atributo TN2.

### ORDEM
Este atributo não é utilizado.

### TPPNT
Tipo dos pontos físicos vinculados a essa NV2.

+ PAF - caso TN2 seja ASTP, AANL ou APFL.
+ PDF - caso TN2 seja ASIM ou ADUP.
+ PTF - caso TN2 seja ATTA.
+ CGF - caso TN2 seja CSIM, CDUP, CREL, CSTP ou CGCD.

### NV1
Chave estrangeira da ocorrência da entidade NV1 à qual o conjunto de dados pertence. Relacionamento 1 -> n entre as entidades NV1 e NV2.

### TN2
Chave estrangeira do tipo de dado sendo configurado dentre os cadastrados na entidade TN2. Relacionamento 1 -> n entre as entidades TN2 e NV2.

**Domínio Válido do TN2 para DNP 3.0:**

| ID   | Descrição |
| ---- | ---- |
| NLN1 | Ausência de entidade de nível 2 |
| ASIM | Aquisição ou distribuição binary input |
| ADUP | Aquisição ou distribuição binary output |
| AANL | Aquisição ou distribuição de analog input |
| ASTP | Aquisição ou distribuição de analog output |
| APFL | Aquisição ou distribuição de floating point input |
| ATTA | Aquisição ou distribuição de binary counter |
| CSIM | Controle supervisório UTR/COS tipo Latch Relay (trip-close) |
| CDUP | Controle supervisório UTR/COS tipo 1-Pulse Relay ("") |
| CREL | Controle supervisório UTR/COS tipo N-Pulse Relay (raise-lower) |
| CSTP | Controle supervisório UTR/COS tipo Analog output (set-point) |
| CGCD | Controle de gestão de comunicação de dados |

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	NV2
		  CMT= Aquisicao BAY de Pontos Digitais DNP3
		   ID= BAY_ADNP_1_ASIM
		  NV1= BAY_ADNP_1
		ORDEM= 1
		  TN2= ASIM
		TPPNT= PDF
	
	NV2
		  CMT= Aquisicao BAY de Pontos Flutuantes DNP3
		   ID= BAY_ADNP_1_APFL
		  NV1= BAY_ADNP_1
		ORDEM= 2
		  TN2= APFL
		TPPNT= PAF
	
	NV2
		  CMT= Controle Latch BAY Nível 2 DNP3
		   ID= BAY_CDNP_2_CSIM
		  NV1= BAY_CDNP_2
		ORDEM= 1
		  TN2= CSIM
		TPPNT= CGF
	
	NV2
		  CMT= Distribuicao DCOD de Pontos Digitais DNP3
		   ID= DCOD_ADNP_1_ASIM
		  NV1= DCOD_ADNP_1
		ORDEM= 1
		  TN2= ASIM
		TPPNT= PDF
	
	NV2
		  CMT= Distribuicao DCOD de Pontos Flutuantes DNP3
		   ID= DCOD_ADNP_1_APFL
		  NV1= DCOD_ADNP_1
		ORDEM= 2
		  TN2= APFL
		TPPNT= PAF
	
	NV2
		  CMT= Roteamento Controle Latch DCOD Nivel 3 DNP3
		   ID= DCOD_ODNP_2_CDUP
		  NV1= DCOD_ODNP_2
		ORDEM= 1
		  TN2= CDUP
		TPPNT= CGF

- - -

# TN1
Entidade que descreve os tipos de grupos que podem existir. Não é preciso alterar essa entidade.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 7.

- - -

# TN2
Esta entidade descreve os tipos de dado que podem existir. Não é preciso alterar essa entidade.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 8.

- - -

# PAF
Configura os pontos analógicos físicos de aquisição e distribuição associados às configurações CNFs das ligações LSCs.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 11.

## Atributos
### ID
Identificador do ponto analógico físico. Deve ser usada uma string resultante da concatenação com underscore do atributo ID do NV2 a que pertence o PAF com o endereço do objeto de informação (index) definido no protocolo DNP 3.0.

### KCONV1
Coeficiente angular de conversão da medida. Usado somente em pontos do tipo AANL.

### KCONV2
Coeficiente linear de conversão da medida. Usado somente para pontos do tipo AANL.

### KCONV3
Este atributo não é utilizado.

### KCONV4
**Parâmetro existente a partir do update 24 do SAGE.**

Se o valor desse atributo for positivo, o processamento "clamp to zero" é ativado. Neste caso, se uma medida aquisitada for menor, em módulo, que o valor definido por este parâmetro, o valor do ponto enviado ao SAC será 0 (zero).

### KCONV5
**Parâmetro existente a partir do update 24 do SAGE.**

Aplica o processamento do tipo "smoothing" ao valor da medida após realizar as conversões pelos valores de KCONV1 e KCONV2. O valor da medida é processado de acordo com a fórmula:

	valor = (1 - KCONV5) * valor_aquisitado + KCONV5 * valor_anterior

### ORDEM
Número sequencial de 0 a 65535 idêntico ao endereço do objeto de informação (index).

### TPPNT
Indica que o ponto físico é de aquisição (PAS) ou de distribuição (PAD).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PAF.

### PNT
Chave estrangeira do ponto analógico lógico ao qual o ponto analógico físico está associado, caso seja um ponto físico de aquisição. Relacionamento 1 -> 1 entre as entidades PAS e PAF, se o ponto físico não for parcela de um filtro; e defini um relacionamento 1 -> n entre as entidades PAS e PAF se o ponto físico for parcela de um filtro.

ou

Identificador do ponto analógico de distribuição ao qual o ponto analógico físico está associado, caso o ponto físico seja de distribuição. Define um relacionamento 1 -> 1 entre as entidades PAD e PAF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PAF
		   ID= BAY_ADNP_1_APFL_0
		  NV2= BAY_ADNP_1_APFL
		ORDEM= 0
		  PNT= BAY:02T1:MVAR
		TPPNT= PAS
		DESC1= Potencia Reativa do 02T1
	
	PAF
		   ID= BAY_ADNP_1_APFL_10
		  NV2= BAY_ADNP_1_APFL
		ORDEM= 10
		  PNT= BAY:02T1:IA
		TPPNT= PAS
		DESC1= Corrente Fase A 02T1
	
	PAF
		   ID= DCOD_DDNP_1_APFL_200
		  NV2= DCOD_DDNP_1_APFL
		ORDEM= 200
		  PNT= COD:02T1:IA
		TPPNT= PAD
		DESC1= Distribuicao Corrente Fase A 02T1

- - -

# PDF
Configura os pontos digitais físicos de aquisição e distribuição associados às configurações CNFs das ligações LSCs.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 13.

## Atributos
### ID
Identificador do ponto digital físico. Deve ser usada uma string resultante da concatenação com underscore do atributo ID do NV2 a que pertence o PDF com o endereço do objeto de informação (index) definido no protocolo DNP 3.0.

### KCONV
Este atributo define a conversão do ponto físico no ponto lógico para aquisição ou vice-versa para distribuição. Ele indica a polaridade do ponto digital e a habilitação da sinalização de sequência de eventos, podendo assumir os valores:

+ **NOR:** Lógica normal com sequência de eventos inibida
+ **INV:** Lógica invertida com sequência de eventos inibida
+ **SQN:** Lógica normal com sequência de eventos habilitada
+ **SQI:** Lógica invertida com sequência de eventos habilitada


### ORDEM
Número sequencial de 0 a 65535 idêntico ao endereço do objeto de informação (index).

### TPPNT
Indica que o ponto físico é de aquisição (PDS) ou de distribuição (PDD).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PDF.

### PNT
Chave estrangeira do ponto digital lógico ao qual o ponto digital físico está associado, caso seja um ponto físico de aquisição. Relacionamento 1 -> 1 entre as entidades PDS e PDF, se o ponto físico não for parcela de um filtro; e defini um relacionamento 1 -> n entre as entidades PDS e PDF se o ponto físico for parcela de um filtro.

ou

Identificador do ponto digital de distribuição ao qual o ponto digital físico está associado, caso o ponto físico seja de distribuição. Define um relacionamento 1 -> 1 entre as entidades PDD e PDF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	PDF
		DESC1= Seccionadora 12D4-1
		   ID= BAY_ADNP_1_ASIM_1
		  NV2= BAY_ADNP_1_ASIM
		ORDEM= 1
		KCONV= SQN
		  PNT= BAY:12D4-1:52
		TPPNT= PDS
		
	PDF
		DESC1= Distribuicao Seccionadora 12D4-1
		   ID= DCOD_ADNP_1_ASIM_10
		  NV2= DCOD_ADNP_1_ASIM
		ORDEM= 10
		KCONV= SQN
		  PNT= COD:12D4-1:52
		TPPNT= PDD

- - -

# PTF
Configura os pontos totalizadores físicos de aquisição e distribuição associados às configurações CNFs das ligações LSCs.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 15.

## Atributos
### ID
Identificador do ponto totalizado físico. Deve ser usada uma string resultante da concatenação com underscore do atributo ID do NV2 a que pertence o PDF com o endereço do objeto de informação (index) definido no protocolo DNP 3.0.

### KCONV1
Coeficiente angular de conversão da medida. Usado somente em pontos do tipo AANL.

### KCONV2
Coeficiente linear de conversão da medida. Usado somente para pontos do tipo AANL.

### ORDEM
Número sequencial de 0 a 65535 idêntico ao endereço do objeto de informação (index).

### TPPNT
Indica que o ponto físico é de aquisição (PTS) ou de distribuição (PTD).

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto físico pertence. Relacionamento 1 -> n entre as entidades NV2 e PTF.

### PNT
Chave estrangeira do ponto totalizador lógico ao qual o ponto totalizador físico está associado, caso seja um ponto físico de aquisição. Relacionamento 1 -> 1 entre as entidades PTS e PTF, se o ponto físico não for parcela de um filtro; e defini um relacionamento 1 -> n entre as entidades PTS e PTF se o ponto físico for parcela de um filtro.

ou

Identificador do ponto totalizador de distribuição ao qual o ponto totalizador físico está associado, caso o ponto físico seja de distribuição. Define um relacionamento 1 -> 1 entre as entidades PTD e PTF.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo
**TODO**

- - -

# CGF
Configura os pontos de controle físicos associados às configurações CNFs das ligações LSCs.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 9.

## Atributos
### ID
Identificador do ponto de controle físico. Deve ser usada uma string resultante da concatenação com underscore do atributo ID do NV2 a que pertence o CGF com o endereço do objeto de informação (index) definido no protocolo DNP 3.0.

### KCONV
Conversão do ponto lógico em ponto físico. 



Em Roteamento, deve apontar para o ID do comando de nível 2.

### ORDEM
Número sequencial de 0 a 65535 idêntico ao endereço do objeto de informação (index).

### CGS
Chave estrangeira do ponto de controle lógico ao qual o ponto de controle físico está associado. Relacionamento 1 -> 1 entre as entidades CGS e CGF. Não é preenchido em caso de ponto de controle físico roteado.

### NV2
Chave estrangeira da ocorrência de NV2 à qual o ponto de controle pertence. Relacionamento 1 -> n entre as entidades NV2 e CGF.

### CNF
Identifica a configuração de aquisição para o ponto de controle físico roteado. Relacionamento 1 -> 1 entre as entidades CNF e CGF. Não utilizado se o ponto de controle físico for de aquisição ou se o ponto controle aquisitado e o roteado acontecem em protocolos diferentes.

### CMT
Preenchimento livre. Destinado a comentários.

### DESC1
Preenchimento livre. Destinado a informações complementares.

### DESC2
Preenchimento livre. Destinado a informações complementares.

## Exemplo

	CGF
		  CMT= Comando Disjuntor 12D4-1
		   ID= BAY_CDNP_2_CSIM_1
		  NV2= BAY_CDNP_2_CSIM
		ORDEM= 1
		KCONV= ON OFF
		  CGS= BAY:12D4-1:52
	
	CGF
		  CMT= Roteamento Comando Disjuntor 12D4-1
		   ID= DCOD_ODNP_2_CDUP_100
		  NV2= DCOD_ODNP_2_CDUP
		ORDEM= 100
		KCONV= BAY_CDNP_2_CSIM_1
		  CNF= BAY
	
	CGF
		  CMT= Roteamento Comando Disjuntor 14c1
		   ID= 4a11-SCSWI1$CO$Pos-d1
		  NV2= 4a11_CSIM
	   OPCOES= SI O3
		KCONV= SBOw TERM
		  CGS= COD:14C1:52

- - -

# RFI
Filtro simples é aquele cujas parcelas (pontos físicos) podem participar de apenas um único filtro gerando ponto lógico.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 16.

## Atributos
### ORDEM
Número sequencial de 1 a n que indica a ordem da parcela no filtro.

### TIPOP
Tipo da parcela do filtro. Pode assumir os valores:

+ PAF - Se a parcela do filtro for um ponto analógico físico.
+ PDF - Se a parcela do filtro for um ponto digital físico.
+ PTF - Se a parcela do filtro for um ponto totalizador físico.

### PNT
Chave estrangeira do ponto físico que compõe a parcela. Relacionamento 1 -> 1 entre as entidades de pontos físicos (PAF, PDF ou PTF. Depende do valor de TIPOP).

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo
**TODO**

- - -

# RFC
Filtro composto é aquele cujas parcelas (pontos físicos) pode participar de vários filtros gerando vários pontos lógicos.

Informações completas em /sage/Manual Protocolos/DNP_3.pdf, página 16.

## Atributos
### ORDEM
Número sequencial de 1 a n que indica a ordem da parcela no filtro.

### TPPARC
Tipo da parcela do filtro. Pode assumir os valores:

+ PAF - Se a parcela do filtro for um ponto analógico físico.
+ PDF - Se a parcela do filtro for um ponto digital físico.
+ PTF - Se a parcela do filtro for um ponto totalizador físico.

### TPPNT
Tipo do ponto resultante do filtro. Pode assumir os valores:

+ PAS - Se o ponto resultante do filtro for analógicos lógicos.
+ PDS - Se o ponto resultante do filtro for digitais lógicos.
+ PTS - Se o ponto resultante do filtro for totalizadores lógicos.

### PARC
Chave estrangeira do ponto físico que compõe a parcela do filtro.

### PNT
**TODO**

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo
**TODO**

- - -

# LSC
Para a implementação SAGE do protocolo DNP 3.0 os atributos aplicados da entidade LSC são apresentados a seguir.

Informações adicionais em /sage/Manual Protocolos/DNP_3.pdf, página 20.

## Atributos
### ID
Identificador longo da Ligação SCADA. Este identificador será utilizado nos alarmes e logs gerados pelo SAGE para eventos ocorridos no âmbito deste protocolo que estejam relacionados com o IED. Recomenda-se um nome descritivo com até 8 caracteres.

### NOME
Nome da Ligação SCADA utilizada na descrição dos alarmes e eventos associados.

### GSD
Chave estrangeira do Gateway-SCADA do SAGE ao qual a Ligação SCADA está associado. Relacionamento 1 -> n entre as entidades GSD e LSC.

### MAP
Chave estrangeira do macro alarme ao qual a ligação SCADA está associado. Relacionamento 1 -> n entre as entidades MAP e LSC.

**Para ligações de Distribuição é aconselhado utilizar o macro alarme GERAL. **

### TCV
Deve ser preenchido com o identificador SAGE para o protocolo DNP 3.0, CNVH.

### TTP
Deve ser preenchido com DNPF3 (iec3d), CXTTD (iec3y), UDPF3 (iec3u), CXTCP (tcps) ou IEC3S (iec3s).

### VERBD
Versão da base de dados.

### TIPO
Deve ser preenchido com o identificador AA para Aquisição e DD para Distribuição..

### NSRV1
Deve ser preenchido com o identificador "localhost".

### NSRV2
Deve ser preenchido com o identificador "localhost".

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	LSC
		 NOME= Aquisicao DNP3 - BAY
		   ID= BAY
		  GSD= SE1
		  MAP= BAY1
		NSRV1= localhost
		NSRV2= localhost
		  TCV= CNVH
		 TIPO= AA
		  TTP= IEC3S
		VERBD= JUL21
	
	LSC
		 NOME= Distribuicao DNP3
		   ID= DCOD
		  GSD= SE1
		  MAP= GERAL
		NSRV1= localhost
		NSRV2= localhost
		  TCV= CNVH
		 TIPO= DD
		  TTP= UDPF3
		VERBD= JUL21

- - -

# CXU
Para a implementação SAGE do protocolo DNP 3.0 os atributos aplicados da entidade CXU são apresentados a seguir.

## Atributos
### ID
Identificador da conexão de comunicação. Deve-se usar um identificação de 2 a 3 caracteres identificando a subestação supervisionada. Recomenda-se utilizar o mesmo identificador utiliza na CNF referente a conexão.

### AQANL 
**TODO**
Tempo em centésimos segundos (0.01) da aquisição periódica de digitais (DO - OBJ10) e analógicas (AO - OBJ40) se a Ligação for de Aquisição. Se for uma Ligação de Distribuição, determina o tempo máximo de espera da resposta de um pedido de controle de nível superior (NHS). Se omitido, é utilizado o valor padrão 200.

### AQPOL
Tempo em centésimos de segundos da verificação de estado OK da UTR (OBJ50) caso seja uma Ligação de Aquisição. Se omitido, é utilizado o valor padrão 100.

### AQTOT
Tempo em centésimos de segundos da aquisição de totalizadores (OBJ20) coso seja uma Ligação de Aquisição. Se omitido, é utilizado o valor padrão 6000.

### FAILP
Tempo em centésimos de segundos do failover para o canal primário. Se omitido, é utilizado o valor padrão 2880000.

### FAILR
Tempo em centésimos de segundos do failover para o canal reserva. Se omitido, é utilizado o valor padrão 30000.

### GSD
Chave estrangeira do Gateway-SCADA do SAGE (par de servidores) ao qual a conexões de comunicação SCADA está associado. Relacionamento 1 -> n entre as entidades GSD e CXU.

### INTGR
Tempo em centésimos de segundos da integridade do canal. Se omitido, é utilizado o valor padrão 18000.

### NFAIL
Número de tentativas de failover. Se omitido, é utilizado o valor padrão 5.

### ORDEM
Deve ser preenchido com um número sequencial de 1 a 9999 com a ordem da conexão UTR dentro do GSD.

### SFAIL
Tempo em segundos do sincronismo de failover. Se omitido, é utilizado o valor padrão 200.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	CXU
		   ID= BAY
		  GSD= SE1
		AQANL= 200
		AQPOL= 200
		AQTOT= 60000
		FAILP= 0
		FAILR= 0
		INTGR= 18000
		NFAIL= 5
		ORDEM= 1
		SFAIL= 200
	
	CXU
		   ID= DCOD
		  GSD= SE1
		AQANL= 4000
		AQPOL= 1500
		AQTOT= 200
		FAILP= 0
		FAILR= 0
		INTGR= 5000
		ORDEM= 2

- - -

# UTR
Para a implementação SAGE do protocolo DNP 3.0 os atributos aplicados da entidade UTR são apresentados a seguir.

### ID
Identificador da unidade terminal. Recomenda-se a concatenação por underscore do CNF com a inicial da ORDEM.

### CNF
Chave estrangeira da configuração física à qual a unidade terminal está associada. Relacionamento 1 -> 2 entre as entidades CNF e UTR.

### CXU
Chave estrangeira da conexão da UTR à qual a unidade terminal está associada. Relacionamento 1 -> n entre as entidades CXU e UTR.

### ENUTR
Endereço DNP da UTR.

### NTENT
Número de tentativas de religação antes de gerar falha de comunicação.

### ORDEM
Ordem de prioridade para utilização. PRI para primário e REV para reserva.

### RESPT
Tempo limite em segundos para resposta.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	UTR
		   ID= BAY_P
		ORDEM= PRI
		  CNF= BAY
		  CXU= BAY
		ENUTR= 2
		NTENT= 3
		RESPT= 1500
	
	UTR
		   ID= BAY_R
		ORDEM= REV
		  CNF= BAY
		  CXU= BAY
		ENUTR= 0
		NTENT= 3
		RESPT= 1500
	
	UTR
		   ID= DCOD_P
		ORDEM= PRI
		  CNF= DCOD
		  CXU= DCOD
		ENUTR= 2
		NTENT= 3
		RESPT= 1500

- - -

# ENU
Para a implementação SAGE do protocolo DNP 3.0 os atributos aplicados da entidade ENU são apresentados a seguir.

## Atributos
### ID
Identificador do enlace. Deve-se usar a concatenação por undescore do CXU com a inicial da ORDEM.

### CXU
Chave estrangeira da Conexão de Comunicação com UTR e CANAL ao qual o enlace está associado. Relacionamento 1 -> 2 entre as entidades CXU e ENU.

### ORDEM
Ordem de prioridade do enlace. Deve-se utilizar **PRI** para primário e **REV** para reserva.

### VLUTR
Velocidade em bits por segundo do canal. Se omitido, é utilizado o valor padrão 2400.

### TRANS
Tempo em segundos do temporizador de transmissão. Se omitido, é utilizado o valor padrão 50.

### TDESC
Tempo em segundos do descanso para reinício. Se omitido, é utilizado o valor padrão 3.

## Exemplo

	ENU
		   ID= BAY_P
		  CXU= BAY
		ORDEM= PRI
		TRANS= 4
		TDESC= 2
		VLUTR= 9600
	
	ENU
		   ID= BAY_R
		  CXU= BAY
		ORDEM= REV
		TRANS= 4
		TDESC= 2
		VLUTR= 9600
	
	ENU
		   ID= DCOD_P
		  CXU= DCOD
		ORDEM= PRI
		TRANS= 4
		TDESC= 2
		VLUTR= 9600
	
	ENU
		   ID= DCOD_R
		  CXU= DCOD
		ORDEM= REV
		TRANS= 4
		TDESC= 2
		VLUTR= 9600

- - -

# TAC
Para a implementação SAGE do protocolo DNP 3.0 os atributos aplicados da entidade TAC são apresentados a seguir.

## Atributos
### ID
Identificador do terminal de aquisição. Deve-se usar um identificação de 2 a 3 caracteres identificando o terminal supervisionado. Sugere-se para este identificador o mesmo da entidade CNF.

### NOME
Nome do terminal de aquisição utilizado por alarmes e eventos associados.

### INS
Chave estrangeira da Instalação a qual o terminal de aquisição está associado. Relacionamento 1 -> n entre as entidades INS e TAC.

### LSC
Chave estrangeira da Ligação SCADA ao qual ao terminal de aquisição está associado. Relacionamento 1 -> n entre as entidades LSC e TAC.

### TPAQS
Tipo da aquisição.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TAC
		   ID= BAY
		  INS= SE1
		  LSC= BAY
		 NOME= Aquisicao DNP3 - Bay1
		TPAQS= ASAC

- - -

# TDD
Para a implementação SAGE do protocolo DNP 3.0 os atributos aplicados da entidade TDD são apresentados a seguir.

## Atributos
### ID
Identificador do terminal de distribuição. Deve-se usar um identificação de 2 a 3 caracteres identificando o terminal supervisionado.

### NOME
Nome do terminal de distribuição utilizado por alarmes e eventos associados.

### LSC
Chave estrangeira da Ligação SCADA ao qual ao terminal de distribuição está associado. Relacionamento 1 -> n entre as entidades LSC e TDD.

### CMT
Preenchimento livre. Destinado a comentários.

## Exemplo

	TDD
		  ID= COD
		NOME= Term. Distribuicao DNP3
		 LSC= DCOD

- - -

# Configurando o Arquivo hosts
**TODO**
O arquivo padrão /etc/hosts, que contém o endereçamento IP cadastrado localmente nos sistemas UNIX, deverá ser editado pelo usuário para que o transportador MMST possa conhecer os dois endereços IP que podem ser associados a cada um dos dois IEDs (principal e reserva) remotos, totalizando assim até 4 endereços IP para endereçar os IEDs remotos de uma MULL/CNF/LSC.

	<IP_Addres> host_dnp_<placa>_<linha>

onde

+ final 1 é a transportadora principal
+ final 1b é a reserva da transportadora principal
+ final 2 é a transportadora secundária
+ final 2b é a reserva da transportadora secundária

Se existir apenas um canal de comunicação, as transportadoras podem ser agrupadas no mesmo endereço IP

	<IP_Addres> host_mms_<id_cnf>1 host_mms_<id_cnf>1b host_mms_<id_cnf>2 host_mms_<id_cnf>2b

## Exemplo

	192.168.101.42	host_dnp_1_2

- - -