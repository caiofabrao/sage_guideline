[//]: # "******************************************************************************"
[//]: # " Project           : In-Tech / Interno                                        "
[//]: # " Author            : caio.fabrao@intech-automacao.com.br                      "
[//]: # " Date              : 2021/02/10                                               "
[//]: # "                                                                              "
[//]: # " Revision history                                                             "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Date          Author  Revision                                               "
[//]: # " 2021/02/10    caf     First commit.                                          "
[//]: # " 2021/05/13    caf     Rev01. Remocao de sessoes pouco usadas                 "
[//]: # "------------------------------------------------------------------------------"
[//]: # " Copyright © 2021 by In-Tech Automacao & Sistemas. All Rights Reserved.       "
[//]: # "******************************************************************************"

- - -

# Guideline SAGE
Neste documento está reunido diversas informações a respeito de desenvolvimento de aplicações em SAGE. Há desde guias de instalações até configurações avançadas.

Está presente no documento:

+ [Documentos de Referência](#markdown-header-documentos-de-referencia)
+ [Principais Arquivos DAT](#markdown-header-principais-arquivos-dat)
	+ [Rede de Difusão Confiável](#markdown-header-rede-de-difusao-confiavel)
	+ [Serviço de Comunicação de Dados](#markdown-header-servico-de-comunicacao-de-dados)
	+ [Alarmes e Eventos](#markdown-header-alarmes-e-eventos)
	+ [Serviço de Aquisição e Controle (SAC)](#markdown-header-servico-de-aquisicao-e-controle-sac)
	+ [Pontos Físicos](#markdown-header-pontos-fisicos)
	+ [Serviço de Distribuição de Dados (SDD)](#markdown-header-servico-de-distribuicao-de-dados-sdd)
	+ [Outros](#markdown-header-outros)
+ [Usuários](#markdown-header-usuarios)
+ [Drivers de comunicação](#markdown-header-drivers-de-comunicacao)
	+ [Outros arquivos](#markdown-header-outros-arquivos)
+ [Servidor de Hora](#markdown-header-servidor-de-hora)
	+ [Configurar o Servidor de Hora](#markdown-header-configurar-o-servidor-de-hora)
	+ [Verificar o Sincronismo](#markdown-header-verificar-o-sincronismo)
+ [Executar Comandos Sem Bloquear o Terminal](#markdown-header-executar-comandos-sem-bloquear-o-terminal)
+ [Criar Bases SAGE](#markdown-header-criar-bases-sage)
+ [Criar Base de Dados](#markdown-header-criar-base-de-dados)
	+ [Base para IEC/61850](#markdown-header-base-para-iec61850)
		+ [Leitura de Arquivos ICD/CID/SCD](#markdown-header-leitura-de-arquivos-icdcidscd)
		+ [Configuração dos Arquivos DATs](#markdown-header-configuracao-dos-arquivos-dats)
		+ [Configuração OPMSK](#markdown-header-configuracao-opmsk)
	+ [Base para 104](#markdown-header-base-para-104)
+ [Design de Tela](#markdown-header-design-de-tela)
	+ [Alterações em Telas Existentes](#markdown-header-alteracoes-em-telas-existentes)
+ [Menu de Navegação](#markdown-header-menu-de-navegacao)
+ [Problemas Comuns](#markdown-header-problemas-comuns)
+ [Glossário](#markdown-header-glossario)

- - -

# TODO
Lista de informações a serem adicionadas ao documento

+ Sessão: Uso Avançado do comando info-tr.
+ Sessão: Uso Avançado do comando grep.
+ Sessão: Uso Avançado do comando sincconfig.
+ Sessão: Explicando os atributos dos DATs.
	+ Para protocolo 101
	+ Para protocolo 104
+ Configuração terminal server
	+ usar opmsk 5 para debug (gera log) e 3 para final (apenas mensagem de erro)

- - -

# Documentos de Referência
Junto a este documento, há uma estrutura de diretórios contendo guias e manuais de referência sobre o SAGE.

A árvore de arquivos e diretórios é a apresentada abaixo.

+ **Base de Dados**
	+ **Base_Fonte_do_SAGE.pdf:** Apresenta nome, tipo, valor padrão e descrição dos campos das tabelas (.dat) da base fonte.
	+ **Base_Referencia_do_SAGE.pdf:** Apresenta os campos das tabelas da base de referência.
+ **DATs Básicos:** Contém as entidades DATs que podem .
	+ **cxp.dat**
	+ **inp.dat**
	+ **ocr.dat**
	+ **pro.dat**
	+ **sev.dat**
	+ **sxp.dat**
	+ **tvc.dat**
	+ **ttp.dat**
+ **Introdutório:** Palestra introdutória sobre o SAGE.
	+ **Visão Geral:** Contém uma visão geral sobre o SAGE.
		+ **Modulo_1_Introducao.pdf**
		+ **Modulo_2_Interface_Grafica.pdf**
		+ **Modulo_3_Configuracao_Base_Dados.pdf**
	+ **Arvore_Diretorios_e_Scripts_SAGE.pdf**
	+ **Base_Dados_SAGE.pdf**
	+ **Conceitos_Basicos_SAGE.pdf**
	+ **Protocolo_DNP_3.pdf**
	+ **Protocolo_IEC_101.pdf**
	+ **Protocolo_IEC61850.pdf**
	+ **Protocolo_TASE2.pdf**
+ **Manual Comunicação:** Contém os manuais de configuração dos diferentes tipos de comunicação suportado pelo SAGE.
	+ **Porta_COM.pdf**
	+ **Terminal_Server.pdf**
+ **Manual Configuração:** Contém os manuais de configuração do SAGE para os modos EMS e SCADA.
	+ **Base_Fonte_EMS.pdf:** Diretrizes para configurar a Base fonte com SAGE em modo EMS.
	+ **Base_Fonte_SCADA.pdf:** Diretrizes para configurar a Base fonte com SAGE em modo SCADA.
	+ **SAGE_EMS.pdf:** Diretrizes para configurar o SAGE em modo EMS.
	+ **SAGE_SCADA.pdf:** Diretrizes para configurar o SAGE em modo SCADA.
	+ **SAGE_SNMP.pdf:** Diretrizes para configurar o SAGE com SNMP.
+ **Manual Protocolos:** Contém os manuais de como definir a aquisição/distribuição de pontos físicos em diversos protocolos.
	+ **AL1000.pdf**
	+ **Conitel.pdf**
	+ **DNP_3.pdf**
	+ **ICCP-MMS_com_TASE_2.pdf**
	+ **IEC60870-5-101.pdf**
	+ **IEC60870-5-104.pdf**
	+ **IEC61850.pdf**
	+ **Leeds_Northrup_IEC870-5_LN57-3.pdf**
	+ **Microlab.pdf**
	+ **MODBUS.pdf**
	+ **Redac_da_Westinhouse.pdf**
	+ **SINSC_Modo_Escravo.pdf**
	+ **SINSC_Modo_Mestre.pdf**
	+ **SINSC_Transporte_SPTDS.pdf**
	+ **Transportadores.pdf:** Configurações adicionais de transporte via Terminal Server, TCP/IP, Porta COM, e outros.
+ **Manual Usuário:** Contém os manuais de uso dos diversos aplicativos do SAGE.
	+ **Manual Aplicativo**
	+ **Sigdraw.pdf**
	+ **Visor_de_Acesso.pdf**
	+ **Visor_de_Alarmes.pdf**
	+ **Visor_de_base.pdf**
	+ **Visor_de_calculos.pdf**
	+ **Visor_de_capacidade.pdf**
	+ **Visor_de_historico.pdf**
	+ **Visor_de_logs.pdf**
	+ **Visor_de_processos.pdf**
	+ **Visor_de_Telas.pdf**
	+ **Visor_de_tendencias.pdf**
	+ **WallTrend.pdf**
+ **Mensagens de Erro:** Contém listas com mensagens de erros comuns e suas possíveis soluções.
	+ **Mensagens_de_Erro_comum.doc**
	+ **Mensagens_de_Erro_dnp3.doc**
	+ **Mensagens_de_Erro_i101.doc**
+ **Outros**: Arquivos que ainda **não** foram analisados/organizados.
+ **Quick Guide:** Contém guias e manuais rápidos sobre os itens mais comuns.
	+ **comandos_sage_linux.md:** Guia rápido com os principais comandos do SAGE e do Linux.
	+ **config_alarmes_eventos.md:** Guia rápido de configuração de alarmes e eventos.
	+ **config_dnp3.md:** Guia rápido de configuração da aquisição/distribuição para o protocolo DNP 3.0.
	+ **config_iec61850.md:** Guia rápido de configuração da aquisição/distribuição para o protocolo IEC/61850.
	+ **config_pontos_fisicos.md:** Guia rápido de configuração dos pontos físicos.
	+ **config_rede_difusao_confiavel.md:** Guia rápido de configuração de rede com difusão confiável.
	+ **config_servico_aquisicao_controle.md:** Guia rápido de configuração do serviço de aquisição e controle de dados.
	+ **config_servico_comunicacao_dados.md:** Guia rápido de configuração do serviço de comunicação de dados.
	+ **config_servico_distribuicao_dados.md:** Guia rápido de configuração do serviço de distribuição de dados.
	+ **Reference_Card_SAGE.pdf:** Cartão de referência com informações básicas para o uso do SAGE.
+ **SAGE**
	+ **Administração_SAGE.pdf:** Manual completo para administração do SAGE. Contém definição de scripts, processos entre outros.
	+ **Aplicacao_do_Usuario.pdf:** Guia de desenvolvimento de aplicações do usuários a serem executadas no processador de cálculos (calc).
	+ **SageFEED.pdf:** Guia de configuração e manutenção do SAGE em modo multi-base em máquinas virtuais.
+ **Script:** Contém scripts uteis para administração e manutenção do SAGE.
	+ **incluidats.sh:** Script para adicionar a referencia de novos dats à base de dados.
+ **Teste Ligação:** Contém guias oficiais da CEPEL para testar a ligação de dados sobre um protocolo.
	+ **Teste_101.pdf**
	+ **Teste_104.pdf**
	+ **Teste_DNP3.pdf**
	+ **Teste_IED_61850.pdf**
+ **acesso_remoto.md:** Informações sobre acesso remoto ao CentOS SAGE.
+ **boas_praticas.md:** Guia de boas práticas de desenvolvimento em SAGE.
+ **instalacao_centOS.md:** Guia de instalação do CentOS SAGE.

- - -

# Estrutura do Sistema SAGE

- - -

# Principais Arquivos DAT

## Rede de Difusão Confiável
![Alt text](Documentos de Referência/Quick Guide/imgs/dc_der.png)

+ **PRO:** Classe de Processo
+ **CXP:** Relacionamento Classe MCD x Classe de Processo
+ **INP:** Instância de Processo
+ **NOH:** Nó da Rede
+ **SXP:** Relacionamento Severidade x Classe de Processo
+ **SEV:** Severidade
+ mcd;ctx;inm;prct;noct

## Serviço de Comunicação de Dados 
![Alt text](Documentos de Referência/Quick Guide/imgs/servico_com_dados_der.png)

+ **TCV:** Tipos de Conversor de Protocolo
+ **TTP:** Tipos de Transportadores de Protocolo
+ **GSD:** Gateway SCADA
+ **CXU:** Conexões de Comunicação com UTRs e Canais
+ **ENU:** Enlaces de Conexões com UTRs
+ **UTR:** Unidade Terminal Remota
+ **MUL:** Multiligação em protocolo X25-X75
+ **ENM:** Enlace de multiligação
+ cnm

## Alarmes e Eventos
![Alt text](Documentos de Referência/Quick Guide/imgs/alarmes_eventos_der.png)

+ **MAP:** Macro Alarmes
+ **E2M:** Relacionamento Ponto de Tempo Real ou Ocorrência x Macro Alarmes
+ **OCR:** Ocorrências do Sistema
+ **TELA:** Telas associadas a Ocorrências

## Serviço de Aquisição e Controle (SAC)
![Alt text](Documentos de Referência/Quick Guide/imgs/sac_der.png)

+ **LSC:** Ligação SCADA
+ **INS:** Instalação
+ **TAC:** Terminal de Aquisição e Controle
+ **CGS:** Ponto de Controle Genérico do SAC
+ **PAS:** Ponto Analógico Lógico
+ **PTS:** Ponto Totalizado Lógico
+ **PDS:** Ponto Digital Lógico
+ **RCA:** Relacionamento de Pontos Lógicos x Pontos Calculados
+ **TCL:** Tipo de Cálculos para Pontos de Medição
+ **TCTL:** Tipo de Ponto de Controle
+ **REGRA:** Regra para o Registrador Condicional
+ **GRUPO:** Grupo de Componentes para Diálogo Automático
+ **GRCMP:** Componentes para Diálogo Automático

## Pontos Físicos
![Alt text](Documentos de Referência/Quick Guide/imgs/pontos_fisicos_der.png)

+ **CNF:** Configuração da Ligação Física do SCD
+ **NV1:** Nível 1 da Configuração Física
+ **NV2:** Nível 2 da Configuração Física
+ **TN1:** Tipos de Entidades Físicas do Nível 1
+ **TN2:** Tipos de Entidades Físicas do Nível 2
+ **CGF:** Ponto de Controle Físico
+ **PAF:** Pontos Analógicos Físicos
+ **PDF:** Pontos Digitais Físicos
+ **PTF:** Pontos Totalizados Físicos
+ **RFC:** Relação de Filtro Composto
+ **RFI:** Relação de Filtro do SCD

## Serviço de Distribuição de Dados (SDD)
![Alt text](Documentos de Referência/Quick Guide/imgs/sdd_der.png)

+ **TDD:** Terminal de distribuição de dados
+ **PAD:** Pontos analógicos de distribuição (relação PAS x TDD)
+ **PDD:** Pontos digitais de distribuição (relação PDS x TDD)
+ **PTD:** Pontos totalizados de distribuição (relação PTS x TDD)

## Outros
+ **LIA:** Ligação de Aquisição e Controle
+ **LID:** Ligação de Distribuição
- - -

# Usuários
+ root@root root
+ sage@sage

- - -

# Drivers de comunicação
Os instaladores e desinstaladores dos drivers dos protocolos de comunicação se encontram no diretório: 

	$SAGE/drivers

Como **Super Usuário**, execute o arquivo do protocolo desejado.

O comando `ls | grep "instala"` listará apenas os arquivos de instalação e desinstalação.

| Arquivo | Descrição |
| ---- | ----|
| `instala_mms` | Instala o driver para IEC/61850 |

## Outros arquivos 
|||||
|----|----|----|----|
| `desinstala_a32`	| `desinstala_transportes`	| `instala_hpux`		| `instala_mms` |
| `desinstala_cep`	| `desinstala_tsr`			| `instala_iec`			| `instala_S98` |
| `desinstala_cyc`	| `desinstala_x328`			| `instala_iec1`		| `instala_sin` |
| `desinstala_cyp`	| `instala_a32`				| `instala_iec3`		| `instala_snmpd` |
| `desinstala_cyy`	| `instala_cep`				| `instala_ifcfg`		| `instala_solaris` |
| `desinstala_cyys`	| `instala_cyc`				| `instala_linux`		| `instala_transportes` |
| `desinstala_iec`	| `instala_cyp`				| `instala_linux.bug`	| `instala_tsr` |
| `desinstala_iec1`	| `instala_cyy`				| `instala_linux.cd`	| `instala_tz` |
| `desinstala_iec3`	| `instala_cyys`			| `instala_linux.org`	| `instala_wdog` |
| `desinstala_sin`	| `instala_decunix`			| `instala_linux_trpd`	| `instala_x328` |

- - -

# Conversores de Protocolo
+ **C32:** - Conitel C3200 (proprietário Leeds & Northrup)0000
+ **LN57:** - Protocolo baseado no padrão FT3 IEC/870-5 (proprietário L&N)
+ **CNO:** - Protocolo SINSC modo mestre (padronizado pela ELETROBRAS)
+ **COS:** - Protocolo SINSC modo escravo (idem)
+ **RDAC:** - Redac 70F (proprietário Westinghouse)
+ **AB1771:** - Protocolo padrão de CLPs Allen-Bradley utilizado em Relés Digitais
+ **IEC101:** - Protocolo padronizado IEC/870-5-101
+ **DNP3:** - Protocolo padronizado DNP Versão 3.0
+ **MODBUS:** - Protocolo padronizado utilizado em CLPs e Relés Digitais

- - -

# Transportadores de Protocolo
+ **IECD** - Conexões seriais em frames FT3 do IEC/870-5-1
+ **IEC1T** - Conexões TCP-IP em modo balanceado FT1.2 do IEC/870-5-2
+ **IEC1D** - Conexões seriais em modo balanceado FT1.2 do IEC/870-5-2
+ **IEC2D** - Conexões seriais em modo não balanceado FT1.2 do IEC/870-5-2
+ **IEC3U** - Conexões TCP-IP em FT3 do IEC/870-5
+ **IEC3D** - Conexões seriais em FT3 do IEC/870-5
+ **CXA32** - Conexões seriais modo Assíncrono-32 bits (síncrono)
+ **CX3/28** - Conexões seriais do protocolo CCITT X3/X28
+ **MLX25** - Multiligações ser imaginiais bit-síncronas do protocolo CCITT X25/X75
+ **CXX25** - Conexões seriais bit-síncronas do protocolo CCITT X25
+ **CXTCP** - Transporte genérico do application level de Conexões
+ **MLTCP** - Transporte genérico do application level de Multiligações 

- - -

# Servidor de Hora
## Configurar o Servidor de Hora
1. Abrir o Terminal e executar `system-config-time &`
1. Acessar a aba **"Network Time Protocol"**
1. Habilitar a opção **"Ativar Network Time Protocol"**
1. No campo **"Servidores NTP"**, excluir os registros padrões
1. Clicar em **<Adicionar\>** e informar o IP do servidor de hora
1. Habilitar a opção **"Sincronize o relógio do sistema antes de iniciar o serviço"**

## Verificar o Sincronismo
Para verificar se o servidor SAGE está devidamente sincronizado com o servidor de horas, execute o procedimento via terminal:

1. Acessar a conta de **Super Usuário**
1. Executar o comando `service ntpd restart` para reiniciar o processo de sincronismo
	+ **"Desligando o ntpd"** deve retornar status OK
	+ **"ntpd: sincronizando com o servidor de hora"** deve retornar status OK
	+ **"Iniciando ntpd"** deve retornar status OK
1. Em seguida, executar `service ntpd status` para verificar se o processo está em execução

Se durante o processo de reinicio do serviço retornar falha, deve-se investigar os arquivos de configuração do NTP:

+ /etc/ntp.conf
+ /etc/ntp/netservers
+ /etc/ntp/setp-tickers

- - -

# Alterar o Hostname
+ Entrar como **Super Usuário**
+ Alterar o hostname
	+ `hostname <nome_computador>`
+ Abrir o arquivo de configuração de rede
	+ `vi /etc/sysconfig/network`
+ Alterar a linha `HOSTNAME=`para contem o novo hostname
	+ `HOSTNAME=<nome_computador>`

- - -
# Alterar IP
+ Entrar como **Super Usuário**
+ Navegar para o diretório de configuração de rede
	+ `cd /etc/sysconfig/network-scripts`
+ Alterar a linha `IPADDR` do arquivo da interface de rede para alterar o IP
	+ `sed -i '6s/.*/IPADDR=<NOVO_IP>/' ifcfg-<INTERFACE_REDE>`
+ Alterar a linha `NETMASK` do arquivo da interface de rede para alterar o Máscara de rede
	+ `sed -i '7s/.*/NETMASK=<NOVA_MASCARA>/' ifcfg-<INTERFACE_REDE>`
+ Alterar a linha `GATEWAY` do arquivo da interface de rede para alterar o IP do Gateway
	+ `sed -i '8s/.*/GATEWAY=<IP_GATEWAY/' ifcfg-<INTERFACE_REDE>`
	+ `service network restart`
	
## Script

	$sudo su -c sed -i '7s/.*/NETMASK=172.22.207.224/' ifcfg-bond0 ; service network restart"
- - -

# Converter ".bz2" para ".Z"
O comando `instala_sage` requer que o arquivo cnf esteja no formato "sagecnf_<base>.tar.Z". Como a compressão ".Z" deixa o arquivo muito grande, é comum utilizar a compressão ".bz2" em vez.

Para converter de ".bz2" para ".Z", pode-se usar o comando:
	
	bunzip2 -c < file.tar.bz2 | gzip -c > sagecnf_<base>.tar.Z

- - -

# Executar Comandos Sem Bloquear o Terminal
Ao abrir programas com GUI, utilize **&** (com ou sem espaço) após o comando para manter o terminal desbloqueado.

**Exemplo:**

	SigDraw &
	SigDraw&

- - -

# Transferência de Arquivos Entre Servidores
Se for preciso passar vários arquivos de diferentes diretórios entre servidores SAGE, pode-se utilizar o programa gFTP para facilitar o processo de transferência.

1. Executar via Terminal `gftp`
1. Na parte superior ficam as informações para o acesso remoto ao segundo servidor
1. O botão mais a esquerda (ícone de dois computadores) é para iniciar a conexão
	+ O painel da esquerda lista os arquivos do servidor local
	+ O painel da direta lista os arquivos do servidor remoto
1. As setas entre os painéis são utilizadas para transferir os arquivos selecionados de um servidor para o outro
	+ Seta para a direita transfere do servidor local para o remoto
	+ Seta para a esquerda transfere do servidor remoto para o local
1. Caso o arquivo a ser transferido já exista, será pedido uma confirmação para ignorar ou sobrescrever o arquivo

- - -

# Diretórios SAGE
## IHM
+ **ListaDisplays.dat:** Lista os displays que podem exibir a aplicação SAGE. Contém os displays da rede, locais e remotos.
+ **Usuarios.dat:** Usuários cadastrados no sistema via CadUsuário (a senha encontra-se criptografada).
+ **NumeroMaximoPrgs.dat:** Defini o número máximo de visores do mesmo tipo que podem ser abertos local e remotamente.
+ **VTelasBotoes.led:** Configura a barra menu de navegação do sistema.
+ **[login].prv:** Define os privilégios gerais atibuídos a um usuário.
+ **[login].prv.[maquina]:** define os privilégios de um usuário quando em uma máquina especifica.

- - -

# SLOG Vs Tail
Utilize o comando `slog` para monitorara o log de mensagens do SAGE de forma rápida.

Caso esteja difícil acompanhar as mensgens, realize o comando `tail` no arquivo de log utilizado pelo slog.

	tail -f /var/log/messages

É ainda possivel combinar o comando `tail` com os comandos `grep` e `egrep` para melhorar a usabilidade do monitoramento.

Por exemplo, `tail -f /var/log/messages | grep <palavra_chave>` filtra o monitoramento para exibir apenas as linhas que contenham uma determinada palavra chave.

- - -

# Comando TAIL + GREP

**Uso:** `tail -fn 20 $ARQS/<arquivo_evento> | egrep --color=always -e '<padrao>' -e $`

**Exemplo:** `tail -fn 20 $ARQS/fev0921.sde | GREP_COLOR='01;31' egrep --color=always -e '^[^NAO]+.DETECTADO.*'`

## Padrões usuais
| Padrão            | Efeito |
| ----              | ----   |
| ^[^NAO DETECTADO] | Ocorrências de "Não Detectado" |
| +.DETECTADO.\*    | Ocorrências de "Detectado" (sem o Não) |
| .\*ATUADO.\*\|$ | Ocorrências de Atuado e Desatuado |	

- - -

# Criar Bases SAGE
1. Criar uma nova estrutura de dados
	+ `cria_base <nome_base>`
1. Habilita a base como default
	+ `habilita_base <nome_base>` 
1. Reiniciar as variáveis de ambiente
	+ `CTRL \+ ALT \+ BACKSPACE` \- (Reinicia o Ambiente Gráfico do Linux)
	+ Sair e reconectar o acesso remoto (Se utilizando acesso remoto)
1. Enviar os arquivos OCR.dat e TCTL.dat (caso não existam) para a pasta de dados
	+ **$BD/dados**

- - -

# Criar Base de Dados
## Base para IEC/61850
1. Iniciar o processo
	+ `cria_base_61850`
1. Enviar os arquivos .CID e/ou .SCD para a pasta xml61850
	+ /export/home/sage/sage/config/<nome\_base\>/bd/dados/xml61850
1. Abrir o programa de leitura de CID
	+ `ativa xml61850`

### Leitura de Arquivos ICD/CID/SCD
1. Iniciar o projeto
	+ Arquivo \> Novo projeto
1. Alterar o nome dos IEDs
	+ Configuração \> Alterar os nomes dos IEDs
1. Remover os IEDs não utilizados
	+ Botão direito \> Remover IEDs
1. Configurar os parâmetros do CNF
	+ Botão direito \> Altera config da CNF
1. Salvar o projeto
	+ Arquivo \> Salvar projeto 
	+ (Usar o nome da base por padrão)
1. Gerar os DATs
	1. Arquivo \> Gerar arquivos dat...
	1. Selecionar /export/home/sage/sage/config/<nome\_base\>/bd/dados/xml61850
	1. Selecione "Não concatenar dats"

**OBS:** Concatenar significa que os DATs gerados serão adicionados aos DATs principais. Essa técnica tem suas vantagens, porém dificultam muito no momento de realizar correções na base. O melhor a ser feito é não concatenar e realizar a inclusão dos novos DATs por include.

### Configuração dos Arquivos DATs
O detalhamento dos DATs necessários para configurar a aquisição e distribuição de pontos físicos sobre protocolo IEC/61850 estão no arquivo **Quick Guide/config_iec61850.txt**

### Configuração OPMSK
Cada marca de IED possui um OPMSK diferente.

| Fabricante | Equipamento | OPMSK	|
| :----:	| :----:	  | :----:   |
| SEL		| Controle redundante | 00089431 |
| SEL		| Relé		| 00088031 |
| SEL		|			 | 00008931 |
| SEL		| Teste	   | 0000F171 |
| Areva		|			 | 00028020 |

Mais informações sobre OPMSK podem ser encontradas no arquivo **Quick Guide/OPMSK.xmls**

## Base para 104
1. Remover as linhas comentadas de PDS, PAS, PTS e CGS
	+ Filtrar por cor
	+ Selecionar todas as linhas
	+ Localizar \> Ir para Especial \> Somente células visíveis
	+ Apagar
1. Adicionar um "x" após a coluna INC
1. Abrir **Sage\_Geral.xls**
	+ (1) Código da SE
	+ (4) Arquivo **template_sage.xls**
	+ (5) ENUTR = 10
		+ **PlPr** e **LiPr** = 1 
		+ **PlRe** e **LiRe** = 21
		+ **OrdemDif** = 1000
		+ **OrdemAna** = 2000
		+ **OrdemTot** = 3000
		+ **OrdemCmd** = 4000
	+ (6) Check Distribuição
		+ Check Aquisição
		+ Criar

- - -

# Design de Tela
Atalhos do SigDraw

| Atalho | Função |
| ---- | ---- |
| **CTRL \+ A**		| Edição das propriedade do objeto selecionado |
| **CTRL \+ T**		| Selecionar todos os objetos |
| **CTRL \+ U**		| Desfazer a última ação |
| **CTRL \+ C**		| Copiar o objeto selecionado |
| **CTRL \+ X**		| Recortar o objeto selecionado |
| **Ins**			| Colar um objeto da área de transferência|
| **CTRL \+ Ins**	| Copiar o texto selecionado |
| **Shift \+ Ins**	| Colar um texto da área de transferência |
| alt shift insert| Testar |
| **Delete**		| Apagar o objeto selecionado |
| **Capslock**		| Impedir copia e colagem |
| **CTRL \+ L**		| Alinhar os objetos selecionados com relação à posição do primeiro objeto selecionado |
| **CTRL \+ G**		| Agrupar os objetos selecionados |
| **CTRL \+ D**		| Desagrupar os objetos de um grupo |

Opções da tela de propriedade do objeto

| Opção | Função|
| ---- | ---- |
| **"..."** | Selecionar o ponto da base |
| **"Mais..."** | Caption do objeto Texto |

Ao selecionar um ponto

| Caixa | Valor | Descrição |
| ---- | :----: | ---- |
| **Select** | **a1_flags**	| Ponto digital completo/abrir janela de comandos |
|  | **a1_flags@bit0** | Valor atual do ponto digital |
|  | **a1_flags@bit7** | Flag de alarme reconhecido do alarme associado ao ponto digital |
|  | **valor** | Valor atual do ponto analógico |
| **From** | **pds** | Pontos Digitais |
|  | **pas** | Pontos Analógicos |
|  | **pts** | Pontos Totalizadores |
|  | **cgs** | Comandos |
| **Where** | **id** | Coluna desejada da tabela |
|  | **TAG** | TAG desejada |

## Alterações em Telas Existentes
É possível abrir as telas do SigDraw com um editor de texto para realizar alterações diretamente.

Isso pode ser utilizado para alterações ou utilizar a tela como template, utilizando ferramentas de substituição para alterar TAGs e textos.

- - -

# Menu de Navegação
A configuração dos botões do menu de navegação é feita no arquivo

	**$IHM/VTelasBotoes.led**

| Opções | Descrição |
| ---- | ---- |
| **botoes\_base** | Menu fica localizado na parte inferior do visor |
| **botoes\_direita** | Menu fica localizado na parte direita do visor |
| **botoes\_esquerda** | Menu fica localizado na parte esquerda do visor |
| **botoes\_topo** | Menu fica localizado na parte superior do visor |
| **Button** | Configura a ação do botão, geralmente a janela que deve abrir ao clicar nele |
| **TIP** | Configura  a legenda do botão quando o mouse é colocado em cima deste |
| **EXPAND=HORIZONTAL** | Configura  o nome que aparecerá no botão |

## Código Exemplo
Exemplo de menu com 4 botões em 2 linhas na parte inferior do visor

	cor_g - color(120, 40, 0)
	
	botoes_base - vbox[FONT=HEVELTICA_BOLD_25](
	  hbox(
		button[ACAO="TELA exemplo", TIP="Tela Exemplo", FGCOLOR=cor_g, EXPAND=HORIZONTAL]("Exemplo",cb_acao),
		button[ACAO="TELA UNIFILAR", TIP="Unifilar SE PS", FGCOLOR=cor_g, EXPAND=HORIZONTAL]("Unifilar SE",cb_acao)
	  ),
	  hbox(
		button[ACAO="TELA exemplo", TIP="Tela Exemplo", FGCOLOR=cor_g, EXPAND=HORIZONTAL]("Exemplo",cb_acao),
		button[ACAO="TELA UNIFILAR", TIP="Unifilar SE PS", FGCOLOR=cor_g, EXPAND=HORIZONTAL]("Unifilar SE",cb_acao)
	)

**OBS: a sintaxes do editor utiliza PARENTES**

- - -

# Problemas Comuns
**Quando o usuário copia uma informação no SigDraw, o mesmo não consegue colar em outra tela do SigDraw.**

1. Abra o SigDraw através do botão Editor do Visor de Acesso, uma instância do SigDraw abrirá em branco
1. No SigDraw, abra a tela da qual deseja copiar a informação (Arquivo \> Abrir)
1. Selecione a informação desejada e copie-a (Edição \> Copiar)
1. Minimize a janela do SigDraw aberta, mantendo-a aberta
1. No Visor de Acesso, clique em Telas
1. Navegue até a tela na qual quer colar a informação
1. Clique no menu Tela \> Editar, esta nova tela abrirá na mesma instância do SigDraw da tela anterior
1. Cole a informação na tela (Edição \> Colar)
1. Salve a alteração (Arquivo \> Salvar)

**SAGE não detecta automaticamente o dispositivo flash (PEN-DRIVE)**

1. No prompt digite su, (root root).
1. Digite fdisk -l para ver qual o endereço do pendrive, normalmente é sdb1
1. Digite então, mount /dev/sdb1 /mnt
1. Digite konqueror &
1. Uma janela do konqueror se abrirá (similar ao Windows Explorer), volte um nível e procure por uma pasta chamada mnt, lá está a pen-drive, agora você pode copiar os arquivos desejados.
1. Para desmontar a pendrive digite umount /dev/sdb1

**Configurar pasta compartilhada do VirtualBox entre o Windows e o SAGE (CentOS)**

1. Crie uma nova pasta no Windows. Por exemplo, _C:\SAGE_
2. No Virtualbox, clique no menu **Dispositivos \> Pastas Compartilhadas**, então clique em **Configurações de Pastas Compartilhadas**
3. Adicione a pasta localizada em C:\SAGE (Clique no ícone com sinal de + do lado direito)
4. Digite o nome e o caminho da pasta, clique no check-box para **Montar automaticamente** e **Tornar permanente**
5. Crie uma pasta dentro de /mnt com o mesmo nome (como **Super Usuário**)
6. Entre como root - **su (root root)**
7. Crie a pasta - **mkdir /mnt/sage**
8. E monte a pasta - **mount -t vboxsf sage /mnt/sage/**

**AtualizaBD fria fonte compila sem erros, mas base não sobe**

Caso a base não suba mesmo com o comando executando sem erros, apague os arquivos .xdr e atualize novamente a base. Durante o processo de atualização, algum dos arquivos .xdr pode ter se corrompido e com isso o SAGE não sobe a base.

**Protocolo exibindo erro 111 no slog**

O driver do protocolo não está instalado no sistema SAGE. A instalação do mesmo é suficiente para resolver o problema. 

**Alterado o arquivo hosts mas comunicação não fecha**

Ao alterar o arquivo hosts é preciso reiniciar o computador para que a nova lista seja carregada.

- - -

# Glossário
+ ALR - Serviço de Alarmes e Eventos
+ DNP - Distributed Network Protocol
+ IED - Industrial Eletronic Device
+ GCD - Gerenciamento de controle Distribuído
+ GMCD - Gerência das Memórias Compartilhadas Distribuídas
+ MCD - Memória Compartilhada e Distribuída
+ SAC - Serviço de Aquisição e Controle
+ SAGE - Sistema Aberto de Gerenciamento de Energia
+ SAR - Subsistema de Análise de Redes
+ SCD - Subsistema de Aquisição e Comunicação de Dados
+ SIG - Subsistema de Interface Gráfica
+ SNMP - Simple Network Management Protocol
+ SSC - Subsistema de Suporte Computacional
+ STI - Subsistema de Tratamento da Informação

- - -